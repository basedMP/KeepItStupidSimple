tfu_startup_effect = {
	#if = {limit = {	SPR = {is_ai = yes}} GER = { country_event = { id = germany.111 }}}
	if = {limit = {	AST = {is_ai = yes}} ENG = { country_event = { id = britain.111 }}}
		ENG = {
			#add_ideas = ENG_no_radar
			add_ideas = anti_strat_meme
		}
		FRA = {
			#add_ideas = ENG_no_radar
			add_ideas = anti_strat_meme
		}
		CAN = {
			#add_ideas = ENG_no_radar
			add_ideas = anti_strat_meme
		}
		AST = {
			add_ideas = ENG_no_radar
			add_ideas = anti_strat_meme
		}
		UAC = {
			add_ideas = ENG_no_radar
			add_ideas = anti_strat_meme
		}
		ITA = {
			#add_ideas = ITA_no_radar
			add_ideas = anti_strat_meme
		}
		GER = {
			#add_ideas = ITA_no_radar
			add_ideas = anti_strat_meme
		}
		ROM = {
			#add_ideas = ITA_no_radar
			add_ideas = anti_strat_meme
		}
		HUN = {
			#add_ideas = ITA_no_radar
			add_ideas = anti_strat_meme
		}
		SPR = {
			#add_ideas = ITA_no_radar
			add_ideas = anti_strat_meme
		}
		BUL = {
			#add_ideas = ITA_no_radar
			add_ideas = anti_strat_meme
		}
		every_country = {
			add_ideas = tradeback_spirit
		}

		tfu_startup_gens = yes
		 
		every_country = { add_ideas = start_eff }
		#FRA = { add_ideas = FRA_mech_strong }
		BHU = { HFU_fuel_startup_effect = yes }
		
			BHU = { give_resource_rights = { receiver = GER state = 724 } }
		GER = { 
		add_relation_modifier = {
		target = ROM
		modifier = trade
				}
		add_relation_modifier = {
		target = HUN
		modifier = trade		
			}
		add_relation_modifier = {
		target = ITA
		modifier = trade		
			}
		add_relation_modifier = {
		target = SPR
		modifier = trade		
			}
		add_relation_modifier = {
		target = BUL
		modifier = trade		
			}
		}
		ITA = { 
		add_relation_modifier = {
		target = ROM
		modifier = trade
				}
		add_relation_modifier = {
		target = HUN
		modifier = trade		
			}
		add_relation_modifier = {
		target = SPR
		modifier = trade		
			}
		add_relation_modifier = {
		target = GER
		modifier = trade_major		
			}
		add_relation_modifier = {
		target = BUL
		modifier = trade		
			}
		}
		JAP = { 
		add_relation_modifier = {
		target = MAN
		modifier = trade
			}
		}
		MAN = { 
		add_relation_modifier = {
		target = JAP 
		modifier = trade_major
			}
		}
		BUL = { 
		add_relation_modifier = {
		target = GER
		modifier = trade_major
				}
		add_relation_modifier = {
		target = ITA
		modifier = trade_major		
			}
		}
		HUN = { 
		add_relation_modifier = {
		target = GER
		modifier = trade_major
				}
		add_relation_modifier = {
		target = ITA
		modifier = trade_major		
			}
		}
		ROM = { 
		add_relation_modifier = {
		target = GER
		modifier = trade_major
				}
		add_relation_modifier = {
		target = ITA
		modifier = trade_major		
			}
		}
		SPR = { 
		add_relation_modifier = {
		target = GER
		modifier = trade_major
				}
		add_relation_modifier = {
		target = ITA
		modifier = trade_major		
			}
		}
		ENG = { 
		add_relation_modifier = {
		target = CAN
		modifier = trade
				}
		add_relation_modifier = {
		target = UAC
		modifier = trade
				}
		add_relation_modifier = {
		target = FRA
		modifier = trade		
				}
		add_relation_modifier = {
		target = AST
		modifier = trade		
				}
		add_relation_modifier = {
		target = RAJ
		modifier = trade		
				}
		add_relation_modifier = {
		target = SOV
		modifier = trade_sov		
				}
		add_relation_modifier = {
		target = USA
		modifier = trade	
				}
			}
		USA = { 
		add_relation_modifier = {
		target = CAN
		modifier = trade
				}
		add_relation_modifier = {
		target = UAC
		modifier = trade
				}
		add_relation_modifier = {
		target = FRA
		modifier = trade		
				}
		add_relation_modifier = {
		target = AST
		modifier = trade		
				}
		add_relation_modifier = {
		target = RAJ
		modifier = trade		
				}
		add_relation_modifier = {
		target = SOV
		modifier = trade_sov		
				}
			}
		FRA = { 
		add_relation_modifier = {
		target = ENG
		modifier = trade_major
				}
		add_relation_modifier = {
		target = SOV
		modifier = trade_sov		
				}
		add_relation_modifier = {
		target = USA
		modifier = trade	
				}
			}
		CAN = { 
		add_relation_modifier = {
		target = ENG
		modifier = trade_major
				}
		add_relation_modifier = {
		target = SOV
		modifier = trade_sov		
		}
		add_relation_modifier = {
		target = USA
		modifier = trade	
		}
	}
		AST = { 
		add_relation_modifier = {
		target = ENG
		modifier = trade_major
				}
		add_relation_modifier = {
		target = SOV
		modifier = trade_sov		
		}
		add_relation_modifier = {
		target = USA
		modifier = trade	
		}
	}
	UAC = { 
		add_relation_modifier = {
		target = ENG
		modifier = trade_major
				}
		add_relation_modifier = {
		target = SOV
		modifier = trade_sov		
		}
		add_relation_modifier = {
		target = USA
		modifier = trade	
		}
	}
	RAJ = { 
		add_relation_modifier = {
		target = ENG
		modifier = trade_major
				}
		add_relation_modifier = {
		target = SOV
		modifier = trade_sov		
		}
		add_relation_modifier = {
		target = USA
		modifier = trade	
		}
	}
	#		182 = {add_dynamic_modifier = {
	#			modifier = ITA_fortress_modifier1
	#		}	}	
	#		164 = {add_dynamic_modifier = {
	#			modifier = ITA_fortress_modifier1
	#		}	}
	#		187 = {add_dynamic_modifier = {
	#			modifier = ITA_fortress_modifier1
	#		}	}	
	#		47 = {add_dynamic_modifier = {
	#			modifier = ITA_fortress_modifier1
	#		}	}		
	#		731 = {add_dynamic_modifier = {
	#			modifier = ITA_fortress_modifier1
	#		}	}	
	#		186 = {add_dynamic_modifier = {
	#			modifier = ITA_fortress_modifier1
	#		}	}	
	#		184 = {add_dynamic_modifier = {
	#			modifier = ITA_fortress_modifier1
	#		}	}	
	#		115 = {add_dynamic_modifier = {
	#			modifier = ITA_fortress_modifier1
	#		}	}	
	#		114 = {add_dynamic_modifier = {
	#			modifier = ITA_fortress_modifier1
	#		}	}
	#		1 = {add_dynamic_modifier = {
	#			modifier = ITA_fortress_modifier1
	#		}	}				
}

africa_fuel_gain_start = {
	GER = {set_variable = { africa_fuel = 0}}
	every_country = { limit = {axis = yes} add_dynamic_modifier = {	modifier = fuel_gain_from_africa}}
}

africa_fuel_gain = {	
	add_to_variable = { var = GER.africa_fuel value = 0.05 }
}

anti_cross_faction_trade_axis = {
add_opinion_modifier = {
    target = GER
    modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
    target = ITA
    modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
    target = ROM
    modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
    target = HUN
    modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
    target = SPR
    modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
	target = BUL
    modifier = anti_cross_faction_trade
	}
}

anti_cross_faction_trade_allies = {
add_opinion_modifier = {
    target = ENG
    modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
    target = FRA
    modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
    target = CAN
    modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
	target = AST
	modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
	target = UAC
	modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
	target = USA
	modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
	target = RAJ
	modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
    target = SOV
    modifier = anti_cross_faction_trade
	}
}

anti_cross_faction_trade_geacps = {
add_opinion_modifier = {
    target = JAP
    modifier = anti_cross_faction_trade
	}
add_opinion_modifier = {
    target = MAN
    modifier = anti_cross_faction_trade
	}
}

ger_total_mob = {
ger_total_mob_1 = yes  
ger_total_mob_1 = yes 
ger_total_mob_1 = yes 
ger_total_mob_1 = yes 
ger_total_mob_1 = yes 
ger_total_mob_1 = yes 
}

ger_total_mob_1 = { 
ger_total_mob_conv = yes
ger_total_mob_conv = yes
ger_total_mob_conv = yes
ger_total_mob_conv = yes
ger_total_mob_conv = yes
ger_total_mob_conv = yes
ger_total_mob_conv = yes
ger_total_mob_conv = yes
ger_total_mob_conv = yes
ger_total_mob_conv = yes
}

ger_total_mob_conv = { 
random_owned_controlled_state = { 	limit = {  is_core_of = GER industrial_complex > 0 } 
	remove_building = {type = industrial_complex level = 1 }	add_building_construction = { type = arms_factory level = 1 instant_build = yes }
	}
}

tank_designers_free = { if = { limit = { NOT = { has_country_flag = nation_picked_tank_designer }} 	ROOT = { add_ideas = free_tank_designer set_country_flag = nation_picked_tank_designer }	} }
mech_designers_free = { if = { limit = { NOT = { has_country_flag = nation_picked_mech_designer }} 	ROOT = { add_ideas = free_mech_designer set_country_flag = nation_picked_mech_designer }	} }
naval_designers_free = { if = { limit = { NOT = { has_country_flag = nation_picked_naval_designer }} 	ROOT = { add_ideas = free_naval_designer set_country_flag = nation_picked_naval_designer }	} }

air_designers_free = { if = { limit = { NOT = { has_country_flag = nation_picked_air_designer }} 	ROOT = { add_ideas = free_air_designer set_country_flag = nation_picked_air_designer }	} }

SOV_relocate_civ = {
	random_owned_controlled_state={
				limit={
				any_neighbor_state={ 
					is_core_of=SOV
					NOT={is_controlled_by=SOV}
					NOT = { state = 146}
					} 
				industrial_complex>0}
				remove_building = {type = industrial_complex level = 1}
			OWNER={
				random_owned_controlled_state={
					limit={OR={state=582 state=572 state=583 state=590 state=573}}
					add_extra_state_shared_building_slots=2
					add_building_construction = {
   				 		type = industrial_complex
   				 		level = 2
  				 		instant_build = yes
				 		}
			}				
			}
}
}
SOV_relocate_civ_into_two_mills = {
	random_owned_controlled_state={
				limit={
				any_neighbor_state={ 
					is_core_of=SOV
					NOT={is_controlled_by=SOV}
					NOT = { state = 146}
					} 
				industrial_complex>0}
				remove_building = {type = industrial_complex level = 1}
			OWNER={
				random_owned_controlled_state={
					limit={OR={state=582 state=572 state=583 state=590 state=573}}
					add_extra_state_shared_building_slots=2
					add_building_construction = {
   				 		type = arms_factory
   				 		level = 2
  				 		instant_build = yes
				 		}
			}				
			}
}
}
SOV_relocate_mil = {
	random_owned_controlled_state={
				limit={any_neighbor_state={is_core_of=SOV
					NOT={is_controlled_by=SOV}} arms_factory>0}
				remove_building = {type = arms_factory level = 1}
			OWNER={
				random_owned_controlled_state={
					limit={OR={state=582 state=572 state=583 state=590 state=573}}
					add_extra_state_shared_building_slots=1
					add_building_construction = {
   				 		type = arms_factory
   				 		level = 1
  				 		instant_build = yes
				 		}
			}				
			}
}
}
SOV_relocate_civ_moscow = {
	random_owned_controlled_state={
				limit={OR ={ 	state = 219 state = 195 state = 217 } industrial_complex>0}
				remove_building = {type = industrial_complex level = 1}
			OWNER={
				random_owned_controlled_state={
					limit={OR={state=582 state=572 state=583 state=590 state=573}}
					add_extra_state_shared_building_slots=1
					add_building_construction = {
   				 		type = industrial_complex
   				 		level = 1
  				 		instant_build = yes
				 		}
			}				
			}
}
}
SOV_relocate_mil_moscow = {
	random_owned_controlled_state={
				limit={OR ={ 	state = 219 state = 195 state = 217 } arms_factory>0}
				remove_building = {type = arms_factory level = 1}
				OWNER={add_to_variable={relocatedmil = 1}}
			OWNER={
				random_owned_controlled_state={
					limit={OR={state=484 state=572 state=477 state=573 state=483 state=179}}
					add_extra_state_shared_building_slots=1
					add_building_construction = {
   				 		type = arms_factory
   				 		level = 1
  				 		instant_build = yes
				 		}
			}				
			}
}
}

air_to_germany = {
	if = { limit = { tag = ITA }
	send_equipment = { equipment = fighter_equipment amount = 10000 target = GER }
	send_equipment = { equipment = CAS_equipment amount = 10000 target = GER }
	send_equipment = { equipment = tac_bomber_equipment amount = 10000 target = GER }
	send_equipment = { equipment = heavy_fighter_equipment amount = 10000 target = GER }	
	send_equipment = { equipment = strat_bomber_equipment amount = 10000 target = GER }	
	}
}

air_to_italy = {
	if = { limit = { tag = GER }
	send_equipment = { equipment = fighter_equipment amount = 10000 target = ITA }
	send_equipment = { equipment = CAS_equipment amount = 10000 target = ITA }
	send_equipment = { equipment = tac_bomber_equipment amount = 10000 target = ITA }
	send_equipment = { equipment = heavy_fighter_equipment amount = 10000 target = ITA }	
	send_equipment = { equipment = strat_bomber_equipment amount = 10000 target = ITA }	
	}
}

air_to_uk = {
	if = { limit = { tag = USA }
	send_equipment = { equipment = fighter_equipment amount = 10000 target = ENG }
	send_equipment = { equipment = CAS_equipment amount = 10000 target = ENG }
	send_equipment = { equipment = tac_bomber_equipment amount = 10000 target = ENG }
	send_equipment = { equipment = heavy_fighter_equipment amount = 10000 target = ENG }	
	send_equipment = { equipment = strat_bomber_equipment amount = 10000 target = ENG }	
	}
}

air_to_usa = {
	if = { limit = { tag = ENG }
	send_equipment = { equipment = fighter_equipment amount = 10000 target = USA }
	send_equipment = { equipment = CAS_equipment amount = 10000 target = USA }
	send_equipment = { equipment = tac_bomber_equipment amount = 10000 target = USA }
	send_equipment = { equipment = heavy_fighter_equipment amount = 10000 target = USA }	
	send_equipment = { equipment = strat_bomber_equipment amount = 10000 target = USA }	
	}
}

### emergency_factory_conversion ####
replace_civ_with_arms_factories = {	

	random_owned_controlled_state = {
		limit = {
			is_fully_controlled_by = ROOT
			industrial_complex > 0
		}
		remove_building = {
			type = industrial_complex
			level = 1
		}
		add_building_construction = {
			type = arms_factory
			level = 1
			instant_build = yes
		}
	}
	
	random_owned_controlled_state = {
		limit = {
			is_fully_controlled_by = ROOT
			industrial_complex > 0
		}
		remove_building = {
			type = industrial_complex
			level = 1
		}
		add_building_construction = {
			type = arms_factory
			level = 1
			instant_build = yes
		}
	}

	random_owned_controlled_state = {
		limit = {
			is_fully_controlled_by = ROOT
			industrial_complex > 0
		}
		remove_building = {
			type = industrial_complex
			level = 1
		}
		add_building_construction = {
			type = arms_factory
			level = 1
			instant_build = yes
		}
	}

	random_owned_controlled_state = {
		limit = {
			is_fully_controlled_by = ROOT
			industrial_complex > 0
		}
		remove_building = {
			type = industrial_complex
			level = 1
		}
		add_building_construction = {
			type = arms_factory
			level = 1
			instant_build = yes
		}
	}

	random_owned_controlled_state = {
		limit = {
			is_fully_controlled_by = ROOT
			industrial_complex > 0
		}
		remove_building = {
			type = industrial_complex
			level = 1
		}
		add_building_construction = {
			type = arms_factory
			level = 1
			instant_build = yes
		}
	}
}

convert_one_mill = {	

	random_owned_controlled_state = {
		limit = {
			is_fully_controlled_by = ROOT
			industrial_complex > 0
		}
		remove_building = {
			type = industrial_complex
			level = 1
		}
		add_building_construction = {
			type = arms_factory
			level = 1
			instant_build = yes
		}
	}
}

USA_great_depression_level_down = {
	if = {
		limit = { has_idea = great_depression_3 }
		custom_effect_tooltip = USA_great_depression_down_tt
		remove_ideas = great_depression_3
	}
	if = {
		limit = { has_idea = great_depression_2 }
		custom_effect_tooltip = USA_great_depression_down_tt
		swap_ideas = {
			remove_idea = great_depression_2
			add_idea = great_depression_3
		}
	}
	if = {
		limit = { has_idea = great_depression }
		custom_effect_tooltip = USA_great_depression_down_tt
		swap_ideas = {
			remove_idea = great_depression
			add_idea = great_depression_2
		}
	}
}


#######GENS########

tfu_startup_gens = {

	GER = {
		add_namespace = {
	name = "ger_unit_leader"
	type = unit_leader

	
	
}

create_field_marshal = {
	name = "Gerd von Rundstedt"
	picture = "Portrait_Germany_Gerd_von_Rundstedt.dds"
	traits = { organizer winter_specialist logistics_wizard infantry_leader inflexible_strategist unyielding_defender inspirational_leader organisational_leader }
	skill = 8
	id = 1
	attack_skill = 4
	defense_skill = 5
	planning_skill = 3
	logistics_skill = 9
}

create_field_marshal = {
	name = "Günther von Kluge"
	picture = "Portrait_Germany_Gunther_von_Kluge.dds"
	traits = { ranger hill_fighter organizer offensive_doctrine logistics_wizard inspirational_leader trickster expert_improviser panzer_leader panzer_expert fast_planner thorough_planner urban_assault_specialist  }
    skill = 8
	id = 2
	attack_skill = 7
    defense_skill = 2
    planning_skill = 4
    logistics_skill = 7
}

create_field_marshal = {
	name = "Friedrich Paulus"
	picture = "Portrait_Germany_Friedrich_Paulus.dds"
	traits = { commando infantry_leader urban_assault_specialist organizer logistics_wizard trait_cautious  }
	skill = 8
	id = 3
	attack_skill = 4
	defense_skill = 4
	planning_skill = 5
	logistics_skill = 9
}

###############################
###########GENS################
############################### 



create_corps_commander = {
	name = "Sepp Dietrich"
	picture = "Portrait_Germany_Josef_Dietrich.dds"
	traits = { trait_engineer urban_assault_specialist ranger hill_fighter organizer trickster offensive_doctrine expert_improviser brilliant_strategist swamp_fox panzer_leader panzer_expert  }
	skill = 8
	id = 24
	attack_skill = 7
    defense_skill = 2
    planning_skill = 5
    logistics_skill = 6
}

create_corps_commander = {
	name = "Heinz Guderian"
	picture = "Portrait_Germany_Heinz_Guderian.dds"
	traits = { trait_engineer urban_assault_specialist ranger hill_fighter organizer trickster offensive_doctrine expert_improviser brilliant_strategist panzer_leader panzer_expert  }
	skill = 8
	id = 6
	attack_skill = 7
    defense_skill = 2
    planning_skill = 5
    logistics_skill = 6
}

create_corps_commander = {
	name = "Hasso von Manteuffel"
	desc = ""
	picture = "Portrait_Germany_Hasso_von_Manteuffel.dds"
	traits = { trait_engineer urban_assault_specialist ranger hill_fighter organizer trickster offensive_doctrine expert_improviser brilliant_strategist panzer_leader panzer_expert  }
	skill = 8
	id = 8
	attack_skill = 7
    defense_skill = 2
    planning_skill = 5
    logistics_skill = 6
}

create_corps_commander = {
	name = "Erich von Manstein"
	picture = "Portrait_Germany_Erich_von_Manstein.dds"
	traits = { trait_engineer organizer trickster offensive_doctrine expert_improviser panzer_leader panzer_expert brilliant_strategist ranger trait_engineer fortress_buster }
	skill = 8
	id = 5
	attack_skill = 3
    defense_skill = 2
    planning_skill = 5
    logistics_skill = 5
}

##########################

create_corps_commander = {
	name = "Kurt Student"
	picture = "Portrait_Germany_Kurt_Student.dds"
	traits = { trait_reckless trait_mountaineer hill_fighter commando camouflage_expert trickster expert_improviser infantry_leader  infantry_expert }
	skill = 8
	id = 16
	 attack_skill = 4
    defense_skill = 6
    planning_skill = 6
    logistics_skill = 6
}


create_corps_commander = {
	name = "Wilhelm List"
	picture = "Portrait_Germany_Wilhelm_List.dds"
	traits = { old_guard  infantry_leader ambusher skilled_staffer trickster guerilla_fighter }
	skill = 7
	attack_skill = 4
    defense_skill = 3
    planning_skill = 6
    logistics_skill = 4
	id = 7
}

create_corps_commander = {
	name = "Johannes Blaskowitz"
	picture = "Portrait_Germany_Johannes_Blaskowitz.dds"
	traits = { old_guard  infantry_leader ambusher skilled_staffer trickster guerilla_fighter }
	skill = 7
	attack_skill = 4
    defense_skill = 3
    planning_skill = 6
    logistics_skill = 4
	id = 19
}

create_corps_commander = {
	name = "Friedrich Schulz"
	picture = "Portrait_Germany_Friedrich_Schulz.dds"
	traits = { old_guard  infantry_leader ambusher skilled_staffer trickster guerilla_fighter }
	skill = 7
	id = 21
	attack_skill = 4
    defense_skill = 3
    planning_skill = 6
    logistics_skill = 4
}

create_corps_commander = {
	name = "Fedor von Bock"
	picture = "Portrait_Germany_Fedor_von_Bock.dds"
	traits = { harsh_leader infantry_leader infantry_expert skilled_staffer trickster expert_improviser }
	skill = 7
	id = 9
	attack_skill = 7
    defense_skill = 4
    planning_skill = 6
    logistics_skill = 5
}

create_corps_commander = {
	name = "Erwin von Witzleben"
	picture = "Portrait_Germany_Erwin_von_Witzleben.dds"
	traits = { harsh_leader infantry_leader infantry_expert skilled_staffer trickster expert_improviser }
	skill = 7
	id = 17
	attack_skill = 7
    defense_skill = 4
    planning_skill = 6
    logistics_skill = 5
}

create_corps_commander = {
	name = "Alfred Jodl"
	picture = "Portrait_Germany_Alfred_Jodl.dds"
	traits = { harsh_leader infantry_leader infantry_expert skilled_staffer trickster expert_improviser }
	skill = 7
	id = 23
	attack_skill = 7
    defense_skill = 4
    planning_skill = 6
    logistics_skill = 5
}


##########
###NAVY###
##########

create_navy_leader = {
	name = "Karl Dönitz"
	picture = "Portrait_Germany_Karl_Donitz.dds"
	traits = { seawolf spotter concealment_expert lancer loading_drill_master torpedo_expert }
	skill = 5
	id = 26
	attack_skill = 5
	defense_skill = 3
	maneuvering_skill = 3
	coordination_skill = 5
}

create_navy_leader = {
	name = "Erich Raeder"
		picture = "Portrait_Germany_Erich_Raeder.dds"
	traits = { naval_lineage battleship_adherent blockade_runner ironside big_guns_expert air_controller torpedo_bomber flight_deck_manager cruiser_captain destroyer_leader lone_wolf fly_swatter fleet_protector }
	skill = 5
	id = 27
	attack_skill = 3
	defense_skill = 6
	maneuvering_skill = 3
	coordination_skill = 3
}
	}

	SOV = {
		
add_namespace = {
	name = "unit_leader_sov"
	type = unit_leader
}

create_corps_commander = {
	name = "Georgy Zhukov"
	id = 410
		picture = "Portrait_Soviet_Gregory_Zhukov.dds"
	traits = { urban_assault_specialist ranger hill_fighter aggressive_assaulter winter_specialist panzer_leader panzer_expert expert_improviser brilliant_strategist trickster expert_officer}
	skill = 8

	
	attack_skill = 4
	defense_skill = 2
	planning_skill = 7
	logistics_skill = 10
}

create_field_marshal = {
	name = "Markian Popov"
		picture = "Portrait_Soviet_Matkian_Popov.dds"
	traits = { winter_specialist brilliant_strategist }
	skill = 8

	
	attack_skill = 2
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Vasily Chuikov"
		picture = "Portrait_Soviet_Vasily_Chuikov.dds"
	traits = { brilliant_strategist }
	skill = 8

	
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_field_marshal = {
	name = "Konstantin Rokossovsky"
	id = 402
		picture = "Portrait_Soviet_Konstantin_Rokossovsky.dds"
	traits = { urban_assault_specialist brilliant_strategist panzer_leader panzer_expert ranger aggressive_assaulter trickster organizer thorough_planner logistics_wizard expert_improviser}
	skill = 8
	
	attack_skill = 4
	defense_skill = 3
	planning_skill = 6
	logistics_skill = 10
}

create_corps_commander = {
	name = "Nikolai Vatutin"
		picture = "Portrait_Soviet_Nikolai_Fyodorovich_Vatutin.dds"
	traits = { infantry_leader infantry_expert ranger urban_assault_specialist trickster expert_improviser}
	skill = 6
	
	attack_skill = 7
	defense_skill = 5
	planning_skill = 4
	logistics_skill = 10
}

create_corps_commander = {
	name = "Aleksandr Vasilevsky"
	id = 407
		picture = "Portrait_Soviet_Aleksandr_Vasilevsky.dds"
	traits = { infantry_leader infantry_expert expert_officer }
	skill = 6

	
	attack_skill = 4
	defense_skill = 3
	planning_skill = 4
	logistics_skill = 10
}

create_corps_commander = {
	name = "Boris Shaposhnikov"
		picture = "Portrait_Soviet_Boris_Shaposhnikov.dds"
	traits = { infantry_leader infantry_expert expert_officer}
	skill = 6
	
	attack_skill = 4
	defense_skill = 3
	planning_skill = 4
	logistics_skill = 10
}

create_corps_commander = {
	name = "Filipp Golikov"
		picture = "Portrait_Soviet_Filipp_Golikov.dds"
	traits = { infantry_leader infantry_expert expert_officer }
	skill = 6
	
	attack_skill = 4
	defense_skill = 3
	planning_skill = 4
	logistics_skill = 10
}

create_field_marshal = {
	name = "Ivan Konev"
	id = 408
		picture = "Portrait_Soviet_Ivan_Konev.dds"
	traits = { infantry_leader expert_delegator }
	skill = 6


	
	attack_skill = 4
	defense_skill = 3
	planning_skill = 8
	logistics_skill = 8
}

create_corps_commander = {
	name = "Semyon Timoshenko"
	id = 404
		picture = "Portrait_Soviet_Semyon_Timoshenko.dds"
	traits = { cavalry_leader }
	skill = 6


	
	attack_skill = 3
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}


create_navy_leader = {
	name = "Filipp Oktyabrskiy"
		picture = "Portrait_Soviet_Filipp_Oktyabrsky.dds"
	traits = { ground_pounder ironside }
	skill = 6
	attack_skill = 3
	defense_skill = 2
	maneuvering_skill = 1
	coordination_skill = 1
}

create_navy_leader = {
	name = "Sergey Gorshkov"
	id = 411
		picture = "Portrait_Soviet_Sergey_Gorshkov.dds"
	traits = { navy_career_officer bold }
	skill = 3
	attack_skill = 3
	defense_skill = 2
	maneuvering_skill = 3
	coordination_skill = 2
}

# Arrested in '41
create_navy_leader = {
	name = "Gordey Levchenko"
	id = 406
		picture = "Portrait_Soviet_Gordey_Levchenko.dds"
	traits = { green_water_expert }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	maneuvering_skill = 1
	coordination_skill = 2
}

create_navy_leader = {
	name = "Arseniy Golovko"
	id = 409
		picture = "Portrait_Soviet_Arseniy_Golovko.dds"
	traits = { arctic_water_expert spotter }
	skill = 4
	attack_skill = 3
	defense_skill = 4
	maneuvering_skill = 3
	coordination_skill = 3
}

create_navy_leader = {
	name = "Vladimir Kasatonov"
	id = 403
		picture = "Portrait_Soviet_Vladimir_Kasatonov.dds"
	traits = { seawolf }
	skill = 2
	attack_skill = 1
	defense_skill = 2
	maneuvering_skill = 2
	coordination_skill = 2
}
	}

}
