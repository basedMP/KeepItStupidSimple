"TFU War Decisions FRA" = {

	FRA_mission_overtime_working = {

		icon = GFX_decision_generic_army_support
		days_mission_timeout = 45
		days_remove = 45
		is_good = no
		available = {   
			always = no
		}
		allowed = {
				original_tag = FRA
		}
		fire_only_once = yes
		visible = {	
			always = no
		} 
		activation = {
			FRA = {
				has_completed_focus = FRA_focus_overtime_working
			}
		}
		timeout_effect = {
				remove_ideas = FRA_overtime_working
				add_ideas = FRA_worker_strikes
		}
	}
}	