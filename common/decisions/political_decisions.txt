political_actions = {
	give_refuge_ger = {

		icon = generic_research

        allowed = {
           
        }

	visible = {
		NOT = { has_government = fascism }
		NOT = { has_government = communism }
		NOT = { has_government = democratic }
		NOT = { has_government = neutrality }
	}

        cost = 100
        fire_only_once = yes
        
        days_remove = -1

        available = {
            
        }

        modifier = {
            research_speed_factor = 0.05
            stability_factor = -0.05
        }


        ai_will_do = {
			factor = 1
			modifier = {
				tag = ENG
				has_War = no
				date < 1937.3.1
				factor = 0
			}
		}
    }
    give_refuge_ita = {

    	icon = generic_research

        allowed = {
            
		}

		visible = {
            NOT = { has_government = fascism }
            NOT = { has_government = communism }
			NOT = { has_government = democratic }
            NOT = { has_government = neutrality }
        }
        cost = 100
        fire_only_once = yes
        days_remove = -1

        available = {
            ITA = { NOT = { has_government = ROOT } }
        }

        modifier = {
            research_speed_factor = 0.05
            stability_factor = -0.05
        }

        remove_trigger = {
            ITA = { has_government = ROOT }
        }
        ai_will_do = {
			factor = 1
			modifier = {
				tag = ENG
				has_War = no
				date < 1937.3.1
				factor = 0
			}
		}
    }

    anti_fascist_raids = {

    	icon = generic_civil_support

    	visible = {
			NOT = { has_government = fascism }
		}
		available = {   fascism > 0.03    }
	
		modifier = {
			fascism_drift = -0.05
			stability_weekly = 0.007
		}
	
		days_remove = 120
		cost = 50

		complete_effect = {
			add_stability = -0.1
		}
		
		remove_effect = {
		}
		
		ai_will_do = {
			base = 0
			modifier = {
				add = 1
				tag = AUS
				fascism > 0.45
				is_historical_focus_on = no
			}
		}
    }

    anti_democratic_raids = {

    	icon = generic_civil_support

    	visible = {
			NOT = { has_government = democratic }
		}
		available = {   democratic > 0.03     }

		modifier = {
			democratic_drift = -0.05
			stability_weekly = 0.007
		}
	
		days_remove = 120
		cost = 50

		complete_effect = {
			add_stability = -0.1
		}
		
		remove_effect = {
		}
		
		ai_will_do = {
			factor = 0
		}
    }
	 anti_monarchist_raids = {
	name = "Anti-Monarchist Raids"
    	icon = generic_civil_support

    	visible = {
			NOT = { has_government = neutrality  }
		}
		available = {   neutrality > 0.03   }

		modifier = {
			neutrality_drift = -0.05
			stability_weekly = 0.007
		}
	
		days_remove = 120
		cost = 50

		complete_effect = {
			add_stability = -0.1
		}
		
		remove_effect = {
		}
		
		ai_will_do = {
			factor = 0
		}
    }
    anti_communist_raids = {

    	icon = generic_civil_support

    	visible = {
			NOT = { has_government = communism }
		}
		available = {   communism > 0.03   }

		modifier = {
			communism_drift = -0.05
			stability_weekly = 0.007
		}
	
		days_remove = 120
		cost = 50

		complete_effect = {
			add_stability = -0.1
		}
		
		remove_effect = {
		}
		
		ai_will_do = {
			factor = 0
		}
    }
}
