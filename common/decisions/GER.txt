#Category only allowed for GER

"TFU War Decisions" = {
	
	fall_weiss = {
		
		icon = GFX_decision_ger_reichskommissariats

		allowed = {
		}
	available = {AND = {date > 1939.9.1 has_completed_focus = GER_danzig_or_war} }
	fire_only_once = yes
	visible = {
			original_tag = GER
			always = yes
	}
	complete_effect = {
	declare_war_on = { type = annex_everything    target = POL }
	hidden_effect = {
		scoped_sound_effect = "5Uhr45"
		every_country = { limit = {axis = yes} add_ideas = "HP Spirit" set_technology = { blitzkrieg_on = 1 popup = no}}
		every_country = { limit = {allies = yes} add_ideas = "HP Spirit Allies" set_technology = { allies_blitzkrieg_on = 1 popup = no}}
		every_country = { remove_ideas = anti_strat_meme remove_ideas = tradeback_spirit }
		add_ideas = { GER_war_consumer_goods }
		add_named_threat = { threat = 20 name = "Fall Weiss" }
		ENG = { add_to_war = { targeted_alliance = POL enemy = GER } } 
		AST = { add_to_war = { targeted_alliance = POL enemy = GER } } 
		UAC = { add_to_war = { targeted_alliance = POL enemy = GER } } 
		RAJ = { add_to_war = { targeted_alliance = POL enemy = GER } } 
		FRA = { add_to_war = { targeted_alliance = POL enemy = GER } }
		CAN = { add_to_war = { targeted_alliance = POL enemy = GER } }
		ROM = { add_to_war = { targeted_alliance = GER enemy = FRA } } 
		ROM = { add_to_war = { targeted_alliance = GER enemy = AST } } 
		ROM = { add_to_war = { targeted_alliance = GER enemy = UAC } } 
		ROM = { add_to_war = { targeted_alliance = GER enemy = ENG } } 
		ROM = { add_to_war = { targeted_alliance = GER enemy = CAN } }
		ROM = { add_to_war = { targeted_alliance = GER enemy = RAJ } } 
		ROM = { add_to_war = { targeted_alliance = GER enemy = POL } } 
		HUN = { add_to_war = { targeted_alliance = GER enemy = FRA } } 
		HUN = { add_to_war = { targeted_alliance = GER enemy = ENG } } 
		HUN = { add_to_war = { targeted_alliance = GER enemy = AST } } 
		HUN = { add_to_war = { targeted_alliance = GER enemy = UAC } } 
		HUN = { add_to_war = { targeted_alliance = GER enemy = RAJ } }
		HUN = { add_to_war = { targeted_alliance = GER enemy = CAN } } 
		HUN = { add_to_war = { targeted_alliance = GER enemy = POL } }
		SPR = { add_to_war = { targeted_alliance = GER enemy = FRA } } 
		SPR = { add_to_war = { targeted_alliance = GER enemy = AST } } 
		SPR = { add_to_war = { targeted_alliance = GER enemy = RAJ } }
		SPR = { add_to_war = { targeted_alliance = GER enemy = UAC } } 
		SPR = { add_to_war = { targeted_alliance = GER enemy = ENG } } 
		SPR = { add_to_war = { targeted_alliance = GER enemy = CAN } } 
		SPR = { add_to_war = { targeted_alliance = GER enemy = POL } } 
		BUL = { add_to_war = { targeted_alliance = GER enemy = RAJ } }
		BUL = { add_to_war = { targeted_alliance = GER enemy = FRA } } 
		BUL = { add_to_war = { targeted_alliance = GER enemy = AST } } 
		BUL = { add_to_war = { targeted_alliance = GER enemy = UAC } } 
		BUL = { add_to_war = { targeted_alliance = GER enemy = ENG } } 
		BUL = { add_to_war = { targeted_alliance = GER enemy = CAN } } 
		BUL = { add_to_war = { targeted_alliance = GER enemy = POL } } 
		
		ITA = { add_to_war = { targeted_alliance = GER enemy = FRA } }
		ITA = { add_to_war = { targeted_alliance = GER enemy = AST } }
		ITA = { add_to_war = { targeted_alliance = GER enemy = UAC } }
		ITA = { add_to_war = { targeted_alliance = GER enemy = ENG } }
		ITA = { add_to_war = { targeted_alliance = GER enemy = CAN } } 
		ITA = { add_to_war = { targeted_alliance = GER enemy = POL } }
		ITA = { add_to_war = { targeted_alliance = GER enemy = RAJ } }

		550 = { set_demilitarized_zone = no }
		271 = { set_demilitarized_zone = no } 
		559 = { set_demilitarized_zone = no } 
		ENG = {add_ideas = {uk_consumer_goods}}
		ENG = {
			add_timed_idea = {
				idea = ENG_africa_buff
				days = 30
			}
		}
		ITA = { 
			add_timed_idea = { idea = ita_italian_naval_escort days = 160 }}
			}
		}
		
	}
	
	fall_gelb_netherlands = {
		
		icon = GFX_decision_ger_reichskommissariats

		allowed = {
		}
		available = {AND = {date > 1939.11.1} }
	fire_only_once = yes
	visible = {
			original_tag = GER
			NOT = {has_war_with = HOL}
	}
	complete_effect = {
	declare_war_on = { type = annex_everything    target = HOL }
	hidden_effect = {
		add_named_threat = { threat = 10 name = "Fall Gelb" }
		ROM = { add_to_war = { targeted_alliance = GER enemy = HOL } }
		HUN = { add_to_war = { targeted_alliance = GER enemy = HOL } }
		SPR = { add_to_war = { targeted_alliance = GER enemy = HOL } }	
		ITA = { add_to_war = { targeted_alliance = GER enemy = HOL } }	
		BUL = { add_to_war = { targeted_alliance = GER enemy = HOL } }	 	
			}
		}
	}
	
	fall_gelb_belgium = {
		
		icon = GFX_decision_ger_reichskommissariats

		allowed = {
		}
		available = {AND = {date > 1939.11.1} }
	fire_only_once = yes
	visible = {
			original_tag = GER
			NOT = {has_war_with = BEL}
	}
	complete_effect = {
	declare_war_on = { type = annex_everything    target = BEL }
	hidden_effect = {
		add_named_threat = { threat = 10 name = "Fall Gelb" }
		ROM = { add_to_war = { targeted_alliance = GER enemy = BEL } }
		HUN = { add_to_war = { targeted_alliance = GER enemy = BEL } }
		SPR = { add_to_war = { targeted_alliance = GER enemy = BEL } }
		ITA = { add_to_war = { targeted_alliance = GER enemy = BEL } }
		BUL = { add_to_war = { targeted_alliance = GER enemy = BEL } }	
			}
		}
	}
	
	"Unternehmen Weseruebung Denmark" = {
		
		icon = GFX_decision_ger_reichskommissariats

		allowed = {
		}
	available = {date > 1939.9.1 AND = {has_war_with = ENG }}
	fire_only_once = yes
	visible = {
			original_tag = GER
			always = yes
	}
	complete_effect = {
	declare_war_on = { type = annex_everything    target = DEN }
	hidden_effect = {
		add_named_threat = { threat = 10 name = "Unternehmen Weserübung" }
		ROM = { add_to_war = { targeted_alliance = GER enemy = DEN } }
		HUN = { add_to_war = { targeted_alliance = GER enemy = DEN } }
		SPR = { add_to_war = { targeted_alliance = GER enemy = DEN } }
		ITA = { add_to_war = { targeted_alliance = GER enemy = DEN } }
		BUL = { add_to_war = { targeted_alliance = GER enemy = DEN } }
			}
		}
	}
	
	"Unternehmen Weseruebung Norway" = {
		
		icon = GFX_decision_ger_reichskommissariats

		allowed = {
		}
	available = {date > 1939.9.1 AND = {has_war_with = ENG }} 
	fire_only_once = yes
	visible = {
			original_tag = GER
			always = yes
	}
	complete_effect = {
	declare_war_on = { type = annex_everything    target = NOR }
	hidden_effect = {
		add_named_threat = { threat = 10 name = "Unternehmen Weserübung" }
		ROM = { add_to_war = { targeted_alliance = GER enemy = NOR } }
		HUN = { add_to_war = { targeted_alliance = GER enemy = NOR } }
		SPR = { add_to_war = { targeted_alliance = GER enemy = NOR } }
		ITA = { add_to_war = { targeted_alliance = GER enemy = NOR } }
		BUL = { add_to_war = { targeted_alliance = GER enemy = NOR } } 
			}
		}
	}
	
	"Unternehmen Barbarossa" = {
		
		icon = GFX_decision_ger_reichskommissariats

		allowed = {
		}
	available = {date > 1941.6.21 AND = {has_war_with = ENG }}
	fire_only_once = yes
	visible = {
			original_tag = GER
			always = yes
	}
	complete_effect = {
	declare_war_on = { type = annex_everything    target = SOV }
	hidden_effect = {
		SPR = { swap_ideas = { 
			remove_idea = SPR_the_duty_to_work2
			add_idea = SPR_the_duty_to_work3 
		} }
		GER = {country_event =  { id = germany.113}}
		add_named_threat = { threat = 10 name = "Unternehmen Barbarossa" }
		ROM = { add_to_war = { targeted_alliance = GER enemy = SOV } }
		HUN = { add_to_war = { targeted_alliance = GER enemy = SOV } }
		ITA = { add_to_war = { targeted_alliance = GER enemy = SOV } }
		SPR = { add_to_war = { targeted_alliance = GER enemy = SOV } }
		BUL = { add_to_war = { targeted_alliance = GER enemy = SOV } }
		ENG = {  
			create_corps_commander = {
   			name = "George Giffard"
   			picture = "Portrait_Britain_George_Giffard.dds"
   			traits = { hill_fighter organizer offensive_doctrine expert_improviser infantry_leader infantry_expert organizer trait_mountaineer desert_fox   }
   			skill = 8
   			attack_skill = 7
   			defense_skill = 3
   			planning_skill = 6
   			logistics_skill = 7
			}
  
		create_field_marshal = {
   			name = "Harold Alexander"
   			id = 995
   			picture = "Portrait_Britain_Harold_Alexander.dds"
   			traits = { offensive_doctrine organisational_leader trickster infantry_leader infantry_expert organizer thorough_planner logistics_wizard  trait_mountaineer desert_fox   }
   			skill = 8
   			attack_skill = 7
   			defense_skill = 4 
    		planning_skill = 6
    		logistics_skill = 7
			}
			create_corps_commander = {
    		name = "Oliver Leese"
			id = 993
   			picture = "Portrait_Britain_Oliver_Leese.dds"
   			traits = {  panzer_leader panzer_expert trait_mountaineer desert_fox trait_engineer harsh_leader trickster expert_improviser}
  			skill = 8
   			attack_skill = 7
    		defense_skill = 5
    		planning_skill = 6
    		logistics_skill = 7
			}
		}
		FRA = {
			create_corps_commander = {
				name = "Leo Arthur"
				picture = "Portrait_France_Generic_land_5.dds"
				traits = { organizer offensive_doctrine  infantry_leader infantry_expert organizer trait_mountaineer desert_fox   }
				skill = 6
				attack_skill = 7
				defense_skill = 3
				planning_skill = 6
				logistics_skill = 7
			 }
   
		 create_field_marshal = {
				name = "Liam Gabriel"
				picture = "Portrait_France_Jean-Pierre_Esteva.dds"
				traits = { offensive_doctrine organisational_leader trickster infantry_leader infantry_expert organizer thorough_planner logistics_wizard  trait_mountaineer desert_fox   }
				skill = 6
				attack_skill = 7
				defense_skill = 4
			 planning_skill = 6
			 logistics_skill = 7
		 	}
			every_unit_leader = {
				limit={has_id=101}
				add_skill_level=1
				add_attack=2
				add_logistics = 2
				add_planning = 4
			}
			every_unit_leader = {
				limit={has_id=103}
				add_skill_level=1
				add_attack=3
				add_planning = 3
				add_unit_leader_trait= urban_assault_specialist
				add_unit_leader_trait= hill_fighter
			}
		}
		CAN = {
			create_corps_commander = {
				name = "George Thomas"
				picture = "Portrait_Britain_Jock_Campbell.dds"
				traits = { organizer offensive_doctrine  infantry_leader infantry_expert organizer trait_mountaineer desert_fox   }
				skill = 6
				attack_skill = 7
				defense_skill = 3
				planning_skill = 6
				logistics_skill = 7
			 }
   
		 create_field_marshal = {
			name = "Liam Alexander"
			picture = "Portrait_Britain_Generic_land_1.dds"
			traits = { offensive_doctrine organisational_leader trickster infantry_leader infantry_expert organizer thorough_planner logistics_wizard  trait_mountaineer desert_fox   }
			skill = 6
			attack_skill = 7
			defense_skill = 4
			planning_skill = 6
			logistics_skill = 7
			 }
			
			every_unit_leader = {
				limit={has_id=104}
				add_skill_level=1
				add_planning = 4
				add_unit_leader_trait=ranger
				add_unit_leader_trait=trait_mountaineer
			}
			every_unit_leader = {
				limit={has_id=105}
				add_skill_level=1
				add_attack=3
				add_planning = 3
				add_unit_leader_trait= hill_fighter
			}
		}
		AST = {
			create_corps_commander = {
   			name = "Desert Cope"
   			picture = "Portrait_Britain_George_Giffard.dds"
   			traits = { hill_fighter organizer offensive_doctrine  infantry_leader infantry_expert organizer trait_mountaineer desert_fox   }
   			skill = 8
   			attack_skill = 7
   			defense_skill = 3
   			planning_skill = 6
   			logistics_skill = 7
			}
  
		create_field_marshal = {
   			name = "Donaldenius"
   			picture = "Portrait_Britain_Harold_Alexander.dds"
   			traits = { offensive_doctrine organisational_leader trickster infantry_leader infantry_expert organizer thorough_planner logistics_wizard  trait_mountaineer desert_fox   }
   			skill = 8
   			attack_skill = 7
   			defense_skill = 4 
    		planning_skill = 6
    		logistics_skill = 7
			}
			create_corps_commander = {
    		name = "Shalluska"
   			picture = "Portrait_Britain_Oliver_Leese.dds"
   			traits = {  panzer_leader panzer_expert trait_mountaineer desert_fox trait_engineer harsh_leader trickster expert_improviser}
  			skill = 8
   			attack_skill = 7
    		defense_skill = 5
    		planning_skill = 6
    		logistics_skill = 7
			}			
		}		
			}
		}
	}

	GER_fall_blau_mission = {
		name = "Fall Blau"
		icon = GFX_decision_generic_army_support
		days_mission_timeout = 100
		days_remove = 100
		is_good = no
		available = {   
			any_country = {
				is_in_faction_with = GER
				controls_state = 217		
			}
		}
		allowed = {
			original_tag = GER
		}
		fire_only_once = yes
		visible = {	
			always = no
		} 
		activation = {
			GER = {
				has_completed_focus = "Fall Blau" 
			}
		}
		complete_effect = {
			add_ideas = GER_fall_blau_success
		}
		timeout_effect = {
			add_manpower = -500000
			remove_ideas = GER_fall_blau_idea
			add_timed_idea = {
				idea = GER_fall_blau_failed
				days = 120
			}
		}
	}

	"Annex and build up Iraq" = {
		
		icon = GFX_decision_generic_construction

		allowed = {
		}
	available = {
			OR = {
				controls_state = 291 
				any_other_country = {
				is_in_faction_with = GER
				controls_state = 291}
			}
		}

	fire_only_once = yes
	visible = {
			original_tag = GER
			always = yes
	}
	complete_effect = {
			291 = { 
				set_building_level = {
				type = infrastructure
				level = 10
				instant_build = yes
				}		
			}
			ITA = { 
				transfer_state = 291
				transfer_state = 656

				transfer_state = 554 
			}
			hidden_effect = {
				activate_mission = GER_fuel_gain_from_africa
				GER = {africa_fuel_gain_start = yes}
			} 
		}
		
	}

GER_fuel_gain_from_africa = {
	icon = generic_operation
	allowed = { tag = GER }
	available = {	ENG = { controls_state = 291 }	} 
		days_mission_timeout = 30
		fire_only_once = no
		is_good = yes
		activation = { always = no }
		timeout_effect = { 
		custom_effect_tooltip = "We will improve our fuel gained from refineries by 5%"
			GER = {africa_fuel_gain = yes}  
			activate_mission = GER_fuel_gain_from_africa
			
		}
	complete_effect = {		}
}

	#"Annex Spain" = {
#
#		icon = GFX_decision_generic_operation
#
#		available = {
#			SPR = {is_ai = yes}
#			NOT = {SPR = {has_country_flag = spr_annexed}}
#			}
#			
#			allowed = {	original_tag = GER }
#			fire_only_once = no
#			cost = 0
#			visible = { NOT = {SPR = {has_country_flag = spr_annexed}	} }
#		complete_effect = {	
#			GER = { country_event = { id = germany.112 }}
#		}
#	}

	send_air_to_ita = { 
	
		icon = GFX_decision_generic_air
			
			allowed = {
		}
		
		available = {} 
		fire_only_once = no
		visible = {
			original_tag = GER
			NOT = {has_country_flag = GER_air_LL}
		}
		
		complete_effect = {
			activate_mission = send_air_to_ita1 set_country_flag = GER_air_LL
		}			
	}
	
	send_air_to_ita1 = {
	
		icon = GFX_decision_generic_air
			
			allowed = {
		}
		
		available = {NOT = {has_country_flag = GER_air_LL}} 
		fire_only_once = no
		visible = {
			original_tag = GER
			always = yes
		}
		days_mission_timeout = 7
		is_good = yes
		activation = { always = no	}
		
		timeout_effect = {
			if = { 
				limit =  { has_country_flag = GER_air_LL 
				}
			air_to_italy = yes
			activate_mission = send_air_to_ita1
			}
		}	
	}
	
	cancel_air_to_ita = {
	
		icon = GFX_decision_generic_air
			
			allowed = {
		}
		
		available = {always = yes} 
		fire_only_once = no
		visible = {
			original_tag = GER
			has_country_flag = GER_air_LL
		}
	
		complete_effect = {
			if = { 
			limit =  { has_country_flag = GER_air_LL 
				}
				clr_country_flag = GER_air_LL
			}
		}	
	}

	"Annex Romania" = {

		icon = generic_operation

		available = {date > 1936.1.1
			ROM = {is_ai = yes}
			country_exists = ROM
			}
			
			allowed = {	original_tag = GER }
			fire_only_once = yes
			cost = 0
			visible = { always = yes	} 
		complete_effect = {	
			ITA = {
				add_research_slot = 1
			}
			ROM = {set_country_flag = rom_annexed}
			every_state = {
				limit = {
					is_core_of = ROM
				}
				set_building_level = {
					type = arms_factory
					level = 0
					instant_build = yes
				}
				set_building_level = {
					type = industrial_complex
					level = 0
					instant_build = yes
				}
				set_building_level = {
					type = dockyard
					level = 0
					instant_build = yes
				}
			}
			
			43 = {
				add_extra_state_shared_building_slots = 3
				add_building_construction = {
					type = arms_factory
					level = 3
					instant_build = yes
				}
			}	
			43 = {
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
			}	
			64 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
			}	
			64 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = industrial_complex
					level = 2
					instant_build = yes
				}
			}
			2 = {
				add_extra_state_shared_building_slots = 5
				add_building_construction = {
					type = arms_factory
					level = 5
					instant_build = yes
				}
			}	
			2 = {
				add_extra_state_shared_building_slots = 5
				add_building_construction = {
					type = industrial_complex
					level = 5
					instant_build = yes
				}
			}	
		}
	}

	"Annex Hungary" = {

		icon = generic_operation

		available = {   
			NOT = {
				country_exists = CZE
			}
			NOT = {
				country_exists = YUG
			}
			HUN = {is_ai = yes}
				}
			allowed = {	original_tag = GER }
			fire_only_once = yes
			cost = 0
			visible = { always = yes	} 
		complete_effect = {	
			GER = {annex_country = { target = HUN transfer_troops = yes }}
		}		
	}
}



