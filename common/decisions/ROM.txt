"TFU War Decisions Romania" = {

	"Deploy Norway Garrison" = {

		icon = GFX_decision_generic_army_support

		available = {   
			GER = {controls_state = 110}
			has_manpower > 500000
		}
			allowed = {	original_tag = ROM }
			fire_only_once = yes
			cost = 0
			visible = { always = yes	} 
			complete_effect = {	
				hidden_effect = {
				add_manpower  = -500000
				load_oob = "ROM_norway"
			}
		}
	}	

}