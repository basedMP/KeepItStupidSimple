"TFU War Decisions USA" = {
	
		send_air_to_uk = {
	
		icon = GFX_decision_generic_air
			
			allowed = {
		}
		
		available = {} 
		fire_only_once = no
		visible = {
			original_tag = USA
			NOT = {has_country_flag = USA_air_LL}
		}
		
		complete_effect = {
			activate_mission = send_air_to_uk1 set_country_flag = USA_air_LL
		}			
	}
	
	send_air_to_uk1 = {
	
		icon = GFX_decision_generic_air
			
			allowed = {
		}
		
		available = {NOT = {has_country_flag = USA_air_LL}} 
		fire_only_once = no
		visible = {
			original_tag = USA
			always = yes
		}
		days_mission_timeout = 7
		is_good = yes
		activation = { always = no	}
		
		timeout_effect = {
			if = { 
				limit =  { has_country_flag = USA_air_LL 
				}
			air_to_uk = yes
			activate_mission = send_air_to_uk1
			}
		}	
	}
	
	cancel_air_to_uk = {
	
		icon = GFX_decision_generic_air
			
			allowed = {
		}
		
		available = {always = yes} 
		fire_only_once = no
		visible = {
			original_tag = USA
			has_country_flag = USA_air_LL
		}
	
		complete_effect = {
			if = { 
			limit =  { has_country_flag = USA_air_LL 
				}
				clr_country_flag = USA_air_LL
			}
		}	
	}
	
}

"USA War Plans" = {
		
	war_plan_orange = {
	
		icon = GFX_decision_generic_intelligence_operation
			
		allowed = {
		}
		
		available = {
			NOT = {has_country_flag = USA_recent_wp }
		} 
		fire_only_once = no
		visible = {
			original_tag = USA
			has_country_flag = USA_unlock_wp_red
		}

		days_remove = 100
		
		complete_effect = {
			add_timed_idea = {
				idea = USA_war_plan_orange
				days = 45
			}
			set_country_flag = USA_recent_wp
		}		
		remove_effect = {
			clr_country_flag = USA_recent_wp
		}

	}
	defense_plan_orange = {
	
		icon = GFX_decision_eng_support_imperialist_coup
			
		allowed = {
		}
		
		available = {
			NOT = {has_country_flag = USA_recent_wp }
		} 
		fire_only_once = no
		visible = {
			original_tag = USA
			has_country_flag = USA_unlock_wp_red
		}

		days_remove = 100
		
		complete_effect = {
			add_timed_idea = {
				idea = USA_defense_plan_orange
				days = 45
			}
			set_country_flag = USA_recent_wp
		}		
		remove_effect = {
			clr_country_flag = USA_recent_wp
		}

	}

	war_plan_silver = {
	
		icon = GFX_decision_generic_intelligence_operation
			
		allowed = {
		}
		
		available = {
			NOT = {has_country_flag = USA_recent_wp }
		} 
		fire_only_once = no
		visible = {
			original_tag = USA
			has_country_flag = USA_unlock_wp_silver
		}

		days_remove = 100
		
		complete_effect = {
			add_timed_idea = {
				idea = USA_war_plan_silver
				days = 45
			}
			set_country_flag = USA_recent_wp
		}		
		remove_effect = {
			clr_country_flag = USA_recent_wp
		}

	}
	defense_plan_silver = {
	
		icon = GFX_decision_eng_support_imperialist_coup
			
		allowed = {
		}
		
		available = {
			NOT = {has_country_flag = USA_recent_wp }
		} 
		fire_only_once = no
		visible = {
			original_tag = USA
			has_country_flag = USA_unlock_wp_silver
		}

		days_remove = 100
		
		complete_effect = {
			add_timed_idea = {
				idea = USA_defense_plan_silver
				days = 45
			}
			set_country_flag = USA_recent_wp
		}		
		remove_effect = {
			clr_country_flag = USA_recent_wp
		}

	}

	war_plan_black = {
	
		icon = GFX_decision_generic_intelligence_operation
			
		allowed = {
		}
		
		available = {
			NOT = {has_country_flag = USA_recent_wp }
		} 
		fire_only_once = no
		visible = {
			original_tag = USA
			has_country_flag = USA_unlock_wp_black
		}

		days_remove = 100
		
		complete_effect = {
			add_timed_idea = {
				idea = USA_war_plan_black
				days = 45
			}
			set_country_flag = USA_recent_wp
		}		
		remove_effect = {
			clr_country_flag = USA_recent_wp
		}

	}
	defense_plan_black = {
	
		icon = GFX_decision_eng_support_imperialist_coup
			
		allowed = {
		}
		
		available = {
			NOT = {has_country_flag = USA_recent_wp }
		} 
		fire_only_once = no
		visible = {
			original_tag = USA
			has_country_flag = USA_unlock_wp_black
		}

		days_remove = 100
		
		complete_effect = {
			add_timed_idea = {
				idea = USA_defense_plan_black
				days = 45
			}
			set_country_flag = USA_recent_wp
		}		
		remove_effect = {
			clr_country_flag = USA_recent_wp
		}

	}


}