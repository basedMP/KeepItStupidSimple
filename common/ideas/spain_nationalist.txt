ideas = {
	country = {
		SPR_german_aid = {
		
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			picture = generic_production_bonus
			
			modifier = {
				consumer_goods_factor = -0.05
			}
		}

		SPR_italian_aid = {
		
			name = "Italian Aid"
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			picture = generic_production_bonus
			
			modifier = {
				production_factory_efficiency_gain_factor = 0.35
				production_factory_max_efficiency_factor = 0.1
				production_factory_start_efficiency_factor = 0.1
				industrial_capacity_factory = 0.2
				line_change_production_efficiency_factor = 0.1
			}
		}

	SPR_sustain_hp = {
		
			name = "Improve our Production"
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			picture = generic_production_bonus
			
			modifier = {
				industrial_capacity_factory = 0.15
				
			}
		}

		SPR_construction_effort = {
		
			name = "Construction Effort"
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			picture = generic_production_bonus
			
			modifier = {
				consumer_goods_factor = -0.01
				production_speed_buildings_factor = 0.05
			}
		}

		SPR_Mechanized = {
		
			name = "Mechanized Infantry"
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			picture = generic_production_bonus
			
			equipment_bonus = {
				mechanized_equipment = {
					build_cost_ic = 0.1 defense = 0.1 instant = no
				}
			}
		}

		SPR_recycle_old_tank_models = {
		
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			picture = idea_por_iberian_workers_united
			
			modifier = {	
				equipment_conversion_speed = 0.25 
			}
		}

		SPR_pensamiento_tactico = {
		
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			picture = idea_por_iberian_workers_united
			
			modifier = {	
				max_planning = 0.1
				planning_speed = 0.2
			}
		}

		SPR_pensamiento_tactico2 = {
		name = "Pensamiento Táctico Mejorado"
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			picture = por_iberian_workers_united
			
			modifier = {	
				max_planning = 0.15
				planning_speed = 0.25
			    army_attack_factor = 0.025
			}
		}

			SPR_schivilwar = {
			name = "Divided from Civil War"
			removal_cost = -1
			
			allowed = {
				always = yes 
			}

			allowed_civil_war = {
				
			}
			
			picture = SPR_schivil

			modifier = {
				stability_factor = -0.2
			}
		}


			SPR_schivilwarfixed = {
			name = "A Stable State"
			picture = SPR_chadge
			removal_cost = -1
			
			allowed = {
				always = yes 
			}

			allowed_civil_war = {
				
			

			}
			
			modifier = {
				stability_factor = 0.05
			}
		}

		SPR_revenge = {
		name = "Spanish Revenge"	
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}
 
			available = {
			NOT = {controls_state = 456}
			}

			picture = generic_production_bonus
			
			modifier = {
				war_support_weekly = 0.015
				stability_weekly = 0.015
			}
				modifier = {
				army_attack_factor = 0.025
			}
		}
		
		SPR_fighters = {
		
			name = "Use African Veterans"
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			picture = generic_production_bonus
			
			modifier = {
                
					army_morale_factor = 0.1
					army_org_factor = 0.05
			
			}
		}
		
		SPR_civil_war_veterans = {
		
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			picture = idea_por_iberian_workers_united
			
			modifier = {
				conscription = 0.01
			}
		}

		SPR_the_duty_to_work = {
			picture = generic_production_bonus
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			
			
			modifier = {
				industrial_capacity_factory = 0.1
				min_export = -1.0
			}
		}

	SPR_the_duty_to_work_6 = {
	name = "The Duty to Work"
			picture = generic_production_bonus
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			
			
			modifier = {
				industrial_capacity_factory = 0.15
				min_export = -1.0
				consumer_goods_factor =-0.03
			}
		}

	SPR_the_duty_to_work_7 = {
		name = "The Duty to Work"
			picture = generic_production_bonus
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			
			
			modifier = {
				industrial_capacity_factory = 0.2
				min_export = -1.0
				consumer_goods_factor =-0.03
			}
		}

		SPR_the_duty_to_work2 = {
			name = "The Duty to Work"
			picture = generic_production_bonus
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			
			
			modifier = {
				industrial_capacity_factory = 0.1
				min_export = -1.0
				consumer_goods_factor = -0.03
			}
		}

		SPR_the_duty_to_work3 = {
			name = "The Duty to Work"
			picture = generic_production_bonus
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
		
			}

			
			
			modifier = {
				industrial_capacity_factory = 0.25
				min_export = -1.0
				consumer_goods_factor = -0.03
			}
		}
	}

	
	political_advisor = {

		SPA_tomas_dominguez_arevalo = {
			picture = SPA_tomas_dominguez_arevalo
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			available = {	date > 1936.7.18	}
			traits = { lifelong_carlist }
		}

		SPA_diego_hidalgo_y_duran = {

			picture = SPA_diego_hidalgo_y_duran

			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			available = { date > 1936.7.18 }
			traits = { war_industrialist }
		}


		SPA_luis_hernando_de_larramendi = {
			picture = SPA_luis_hernando_de_larramendi
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			available = {	date > 1936.7.18	}	
			traits = { traditionalist_theorist captain_of_industry }
		}
	}

	theorist = {
		SPA_jose_enrique_varela = { # Carlist
			ledger = army
				
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}

			
			picture = SPA_jose_enrique_varela
			
			research_bonus = {
				land_doctrine = 0.35
			}
			
			traits = { military_theorist }
		}
	}

	# MILITARY
	army_chief = {
		SPA_juan_yague = { # Falangist
			picture = SPA_juan_yague
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			traits = { army_chief_offensive_3 }
		}

		SPA_rafael_garcia_valino = { # Francoist/Carlist
			picture = SPA_rafael_garcia_valino
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}

			traits = { army_chief_organizational_2 }
		}

		SPA_heli_rolando_de_tella = { # Francoist/Carlist
			picture = SPA_heli_rolando_de_tella
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			traits = { army_chief_defensive_2 }
		}
	}
	air_chief = {
		SPA_joaquin_garcia_morato = { 
			picture = SPA_joaquin_garcia_morato
			allowed = { original_tag = SPR	}
			traits = { air_chief_ground_support_2 }
		}
	}

	materiel_manufacturer = {
		designer = yes
		SPR_star_bonifacio_echeverria = {
			picture = SPR_star_bonifacio_echeverria
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			research_bonus = {
				infantry_weapons = 0.15
			}
			on_add = { hidden_effect = { mech_designers_free = yes } }
			traits = { infantry_equipment_manufacturer }
		}

		SPR_esperanza_y_cia = {
			picture = SPR_esperanza_y_cia
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}	
			on_add = { hidden_effect = { mech_designers_free = yes } }
			research_bonus = {	artillery = 0.15	}	
			traits = { artillery_manufacturer }
		}

		SPR_llama_gabilondo_y_cia = {
			picture = SPR_llama_gabilondo_y_cia_sa
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			on_add = { hidden_effect = { mech_designers_free = yes } }
			research_bonus = {
				support_tech = 0.15
			}
			traits = { support_equipment_manufacturer }
		}
		SPR_hispano_suiza = {
			picture = SPR_hispano_suiza
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			on_add = { hidden_effect = { mech_designers_free = yes } }
			research_bonus = {	motorized_equipment = 0.15	}
			traits = { motorized_equipment_manufacturer }
		}

	}


	industrial_concern = {
		SPR_compania_telefonica_nacional = {
			picture = SPR_compania_telefonica_nacional
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}		
			research_bonus = {	electronics = 0.15	}	
			traits = { electronics_concern }
		}

		SPR_industria_de_guerra_de_cataluna = {
			picture = SPR_industrias_de_guerra_de_cataluna
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			traits = { construction_company }
		}

		SPR_altos_hornos_de_vizcaya = {
			picture = SPR_altos_hornos_de_vizcaya
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			research_bonus = {	industry = 0.15	}
			traits = { industrial_concern }
		}

		SPR_campsa = {
			picture = SPR_campsa
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}		
			research_bonus = {
				synth_resources = 0.15
				industry = 0.1
			}
			traits = { refinery_concern }
		}
	}
		
		
	high_command = {
		SPA_emilio_mola = { # Falangist/Francoist
			ledger = army
			picture = SPA_emilio_mola
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			traits = { army_infantry_2 }
		}
		SPA_marco_canurco = { # Falangist/Francoist
			ledger = army
			picture = SPA_wilhelm_ritter_von_thoma
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			traits = { army_commando_2 }
		}

		SPA_wilhelm_ritter_von_thoma = { # Falangist/Francoist
			ledger = army
			picture = SPA_wilhelm_ritter_von_thoma
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			available = {}
			traits = { army_armored_2 }
		}
		SPA_enrique_canovas_lacruz = { # Francoist
			ledger = army
			picture = SPA_enrique_canovas_lacruz
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			traits = { army_logistics_2 }
		}
		SPA_miguel_cabanellas = { # Carlist
			ledger = army
			picture = SPA_miguel_cabanellas
			allowed = {
				has_dlc = "La Resistance"
				original_tag = SPR
			}
			traits = { army_regrouping_2 }
		}
	}
}