ideas = {

	country = {
		SPR_seized_spanish_gold_reserves = {
			picture = SPR_seize_the_gold_reserves
			allowed = {	original_tag = SOV	}
			allowed_civil_war = {	always = yes	}
			modifier = {
				consumer_goods_factor = -0.05
				industrial_capacity_factory = 0.05
			}
		}

		

		SOV_man_of_steel = {
		
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
			NOT = {controls_state = 456}
			}

			picture = idea_por_iberian_workers_united
			
			modifier = {
				army_core_attack_factor = 0.075
				army_core_defence_factor = 0.075
				industrial_capacity_factory	= 0.05
				army_org_Factor = 0.1
				army_morale_factor = 0.1
			}
		}

		SOV_support_equipment = {
			removal_cost = -1
			name = "Shovel Production"
			allowed = {
				always = no # Added via focus
			}

			available = {
			}

			picture = eng_for_the_good_of_the_revolution
			
			equipment_bonus = {
				support_equipment = {
					build_cost_ic = -0.25 instant = yes
				}
			}
		}

		SOV_gun_production = {
			removal_cost = -1
			name = "Gun Production"
			allowed = {
				always = no # Added via focus
			}

			available = {
			}

			picture = disarmed_nation
			
			equipment_bonus = {
				infantry_equipment = {
					build_cost_ic = -0.2 instant = yes
				}
			}
		}

		SOV_capital_ship = {
			removal_cost = -1
			name = "Capital Ship Effort"
			allowed = {
				always = no # Added via focus
			}

			available = {
			}

			picture = disarmed_nation
			
			equipment_bonus = {
				capital_ship	= { build_cost_ic = -0.3 instant = yes }
			}
		}

		SOV_submarine = {
			removal_cost = -1
			name = "Submarine Effort"
			allowed = {
				always = no # Added via focus
			}

			available = {
			}

			picture = disarmed_nation
			
			equipment_bonus = {
				submarine		= { build_cost_ic = -0.1 instant = yes }
			}
		}

		SOV_socialist_realism = {
			
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}
 
			available = {
			NOT = {controls_state = 456}
			}

			picture = generic_production_bonus
			
			modifier = {
				stability_weekly = 0.015
				political_power_gain = 0.5
			}
		}

		SOV_small_barb_modifier = {
		
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
			}

			picture = generic_air_payment
			
			modifier = {
				air_range_factor = 2
			}

			equipment_bonus = {
			
			}
		}

		SOV_t34 = {
			name = "T-34-85"
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			available = {
			}

			picture = generic_production_bonus

			research_bonus = {
				armor = -0.8
			}
			
			modifier = {
				license_production_speed = -0.5
			}
		}
		
		militarized_schools = {
		
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				NOT = {
					has_government = democratic
				}
			}

			picture = generic_manpower_bonus
			
			modifier = {
				conscription = 0.035
			}
		}
		
		workers_culture = {
			
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}

			picture = generic_production_bonus
			
			modifier = {
				production_speed_industrial_complex_factor = 0.1
				consumer_goods_factor = -0.02
				production_speed_arms_factory_factor = 0.15
			}
		}

		SOV_long_term_investments = {
			
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}

			picture = generic_production_bonus
			
			modifier = {
				production_factory_max_efficiency_factor = 0.05
				production_factory_efficiency_gain_factor = -0.15
				production_factory_start_efficiency_factor = -0.075
				conversion_cost_civ_to_mil_factor = -0.1
				conversion_cost_mil_to_civ_factor = -0.1
			}
		}

		SOV_medium_term_investments = {
			
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}

			picture = generic_production_bonus
			
			research_bonus = {
				industry = 0.1
			}

			modifier = {
				line_change_production_efficiency_factor = 0.15
				production_factory_start_efficiency_factor = 0.02

			}
		}

		SOV_short_term_investments = {
			
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}

			picture = generic_production_bonus
			
			

			modifier = {
				production_factory_efficiency_gain_factor = 0.325
				production_factory_max_efficiency_factor = -0.05
				production_factory_start_efficiency_factor = 0.1
				line_change_production_efficiency_factor = 0.1
				production_speed_arms_factory_factor = 0.1
			}
		}

		SOV_allied_help = {
			name = "Allied Help"
			picture = ai_limiter
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}

			picture = generic_production_bonus
			
			modifier = {
				production_factory_efficiency_gain_factor = 0.25
				production_speed_arms_factory_factor = 0.1
				conversion_cost_civ_to_mil_factor = -0.2
			}
		}

		SOV_at_production = {
			
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}

			picture = generic_production_bonus
			
			equipment_bonus = {
				anti_tank = { build_cost_ic = -0.1 instant = yes }
			}
		}

		SOV_HA_production = {
			name = "ZiS-3"
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}

			picture = generic_production_bonus
			
			equipment_bonus = {
				anti_tank = { hard_attack = 0.1 instant = yes }
			}
		}

		SOV_arty_production = {
			name = "Soviet Artillery Production"
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}

			picture = generic_production_bonus
			
			equipment_bonus = {
				artillery = { build_cost_ic = -0.2 soft_attack = 0.1 instant = yes }
			}
		}
	
		partisan_suppression_focus = {
			
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				NOT = {
					has_government = democratic
				}
			}

			picture = generic_intel_bonus
			
			modifier = {
				resistance_damage_to_garrison = -0.25 
			}
		}

		SOV_prisoners_of_war = {
			
			removal_cost = -1
			name = "Prisoners of War"
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				NOT = {
					has_government = democratic
				}
			}

			picture = generic_intel_bonus
			
			modifier = {
				production_factory_efficiency_gain_factor = 0.25
				industrial_capacity_factory = 0.05
			}
		}

		SOV_the_gulag = {
			name = "The Gulag"
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				NOT = {
					has_government = democratic
				}
			}

			picture = generic_intel_bonus
			
			modifier = {
				production_speed_buildings_factor = 0.1
				consumer_goods_factor = -0.03
			}
		}

		smersh = {
			allowed = {
				always = no # Added via focus
			}
			name = SOV_smersh

			removal_cost = -1
			
			modifier = {
				operative_slot = 1
				intelligence_agency_defense = 0.5
			}
		}

		nkvd = {
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			modifier = {
			}
		}

		nkvd_2 = {
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			modifier = {
				land_reinforce_rate = 0.12
			}
		}

		nkvd_3 = {
			removal_cost = -1
			name = "NKVD"
			picture = generic_manpower_bonus
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			modifier = {
				land_reinforce_rate = 0.12
				dig_in_speed_factor = 1
			}
		}
		
		comintern_influence = {
			
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				always = yes
			}

			picture = generic_communism_drift_bonus
			
			modifier = {
				communism_drift = 0.05
			}
		}

		progress_cult_focus = {
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				always = yes
			}
			
			picture = generic_research_bonus
			
			modifier = {
				research_speed_factor = 0.1
				
			}
		}
		progress_cult_focus2 = {
			removal_cost = -1
			name = "Progress Cult"
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				always = yes
			}
			research_bonus = {
				industry = 0.25
			}
			picture = generic_research_bonus
			
			modifier = {
				research_speed_factor = 0.15
				
			}
		}

		socialist_science_focus = {
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}

			picture = generic_manpower_bonus
			
			modifier = {
				conscription = 0.03
			}
		}

		peoples_commissariat_focus = {
			allowed = {
				always = no
			}

			removal_cost = -1

			picture = generic_production_bonus

			allowed_civil_war = {
				has_government = communism
			}

			modifier = {
				conversion_cost_civ_to_mil_factor= -0.2
			}
		}

		rehabilitated_military_focus = {
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				always = yes
			}

			picture = generic_morale_bonus
			
			modifier = {
				land_reinforce_rate = 0.02
				army_morale_factor = 0.10
			}
		}
		
		home_of_revolution = {
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			modifier = {
				industrial_capacity_factory = 0.1
				industrial_capacity_dockyard = 0.1
				consumer_goods_factor = -0.10
				production_speed_buildings_factor = 0.1
				
			}
		}

		home_of_revolution2 = {
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			modifier = {
				industrial_capacity_factory = 0.2
				industrial_capacity_dockyard = 0.15
				consumer_goods_factor = -0.10
				production_speed_buildings_factor = 0.1
				production_speed_arms_factory_factor = 0.2
			}
		}

		trotskyite_plot = {
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			modifier = {
				stability_factor = -0.2
			}
		}
		
		trotskyite_plot_purged = {
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			modifier = {
				stability_factor = -0.05
			}
		}

		officers_purged = {
			removal_cost = -1
			name = "Purged Officers"
			picture = GFX_idea_officers_purged
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			research_bonus = {
				land_doctrine = -0.25
			}

			modifier = {
				army_org_factor = -0.5
				planning_speed = -0.5
				max_planning_factor = -0.2
			}
		}
		officers_purged2 = {
			removal_cost = -1
			name = "Purged Officers"
			picture = GFX_idea_officers_purged
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}

			research_bonus = {
			
			}

			modifier = {
				army_org_factor = -0.05
				planning_speed = -0.1
				max_planning_factor = -0.1
			}
		}
		officers_purged3 = {
			removal_cost = -1
			name = "Officer Schools"
			picture = GFX_idea_officers_purged
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}

			research_bonus = {
				
			}

			modifier = {
				planning_speed = 0.25
				max_planning_factor = 0.05
				army_speed_factor = 0.1
			}
		}



		SOV_great_patriotic_war = {
			removal_cost = -1

			picture = generic_intel_bonus
			
			allowed = {
				always = no # Added via event
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			research_bonus = {
				land_doctrine = 0.05
				naval_doctrine = 0.05
				air_doctrine = 0.05
			}

			modifier = {
				army_org_factor = 0.45
				war_support_factor = 0.2
			}
		}

		SOV_great_patriotic_war_2 = {
			removal_cost = -1

			picture = generic_intel_bonus
			
			allowed = {
				always = no # Added via event
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			research_bonus = {
				land_doctrine = 0.04
				naval_doctrine = 0.04
				air_doctrine = 0.04
			}

			modifier = {
				army_org_factor = 0.35
				war_support_factor = 0.15
			}
		}

		SOV_great_patriotic_war_3 = {
			removal_cost = -1
			
			picture = generic_intel_bonus
			
			allowed = {
				always = no # Added via event
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			research_bonus = {
				land_doctrine = 0.03
				naval_doctrine = 0.03
				air_doctrine = 0.03
			}

			modifier = {
				army_org_factor = 0.25
				war_support_factor = 0.1
			}
		}

		SOV_great_patriotic_war_4 = {
			removal_cost = -1
			
			picture = generic_intel_bonus

			allowed = {
				always = no # Added via event
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			research_bonus = {
				land_doctrine = 0.02
				naval_doctrine = 0.02
				air_doctrine = 0.02
			}

			modifier = {
				army_org_factor = 0.15
				war_support_factor = 0.05
			}
		}

		SOV_great_patriotic_war_5 = {
			removal_cost = -1
			
			picture = generic_intel_bonus

			allowed = {
				always = no # Added via event
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			research_bonus = {
				land_doctrine = 0.01
				naval_doctrine = 0.01
				air_doctrine = 0.01
			}

			modifier = {
				army_org_factor = 0.05
			}
		}


		SOV_molotov_ribbentrop_pact = { 
			removal_cost = -1
			
			picture = soviet_german_friendship

			allowed = {
				always = no # Added via event
			}

			allowed_civil_war = {
				has_government = communism
			}

		}

		SOV_industrial_supremacy = { 
			removal_cost = -1
			
			picture = generic_communism_drift_bonus

			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communism
			}
			
			modifier = {
				industrial_capacity_factory	= 0.2
			}

		}

		SOV_disorganized_army = { 
			removal_cost = -1
			
			picture = generic_communism_drift_bonus

			allowed = {
				always = yes
			}
			
			modifier = {
				army_morale_factor = -0.05
				army_org_factor = -0.05
				air_defence_factor = 0.2
			}
		}

		SOV_disorganized_army2 = { 
			removal_cost = -1
			
			picture = generic_communism_drift_bonus

			allowed = {
				always = yes
			}
			
			modifier = {
				air_defence_factor = 0.2
			}

		}

		SOV_disorganized_army3 = { 
			removal_cost = -1
			
			picture = generic_communism_drift_bonus

			allowed = {
				always = yes
			}
			
			modifier = {
				army_org_Factor = 0.1
				army_morale_factor = 0.1
				air_defence_factor = 0.2
			}

		}

		SOV_winter_offensive1 = { 
			removal_cost = -1
			
			picture = generic_communism_drift_bonus

			allowed = {
				always = yes
			}
			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 	0.1
				army_org_Factor = 0.05
			}

		}

		SOV_winter_offensive2 = { 
			removal_cost = -1
			
			picture = generic_communism_drift_bonus

			allowed = {
				always = yes
			}
			
			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 	0.1
				army_org_Factor = 0.05
				planning_speed = 0.5
				army_morale_factor = 0.1
			}

		}

		SOV_winter_offensive3 = { 
			removal_cost = -1
			
			picture = generic_communism_drift_bonus

			allowed = {
				always = yes
			}
			
			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 	0.1
				army_org_Factor = 0.15
				planning_speed = 0.5
				army_morale_factor = 0.2
			}

		}

		SOV_winter_offensive4 = { 
			removal_cost = -1
			
			picture = generic_communism_drift_bonus

			allowed = {
				always = yes
			}
			
			modifier = {
				army_core_attack_factor = 0.15
				army_core_defence_factor = 	0.15
				army_org_Factor = 0.2
				planning_speed = 0.5
				army_morale_factor = 0.2
			}

		}

		SOV_expanded_tank_production = {

			picture = generic_research_bonus

			allowed = {
				always = no # Added via event
			}

			research_bonus = {
				armor = -0.75
			}

		}
	}


	political_advisor = {
	
		mikhail_kalinin = {
			
			
			allowed = {
				original_tag = "SOV"
			}
			available = 
			{
				NOT = { has_country_flag = purged_kalinin }
				NOT = { has_country_flag = counter_purge }
			}
			

			
			traits = { popular_figurehead }
		}

		SOV_nikolai_voznesensky = {
			
			picture = generic_political_advisor_europe_2
			
			allowed = {
				original_tag = SOV
			}

			available = {
				has_completed_focus = "Long-Term Investments"
			}
			
			traits = { captain_of_industry }
		}

		SOV_julaska_kowalski = {
			
			picture = generic_political_advisor_europe_3
			
			allowed = {
				original_tag = SOV
			}
			
			available = {
				has_completed_focus = "Short-Term Investments"
			}

			traits = { war_industrialist }
		}

		SOV_sergei_korolev = {

			picture = sergei_korolev
			
			allowed = {
				original_tag = SOV
			}
			
			available = {
				has_completed_focus = "Medium-Term Investments"
			}

			traits = { lieutenant_of_industry war_profiteer }

		}

		
		
		nikita_khrushchev = {
			
			
			allowed = {
				original_tag = "SOV"
			}
			available = 
			{
				NOT = { has_country_flag = purged_khrushchev }
				NOT = { has_country_flag = counter_purge }
				if = {
					limit = { has_dlc = "Man the Guns" }	
					NOT = { has_autonomy_state = autonomy_supervised_state }
				}
			}
			

			
			traits = { communist_revolutionary }
	
			do_effect = {
				NOT = {
					has_government = communism
				}
			}
			
			ai_will_do = {
				factor = 0
			}
		}	
	}
		
	army_chief = {
		
		kliment_voroshilov = {
			#Also unit leader: 405
			
			allowed = {
				original_tag = SOV
			}
			available = 
			{
				NOT = { has_country_flag = purge_1_group_b }
				has_completed_focus = SOV_positive_heroism
				hidden_trigger = {
					NOT = { 
						any_other_country = {
							has_war_with = SOV
							original_tag = SOV
							has_unit_leader = 405
						} 
					}
				}
			}
			

			
			traits = { army_entrenchment_3 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		vasily_blucher = {
			
			
			allowed = {
				original_tag = SOV

			}
			available = 
			{
				NOT = { has_country_flag = purge_1_group_a }
			}
			

			
			# PURGED
			traits = { army_chief_maneuver_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		mikhail_tukhachevsky = {
			allowed = {
				original_tag = SOV

			}
			available = 
			{ has_completed_focus = "The Second Winter Counter Offensive"
			}

			traits = { army_chief_offensive_1 }
			
			ai_will_do = {
				factor = 1
			}			
		}	
	}
	navy_chief = {
		
		
	}
	
	air_chief = {
		
		
			
	}
	
	
	high_command = {
	
		ivan_konev = {
			ledger = army
			#Also unit leader: 408
			
			allowed = {
				original_tag = SOV
			}
			available = 
			{
				NOT = { has_country_flag = counter_purge }
				custom_trigger_tooltip = {
				tooltip = SOV_konev_civil_war
					NOT = { 
						any_other_country = {
							has_war_with = SOV
							original_tag = SOV
							has_unit_leader = 408
						}
					}
				}
			}
			

			
			traits = { army_infantry_4 }
			# Almost certainly a psychopath
			
			ai_will_do = {
				factor = 1
			}
		}
		
		aleksandr_vasilevsky = {
			ledger = army
			#Also unit leader: 407
			
			allowed = {
				original_tag = SOV

			}
			available = 
			{
				NOT = { has_country_flag = purge_2_group_b }
				hidden_trigger = {
					NOT = { 
						any_other_country = {
							has_war_with = SOV
							original_tag = SOV
							has_unit_leader = 407
						} 
					}
				}
			}
			

			
			traits = { army_regrouping_1 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		konstantin_rokossovsky = {
			ledger = army
			#Also unit leader: 402
			
			allowed = {
				original_tag = SOV # Unlocked via focus
				hidden_trigger = {
					NOT = {
						any_other_country = {
							has_war_with = SOV
							original_tag = SOV
							has_unit_leader = 403
						} 
					}
				}
			}
			available = 
			{
				NOT = { has_country_flag = purge_1_group_b }
				has_completed_focus = SOV_positive_heroism
			}
			

			
			# Not purged, but tried and tortured, then released
			traits = { army_armored_2 }
			
			ai_will_do = {
				factor = 1
			}
		}	

		pavel_zhigarev = {
			ledger = army
			
			allowed = {
				original_tag = SOV # Unlocked via focus

			}
			available = 
			{
				has_completed_focus = "Industrial Supremacy"
			}
			

			
			# Not purged, but tried and tortured, then released
			traits = { army_armored_1 }
			
			ai_will_do = {
				factor = 1
			}			
		}
	}
		
	tank_manufacturer = {
		
		designer = yes
		
		morozov_design_bureau = {
			
			
			allowed = {
				original_tag = SOV
			}

			on_add = { hidden_effect = { tank_designers_free = yes } }
			
			research_bonus = {
				armor = 0.15
			}
			
			traits = { fast_tank_manufacturer }
			
			modifier = {
			}
		}
		
		astrov_design_bureau = {
			
			
			allowed = {
				original_tag = SOV
			}
			
			on_add = { hidden_effect = { tank_designers_free = yes } }
			
			research_bonus = {
				armor = 0.15
			}
			
			traits = { soft_attack_manufacturer }
			
			modifier = {
			}
		}
		
		okmo = {
			
			
			allowed = {
				original_tag = SOV
			}
			
			on_add = { hidden_effect = { tank_designers_free = yes } }
			
			research_bonus = {
				armor = 0.15
			}
			
			traits = { hard_attack_manufacturer }
			
			modifier = {
			}
		}

		krasnoye_sormovo = {
			picture = krasnoye_sormovo
			
			allowed = {
				original_tag = SOV
			}
			
			on_add = { hidden_effect = { tank_designers_free = yes } }
			
			research_bonus = {
				armor = 0.15
			}
			
			traits = { breakthrough_designer }
			
			modifier = {
			}
		}

		kharkiv_diesel_factory = {
			picture = kharkiv_diesel_factory
			
			allowed = {
				original_tag = SOV
			}
			
			on_add = { hidden_effect = { tank_designers_free = yes } }
			
			research_bonus = {
				armor = 0.15
			}
			
			traits = { main_battle_tank }
			
			modifier = {
			}
		}

		uralvagonazavod = {
			picture = uralvagonazavod
			
			allowed = {
				original_tag = SOV
			}
			
			on_add = { hidden_effect = { tank_designers_free = yes } }
			
			research_bonus = {
				armor = 0.15
			}
			
			traits = { blitzkrieg_tank_designer }
			
			modifier = {
			}
		}

		uralmash = {
			picture = uralmash 
			
			allowed = {
				original_tag = SOV
			}
			
			on_add = { hidden_effect = { tank_designers_free = yes } }
			
			research_bonus = {
				armor = 0.15
			}
			
			traits = { mass_tank_designer }
			
			modifier = {
			}
		}
	}
	
	aircraft_manufacturer = {
		
		designer = yes
		
		mig_design_bureau = {
			
			
			allowed = {
				original_tag = SOV
			}
			

			
			research_bonus = {
				air_equipment = 0.15
			}
			
			traits = { light_aircraft_manufacturer_3 }
			
			modifier = {
			}
		}
	}
	
	industrial_concern = {
				
		stalingrad_tractor_factory = {
		
			
			allowed = {
				original_tag = SOV
			}
			

			
			research_bonus = {
				industry = 0.15
			}
			
			traits = { industrial_concern }
			
			modifier = {
			}
		}
		
		leningrad_polytechnical_institute = {
			
			
			allowed = {
				original_tag = SOV
			}
			

			
			research_bonus = {
				electronics = 0.15
			}
			
			traits = { electronics_concern }
			
			modifier = {
			}
		}
	}
	
	materiel_manufacturer = {
			
		designer = yes
		
		grabin_design_bureau = {
			
			on_add = { hidden_effect = { mech_designers_free = yes } }
			allowed = {
				original_tag = SOV
			}
			

			
			research_bonus = {
				artillery = 0.15
			}
			
			traits = { artillery_manufacturer }
			
			modifier = {
			}
		}	
		
	
		
		tula_arms_plant = {
			on_add = { hidden_effect = { mech_designers_free = yes } }
			
			allowed = {
				original_tag = SOV
			}
			

			
			research_bonus = {
				infantry_weapons = 0.15
			}
			
			traits = { infantry_equipment_manufacturer }
			
			modifier = {
			}
		}
		
		gaz = {
			
			on_add = { hidden_effect = { mech_designers_free = yes } }
			allowed = {
				original_tag = SOV
			}
			

			
			research_bonus = {
				motorized_equipment = 0.15
				mot_rockets = 0.50
			}
			
			traits = { motorized_equipment_manufacturer }
			
			modifier = {
			}
		}
	}
	
	naval_manufacturer = {
		
		designer = yes
		
		SOV_yarrow_shipbuilders = {
			
			allowed = {
				original_TAG = SOV
			}
			
			on_add = { hidden_effect = { naval_designers_free = yes } }
						
			research_bonus = {
				naval_equipment = 0.15
			}
			
			traits = { convoy_escort_naval_manufacturer }

		}
		
		SOV_harland_wolff = {
			
			allowed = {
				original_TAG = SOV
			}
						
			on_add = { hidden_effect = { naval_designers_free = yes } }
			
			research_bonus = {
				naval_equipment = 0.15
			}
			
			traits = { pacific_fleet_naval_manufacturer }
		}
		
		SOV_cammell_laird = {
			
			allowed = {
				original_TAG = SOV
			}
						
			on_add = { hidden_effect = { naval_designers_free = yes } }
			
			research_bonus = {
				naval_equipment = 0.15
			}
			
			traits = { atlantic_fleet_naval_manufacturer }
			
			modifier = {
			}
		}
		
		SOV_john_brown_company = {
			
			allowed = {
				original_TAG = SOV
			}
			
			on_add = { hidden_effect = { naval_designers_free = yes } }
			
			research_bonus = {
				naval_equipment = 0.15
			}
			
			traits = { coastal_defence_naval_manufacturer }
		}

		SOV_electric_boat_company = {

			allowed = {
				original_TAG = SOV
			}
			
			on_add = { hidden_effect = { naval_designers_free = yes } }


			research_bonus = {
				naval_equipment = 0.15
			}

			traits = { raiding_fleet_naval_manufacturer }
		}

	}
	
	theorist = {	
		
		boris_shaposhnikov = {
			ledger = army
			
			
			allowed = {
				original_tag = SOV
			}
			

			
			research_bonus = {
				land_doctrine = 0.35
			}
			
			traits = { military_theorist }
		}
		
	}
	
}