ideas = {

	country = {
		
		hun_victory_through_movement = {

			picture = generic_armor

			allowed_civil_war = {
				
			}
			
			modifier = {
				army_speed_factor = 0.1
				army_morale_factor = 0.1
				army_org_factor = 0.05
			}
		}

		hun_victory_through_firepower = {

			picture = generic_production_bonus

			allowed_civil_war = {
				
			}
			
			modifier = {
				army_armor_attack_factor = 0.05
			}
		}

		HUN_take_the_initiative = {
			name = "Swift Offensive"
			picture = hun_victory_through_firepower

			allowed_civil_war = {
				
			}
			
			modifier = {
				army_armor_attack_factor = 0.1
				army_armor_defence_factor = 0.1
			}
		}
		HUN_take_the_initiative2 = {
			name = "Lost Momentum"
			picture = hun_victory_through_firepower

			allowed_civil_war = {
				
			}
			
			modifier = {
				army_armor_attack_factor = -0.05
				army_armor_defence_factor = -0.15
				army_org_regain = -0.1
			}
		}

		HUN_take_the_initiative3 = {
			name = "Victory in the North"
			picture = hun_victory_through_firepower

			allowed_civil_war = {
				
			}
			
			modifier = {
				army_org_regain = 0.1
			}
		}

		HUN_riga_bounce = {
			name = "Riga Bounce"
			picture = generic_production_bonus

			allowed_civil_war = {
				
			}
			
			modifier = {
				industrial_capacity_factory	= -0.5
				production_factory_efficiency_gain_factor = -0.5
				production_factory_max_efficiency_factor = -0.5
				production_factory_start_efficiency_factor = -0.5
				research_speed_factor = -1
				army_armor_attack_factor = -1
				army_speed_factor = -1
			}
		}
		
		hun_infantry_ideas = {

			picture = generic_armor

			allowed_civil_war = {
				
			}
			
			modifier = {
				army_infantry_attack_factor = 0.05
				conscription = 0.05
			}
		}

		HUN_movement_training = {

			picture = generic_armor

			allowed_civil_war = {
				
			}
			
			modifier = {
				army_speed_factor = 0.1
			}
		}
		
		hun_improved_gun_production = {

			picture = generic_armor

			allowed_civil_war = {
				
			}
			
			equipment_bonus = {
				infantry_equipment = {
					build_cost_ic = -0.1 instant = yes
				}
			}
		}

		HUN_treaty_of_triannon = {

			available = {
				
			}

			modifier = {
				conscription = -0.005
				production_speed_arms_factory_factor = -0.5
			}
		}
		HUN_treaty_of_triannon_2 = {

			picture = HUN_treaty_of_triannon

			available = {
				
			}

			modifier = {
				conscription = -0.005
				production_speed_arms_factory_factor = -0.15	
			    production_speed_industrial_complex_factor = 0.1
				conversion_cost_mil_to_civ_factor = -0.2
			}
		}

		HUN_treaty_of_triannon_3 = {

			picture = HUN_treaty_of_triannon

			available = {
				
			}

			modifier = {
				conscription = 0.1
				production_speed_industrial_complex_factor = 0.1
				production_speed_arms_factory_factor = -0.15
				conversion_cost_civ_to_mil_factor = -0.2	
			}
		}

		HUN_trade_with_czech = {
			name = "Trade With Czechoslowakia"
			picture = HUN_treaty_of_triannon

			available = {
				country_exists = CZE
			}

			modifier = {
				consumer_goods_factor = -0.05
			}
		}

		HUN_trade_with_romania = {
			name = "Trade With Romania"
			picture = HUN_treaty_of_triannon

			available = {
				
			}

			modifier = {
				production_speed_buildings_factor = 0.1
				conversion_cost_mil_to_civ_factor = -0.2
			}
		}

		HUN_danube_trade_region = {
			name = "Danube Trade Region"
			picture = HUN_treaty_of_triannon

			available = {
				
			}

			modifier = {
				production_speed_buildings_factor = 0.1
				consumer_goods_factor = -0.02
			}
		}

		HUN_foreign_resource_companies = {
			name = "Cooperation with foreign Companies"
			picture = HUN_treaty_of_triannon

			available = {
				
			}

			modifier = {
				production_lack_of_resource_penalty_factor = -0.1
				min_export = -1
			}
		}

		HUN_ally_with_the_devil = {
			name = "Ally With The Devil"
			picture = HUN_treaty_of_triannon

			available = {
				
			}

			modifier = {
				production_factory_max_efficiency_factor = 0.05
				industrial_capacity_factory = 0.1
			}
		}

		HUN_war_preparation = {

			picture = generic_war_preparation

			modifier = {
				justify_war_goal_time = -0.5
				planning_speed = 0.2
				generate_wargoal_tension = -0.5
			}
		}

		

		HUN_hungarian_monarchy = {

			allowed_civil_war = {
				has_government = neutrality
			}
			
			modifier = {
				neutrality_drift = 0.02
			}
		}

		HUN_hungarian_monarchy_2 = {

			picture = HUN_hungarian_monarchy

			allowed_civil_war = {
				has_government = neutrality
			}
			
			modifier = {
				neutrality_drift = 0.02
				stability_factor = 0.1
			}
		}


		HUN_hungarian_monarchy_fascism = {			

			allowed_civil_war = {
				has_government = fascism
			}

			available = {
				OR = {
					has_government = fascism
					AND = {
						has_government = neutrality
						NOT = { has_completed_focus = HUN_renounce_the_treaty_of_trianon }
					}
				}
			}
			
			modifier = {
				fascism_drift = 0.034
				stability_factor = 0.1
			}
		}

		HUN_his_majestys_government = {

			picture = HUN_hungarian_monarchy_democratic

			allowed_civil_war = {
				has_government = democratic
			}

			available = {
				has_government = democratic
			}
			
			modifier = {
				democratic_drift = 0.02
				fascism_drift = -0.01
				communism_drift = -0.01
				stability_factor = 0.1
			}
		}

		HUN_habsburg_restored = {

			picture = HUN_hungarian_monarchy_habsburg

			allowed_civil_war = {
				has_government = neutrality
			}

			available = {
				has_government = neutrality
			}
			
			modifier = {
				neutrality_drift = 0.02
				stability_factor = 0.1
				political_power_factor = 0.2
				generate_wargoal_tension = -0.4
			}
		}

		HUN_charles_v = {

			allowed_civil_war = {
				has_government = democratic
			}
			
			modifier = {
				democratic_drift = 0.02
			}
		}

		HUN_dynastic_ties = {

			allowed_civil_war = {
				has_government = democratic
			}
			
			modifier = {
				
			}
		}

		HUN_strengthen_fascists = {

			picture = generic_fascism_drift_2
			
			modifier = {
				fascism_drift = 0.03
			}
		}

		HUN_interventionism = {

			picture = FRA_scw_intervention_republicans_focus

			modifier = {
				political_power_factor = -0.2
				consumer_goods_factor = -0.05
				conversion_cost_mil_to_civ_factor = -0.15
			}
		}

		HUN_sore_loser = {
			name = "Sore Loser"
			picture = FRA_scw_intervention_republicans_focus

			modifier = {
				consumer_goods_factor = 0.05
				industrial_capacity_factory = -0.2
			}
		}
		HUN_sore_loser2 = {
			name = "Sore Loser"
			picture = FRA_scw_intervention_republicans_focus

			modifier = {
				consumer_goods_factor = 0.05
				industrial_capacity_factory = -0.1
			}
		}

		HUN_interventionism_2 = {

			picture = FRA_scw_intervention_republicans_focus

			modifier = {
				consumer_goods_factor = -0.05
				conversion_cost_mil_to_civ_factor = -0.15
				min_export = -1
				political_power_factor = -0.05
			}
		}

		HUN_reichsautobahn = {
			name = "Reichsautobahn Connection"
			picture = generic_production_bonus

			modifier = {
				production_speed_infrastructure_factor = 0.2
			}
		}

		HUN_nazism = {
			name = "Nazism"
			picture = FRA_scw_intervention_republicans_focus

			modifier = {
				political_power_factor = 0.1
				production_speed_arms_factory_factor = 0.15
				production_factory_efficiency_gain_factor = 0.25
			}
		}

		HUN_submission = {
			name = "Submission"
			picture = FRA_scw_intervention_republicans_focus

			modifier = {
				stability_weekly = 0.002
			}
		}

		HUN_rapid_mobilisation = {
			name = "Rapid Mobilisation"
			picture = FRA_scw_intervention_republicans_focus

			modifier = {
				stability_factor = -0.05
				conversion_cost_civ_to_mil_factor = -0.1
			}
		}

		HUN_interventionism_communism = {

			picture = FRA_scw_intervention_nationalists_focus

			modifier = {
				consumer_goods_factor = -0.05
				communism_drift = 0.02
			}
		}

		HUN_the_hungarian_red_army = {

			picture = generic_communist_army

			modifier = {
				conscription = 0.05
			}
		}

		HUN_secret_rearmament = {

			available = {
			}

			allowed_civil_war = {
				
			}
			
			modifier = {
				industrial_capacity_factory = 0.05
				production_factory_max_efficiency_factor = 0.05
				conscription = 0.03
			}
		}

		HUN_invite_foreign_investors = {

			picture = generic_foreign_capital

			allowed_civil_war = {
				
			}
			
			modifier = {
				production_speed_infrastructure_factor = 0.1
			}
		}

		HUN_indigenous_designs = {

			picture = generic_air_research

			allowed_civil_war = {
				
			}
			
			research_bonus = {
				air_equipment = 0.15
			}
		}

		HUN_license_foreign = {

			picture = generic_license_production

			allowed_civil_war = {
				
			}
			
			modifier = {
				license_air_purchase_cost = -0.5
			}
		}


		HUN_assault_gun_focus = {

			picture = generic_armor

			allowed_civil_war = {
				
			}
			
			equipment_bonus = {
				light_tank_destroyer_equipment = {
					build_cost_ic = -0.1 instant = yes
				}
				medium_tank_destroyer_equipment = {
					build_cost_ic = -0.1 instant = yes
				}
				heavy_tank_destroyer_equipment = {
					build_cost_ic = -0.1 instant = yes
				}
			}
		}
	}

	political_advisor = {

		HUN_lajos_remenyi_schneller = {

			picture = generic_political_advisor_europe_1

			allowed = {
				original_tag = HUN
			}
			available = {
				has_completed_focus = HUN_focus_civ_advisor
			}
			traits = { captain_of_industry }
		}

		HUN_vilmos_roder = {

			picture = generic_political_advisor_europe_2
			available = {
				has_completed_focus = HUN_focus_mill_advisor
			}
			allowed = {
				original_tag = HUN
			}
			
			traits = { war_industrialist }

		}

		HUN_zoltan_tildy = {

			picture = generic_political_advisor_europe_3
			
			allowed = {
				original_tag = HUN
			}
			
			traits = { silent_workhorse }
			}
		}
	# MILITARY

	army_chief = {
		

		HUN_ferenc_szombathelyi = {
			
			picture = generic_army_europe_4
			
			allowed = {
				original_tag = HUN
			}
			

			
			traits = { army_chief_defensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
 
		HUN_hugo_sonyi = {
			
			picture = generic_army_europe_5
			
			allowed = {
				original_tag = HUN
			}
			

			
			traits = { army_chief_offensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	air_chief = {
		
		HUN_Finn_janzsi = {
		ledger = army
			
			picture = generic_army_europe_5			
			allowed = {
				original_tag = HUN
			}
			

			
			traits = { air_tactical_bombing_3 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	navy_chief = {
	}

	high_command = {

		HUN_gusztav_jany = {
			ledger = army
			
			picture = generic_army_europe_3

			allowed = {
				original_tag = HUN
			}
			

			
			traits = { army_armored_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	
		HUN_dezso_laszlo = {
			ledger = army
			
			picture = generic_army_europe_2			
			allowed = {
				original_tag = HUN
			}
			

			
			traits = { army_regrouping_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		HUN_andor_gaszi  = {
			ledger = army
			
			picture = generic_army_europe_4		
			allowed = {
				original_tag = HUN
			}
			

			
			traits = { army_chief_planning_3 }
			
			ai_will_do = {
				factor = 1
			}
		}

		HUN_hatty_zsuco = {
			ledger = army
			
			picture = generic_army_europe_2			
			allowed = {
				original_tag = HUN
			}
			

			
			traits = { army_commando_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		HUN_henrik_werth = {
			ledger = army

			picture = generic_army_europe_1

			allowed = {
				original_tag = HUN
			}
			
			traits = { army_infantry_2 }
			
			ai_will_do = {
				factor = 1
			}
		}


	}
	
	theorist = {
		HUN_shvoy_kalman = {
			ledger = army
		
			picture = generic_army_europe_2

			allowed = {
				original_tag = HUN
			}
			
			research_bonus = {
				land_doctrine = 0.35
			}
			
			traits = { military_theorist }
		}
	}	
	
	aircraft_manufacturer = {

	}

	naval_manufacturer = {

	}

	industrial_concern = {
	
		HUN_mavag = {
			
			picture = generic_industrial_concern_1

			allowed = {
				original_tag = HUN
			}
			
			cost = 150
			removal_cost = 10
			
			research_bonus = {
				industry = 0.15
			}
			
			traits = { industrial_concern }
		}
	}

	materiel_manufacturer = {
	
		designer = yes
	
		HUN_femaru_fegyver_es_gepgyar = {
			on_add = { hidden_effect = { mech_designers_free = yes } }
			picture = generic_infantry_equipment_manufacturer_2

			allowed = {
				original_tag = HUN
			}
			
			cost = 150
			removal_cost = 10
			
			research_bonus = {
				infantry_weapons = 0.15
			}
			
			traits = { infantry_equipment_manufacturer }
		}
	}
}