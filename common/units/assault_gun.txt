sub_units = {

	assault_gun = {
		#sprite = light_armor
		sprite = light_armor
		map_icon_category = infantry
		priority = 501
		ai_priority = 200
		active = yes
		
		type = {
			infantry
			support
		}
		
		group = support

		categories = {
			category_support_battalions
			category_army
		}


		combat_width = 0

		need = {
			assault_gun_equipment = 24
		}
		manpower = 250
		max_organisation = 10
		default_morale = 0.3
		training_time = 90
		max_strength = 7
		weight = 0.8
		supply_consumption = 0.14

		
	}
}
