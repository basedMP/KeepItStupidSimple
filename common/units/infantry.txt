# options for map_icon_category:
# For land units: infantry,armored,other
# For sea units: ship,transport,uboat

sub_units = {

	infantry = {
		sprite = infantry
		map_icon_category = infantry
		
		priority = 600
		ai_priority = 200
		active = no

		type = {
			infantry
		}
		
		group = infantry
		
		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
		}
		
		combat_width = 2
		
		#Size Definitions
		max_strength = 30
		max_organisation = 60
		default_morale = 0.3
		manpower = 1000

		#Misc Abilities
		training_time = 45
		suppression = 1.5
		weight = 0.5
		
		supply_consumption = 0.11
	
		need = {
			infantry_equipment = 100
		}
		forest = {
			defence = -0.1
			movement = 0.1
		}
		urban = {
			defence = -0.1						
		}
		river = {
			attack = 0.1
		}
		plains = {
			movement = 0.1
		}
	}

	bicycle_battalion = {
		sprite = bicycle
		map_icon_category = infantry
		
		priority = 600
		ai_priority = 150
		active = no

		type = {
			infantry
		}
		
		group = infantry
		
		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
		}
		
		combat_width = 2
		
		#Size Definitions
		max_strength = 30
		max_organisation = 60
		default_morale = 0.3
		maximum_speed = 0.6
		manpower = 1000

		#Misc Abilities
		training_time = 45
		suppression = 2
		weight = 0.5
		
		supply_consumption = 0.07
	
		need = {
			infantry_equipment = 100
			support_equipment = 10			
		}

		forest = {
			movement = 0.00
		}
		hills = {
			movement = 0.1
		}
		mountain = {
			movement = 0.05
		}
		marsh = {
			movement = 0.05
		}
		plains = {
			movement = 0.2
		}
		urban = {
			movement = 0.2
		}
		desert = {
			movement = 0.05
		}
		river = {
			movement = -0.05
		}
		amphibious = {
			movement = -0.05
		}
	}

	camelry = {
		sprite = camelry
		map_icon_category = infantry
		priority = 599
		ai_priority = 200
		special_forces = yes
		active = no
		

		type = { infantry }
		
		group = infantry
		
		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
			category_special_forces
		}
		
		combat_width = 2
		
		#Size Definitions
		max_organisation = 70
		max_strength = 30
		default_morale = 0.3
		manpower = 1000

		#Misc Abilities
		
		training_time = 120		
		weight = 0.5
		
		supply_consumption = 0.09

		#Offensive Abilities
		suppression = 2
	
		need = {	infantry_equipment = 200	}
			forest = {
			attack = -0.2
			defence = -0.2
			movement = 0.4			
			}
		
		plains = {
			attack = -0.1
			defence = -0.1
			movement = 0.4						
		}

		hills = {
			attack = -0.15
			defence = -0.15	
			movement = 0.2						
		}
		river = {
			attack = 0.1
		}

		mountain = {
			attack = -0.2
			defence = -0.2
			movement = 0.4									
		}
		desert = { 
			attack = 0.1
			defence = 0.1
			movement = 0.5
		}
		jungle = {
			attack = -0.2
			defence = -0.2
			movement = 0.4									
		}

		marsh = {
			attack = -0.1
			defence = -0.1
			movement = 0.4									
		}

		fort = { attack = 0.1 }
		urban = {
			attack = -0.1
			defence = -0.1
			movement = 0.4						
		}
		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.4
		}
	}
	
	marine = {
		sprite = infantry
		map_icon_category = infantry
		special_forces = yes
		marines = yes
		
		priority = 404
		ai_priority = 200
		active = no

		type = {
			infantry
		}
		
		group = infantry
		
		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
			category_special_forces
		}
		
		combat_width = 2
		
		#Size Definitions
		max_strength = 30
		max_organisation = 70
		default_morale = 0.4
		manpower = 1000

		#Misc Abilities
		training_time = 60
		suppression = 1
		weight = 0.5
		supply_consumption = 0.11
		breakthrough = 1
	
		need = {
			infantry_equipment = 300
		}

		marsh = {
			attack = 0.3
		}
		river = {
			attack = 0.3
		}
		amphibious = {
			attack = 0.5
		}
		urban = {
			attack = -0.05
			defence = -0.1
			movement = 0.1
		}
	}
	
	mountaineers = {
		sprite = infantry
		map_icon_category = infantry
		special_forces = yes
		mountaineers = yes

		priority = 403
		ai_priority = 200
		active = no

		type = {
			infantry
		}
		
		group = infantry
		
		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
			category_special_forces
		}
		
		combat_width = 2
		
		#Size Definitions
		max_strength = 30
		max_organisation = 70
		default_morale = 0.4
		manpower = 1000

		#Misc Abilities
		training_time = 60
		suppression = 1
		weight = 0.5
		
		supply_consumption = 0.12
		breakthrough = 1
	
		need = {
			infantry_equipment = 250
		}

		hills = {
			attack = 0.20
			defence = 0.05
			movement = 0.4
		}
		desert = {
			defence = 0.05
			attack = 0.05
		}
		river = {
			attack = 0.1
		}
		urban = {
			attack = 0.2
			movement = 0.1
		}
		mountain = {
			attack = 0.25
			defence = 0.1
			movement = 0.2
		}
	}

	paratrooper = {
		sprite = infantry
		map_icon_category = infantry
		special_forces = yes

		priority = 2
		ai_priority = 2
		active = no

		type = {
			infantry
		}
		
		group = infantry
		
		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
			category_special_forces
		}
		
		combat_width = 2
		
		#Size Definitions
		max_strength = 30
		max_organisation = 70
		default_morale = 0.4
		manpower = 1000

		#Misc Abilities
		training_time = 75
		suppression = 1
		weight = 0.5
		supply_consumption = 0.07

		can_be_parachuted = yes
	
		need = {
			infantry_equipment = 10000
		}
	}

	motorized = {
		sprite = motorized
		map_icon_category = infantry

		priority = 599
		ai_priority = 200
		active = no

		type = {
			motorized
		}
		
		group = mobile
		
		categories = {
			category_front_line
			category_all_infantry
			category_army
		}
		
		combat_width = 2
		
		#Size Definitions
		max_strength = 16
		max_organisation = 60
		default_morale = 0.30
		manpower = 1200

		#Misc Abilities
		training_time = 45
		suppression = 2.2
		weight = 0.75
		supply_consumption = 0.11
	
		# this is what moves us and sets speed
		transport = motorized_equipment
	
		need = {
			infantry_equipment = 100
			motorized_equipment = 50
		}

		forest = {
			attack = -0.1
			movement = -0.5
		}
		mountain = {
			attack = -0.05
		}
		jungle = {
			attack = -0.2
			movement = -0.5
		}
		marsh = {
			attack = -0.1
			movement = -0.3
		}
		urban = {
			attack = -0.1
		}
		river = {
			attack = -0.1
			movement = -0.1
		}
		amphibious = {
			attack = -0.2
		}
	}

	mechanized = {
		sprite = mechanized
		map_icon_category = infantry

		priority = 610
		ai_priority = 200
		active = yes

		type = {
			mechanized
		}
		
		group = mobile
		
		categories = {
			category_front_line
			category_all_infantry
			category_army
		}

		combat_width = 2

		#Offensive Abilities
		soft_attack = 0.1
		hard_attack = 4.0
		
		#Size Definitions
		max_strength = 21
		max_organisation = 68
		default_morale = 0.3
		manpower = 1200

		#Misc Abilities
		training_time = 60
		suppression = 2
		weight = 1
		
		supply_consumption = 0.18
		
		# needed since we give so much bonus to infantry even with no mech equipment
		essential = {
			infantry_equipment
			mechanized_equipment
		}

		# this is what moves us and sets speed
		transport = mechanized_equipment

		need = {
			mechanized_equipment = 50
			infantry_equipment = 100
		}

		forest = {
			attack = -0.2
			movement = -0.1
		}
		hills = 	{
			defence = -0.1
		}
		mountain = 	{
		    attack = -0.1
			defence = -0.3
		}
		jungle = {
			attack = -0.3
		}
		marsh = {
			attack = -0.1
			defence = -0.05 
		}
		desert = {
			attack = 0.1
			movement = 0.05
		}
		urban = {
			attack = -0.2
			defence = -0.05
		}
		river = {
			attack = -0.2
			movement = -0.2
		}
		amphibious = {
			attack = -0.4
		}
		plains = {
			movement = -0.1
		}

		#hardness = 0.2 moving these buffs to unit stats
	}
	
	
	fake_intel_unit = {
		sprite = infantry
		map_icon_category = infantry
		
		priority = 0
		ai_priority = 0
		active = no

		type = {
			infantry
		}
		
		group = infantry
		
		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
		}
		
		combat_width = 1
		
		#Size Definitions
		max_strength = 1
		max_organisation = 100
		default_morale = 0.3
		manpower = 0

		#Misc Abilities
		training_time = 90
		suppression = 1
		weight = 0.5
		
		supply_consumption = 0.0
	
		need = {
			infantry_equipment = 1
		}
	}
	Converts = {
		sprite = motorized
		map_icon_category = infantry
		
		priority = 600
		ai_priority = 200
		active = yes

		type = {
			infantry
		}
		
		group = infantry
		
		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
		}
		
		combat_width = 2
		
		#Size Definitions
		max_strength = 5
		max_organisation = 1
		default_morale = 0.3
		manpower = 1000

		#Misc Abilities
		training_time = 45
		suppression = 1.5
		weight = 0.5
		
		supply_consumption = 0.02
	
		need = {
			infantry_equipment = 10
		}
		plains = {
			attack = -1
			defence = -1
		}
		hills = {
			attack = -1
			defence = -1
		}
		forest = {
			attack = -1
			defence = -1
		}
		mountain = {
			attack = -1
			defence = -1
		}
		jungle = {
			attack = -1
			defence = -1
		}
		marsh = {
			attack = -1
			defence = -1
		}
		desert = {
			attack = -1
			defence = -1
		}
		urban = {
			attack = -1
			defence = -1
		}
		river = {
			attack = -1
			defence = -1
		}
		amphibious = {
			attack = -1
			defence = -1
		}
		desert = {
			attack = -1
			defence = -1
		}
	}
	rangers = {
		sprite = infantry
		map_icon_category = infantry
		special_forces = yes

		priority = 403
		ai_priority = 200
		active = no

		type = {
			infantry
		}
		
		group = infantry
		
		categories = {
			category_front_line
			category_light_infantry
			category_all_infantry
			category_army
			category_special_forces
		}
		
		combat_width = 2
		
		#Size Definitions
		max_strength = 30
		max_organisation = 70
		default_morale = 0.4
		manpower = 1000

		#Misc Abilities
		training_time = 60
		suppression = 1
		weight = 0.5
		soft_attack = -0.5
		
		supply_consumption = 0.13
		breakthrough = 1.5
	
		need = {
			infantry_equipment = 350
		}

		river = {
			attack = 0.1
		}
		forest = {
			attack = 0.15
			defence = 0.05
		}
		urban = {
			attack = 0.05
		}
	}
}
