# All equipment based on light tank chassis

equipments = {

	amphibious_tank_equipment = {
		year = 1934

		
	
		is_archetype = yes
		is_buildable = no
		is_convertable = yes
		picture = archetype_amphibious_tank_equipment			
		type = armor
		group_by = archetype
		
		interface_category = interface_category_armor
		
		

		#Misc Abilities
		maximum_speed = 8.8
		reliability = 1

		#Defensive Abilities
		defense = 5
		breakthrough = 36
		hardness = 0.8
		armor_value = 20

		#Offensive Abilities
		soft_attack = 18.4
		hard_attack = 7.5
		ap_attack = 37.5
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 10
		
		build_cost_ic = 9
		resources = {
			steel = 3
		}

		fuel_consumption = 0.9
	}

	amphibious_tank_equipment_1 = {
		year = 1934
		
		archetype = amphibious_tank_equipment
		priority = 10
		visual_level = 0
		is_convertable = yes
		can_convert_from = { 
			light_tank_equipment_2
		}
	}

	amphibious_tank_equipment_2 = {
		year = 1939
		
		archetype = amphibious_tank_equipment
		parent = amphibious_tank_equipment_1
		priority = 10
		visual_level = 1
		is_convertable = yes
		can_convert_from = { 
			medium_tank_equipment_1 
		}

		#Misc Abilities
		maximum_speed = 8.8

		#Defensive Abilities
		defense = 5
		breakthrough = 42
		hardness = 0.9
		armor_value = 80

		#Offensive Abilities
		soft_attack = 25
		hard_attack = 22
		ap_attack = 76.2
		air_attack = 0	
		
		build_cost_ic = 15
		resources = {
			steel = 2
			tungsten = 2
		}
	}
}
