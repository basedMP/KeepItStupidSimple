bookmarks = {
	bookmark = {
		name = "TFU ASIA (CURRENTLY NOT PLAYABLE)"
		desc = "GATHERING_STORM_DESC"
		date = 1936.1.1.13
		picture = "GFX_select_date_1936"
		default_country = "GER"
		default = yes
		
		"FRA"={
			history = "FRA_GATHERING_STORM_DESC"
			ideology = democratic
			available = { NOT = { has_dlc = "La Resistance" } }
			ideas = {
				FRA_victors_of_wwi
				FRA_disjointed_government
				FRA_protected_by_the_maginot_line
			}
			focuses = {
				FRA_form_the_popular_front
			}
		}
		"FRA"={
			history = "FRA_GATHERING_STORM_DESC"
			ideology = democratic
			available = { has_dlc = "La Resistance" } 
			ideas = {
				FRA_victors_of_wwi
				FRA_disjointed_government
				FRA_protected_by_the_maginot_line
			}
			focuses = {
				FRA_battle_of_maneuver
				FRA_army_reform
				FRA_defend_paris
			}
		}
		"ENG"={
			history = "ENG_GATHERING_STORM_DESC"
			ideology = democratic
			available = { has_dlc = "Man the Guns" }
			ideas = {
				stiff_upper_lip
				ENG_the_war_to_end_all_wars
				george_v
			}
			focuses = {
				uk_empire_focus
				ENG_imperial_conference
				"Learn from Africa"
			}
		}
		"ENG"={
			history = "ENG_GATHERING_STORM_DESC"
			ideology = democratic
			available = { NOT = { has_dlc = "Man the Guns" } }
			ideas = {
				stiff_upper_lip
				ENG_the_war_to_end_all_wars
				george_v
			}
			focuses = {
				tizard_mission_focus
			}
		}
		"GER"={
			history = "GER_GATHERING_STORM_DESC"
			ideology = fascism
			available = { has_dlc = "Waking the Tiger" }
			ideas = {
				sour_loser
				general_staff
				GER_mefo_bills_1
			}
			focuses = {
				GER_air_innovation
				"Totaler Krieg"
				"Aggregat 4"
			}
		}
		"GER"={
			history = "GER_GATHERING_STORM_DESC"
			ideology = fascism
			available = { NOT = { has_dlc = "Waking the Tiger" } }
			ideas = {
				sour_loser
				general_staff
				GER_mefo_bills_1
			}
			focuses = {
			}
		}
		"ITA"={
			history = "ITA_GATHERING_STORM_DESC"
			ideology = fascism
			ideas = {
				vittoria_mutilata
				victor_emmanuel
			}	
			focuses = {
				ITA_atlantic_fleet
				ITA_italian_highways
				ITA_victoryinETH
			}		
		}
		"SOV"={
			history = "SOV_GATHERING_STORM_DESC"
			ideology = communism
			ideas = {
				trotskyite_plot
				home_of_revolution
			}
			focuses = {
				SOV_finish_five_year_plan
				SOV_great_purge
				 SOV_Great_Patriotic_War
			}	
		}
		"USA"={
			history = "USA_GATHERING_STORM_DESC"
			ideology = democratic
			ideas = {
				home_of_the_free
				great_depression
				undisturbed_isolation
			}
			focuses = {
				USA_arsenal_of_democracy
				USA_wpa
				USA_selective_training_act
			}
		}
		"JAP"={
			history = "JAP_GATHERING_STORM_DESC"
			ideology = fascism
			ideas = {
				state_shintoism
				JAP_zaibatsus
				JAP_militarism
			}	
			focuses = {
				JAP_the_zero
				JAP_warrior_spirit
				JAP_strike_on_the_southern_resource_area
			}			
		}
		"---"={
			history = "OTHER_GATHERING_STORM_DESC"
		}


		# minors from DLC ####
		"CAN"={
			minor = yes
			history = "CAN_GATHERING_STORM_DESC"
			ideology = democratic
			ideas = {
				CAN_great_depression_1
				CAN_conscription_crisis
			}
			focuses = {
				CAN_rowell_sirois_commission
				CAN_strengthen_the_commonwealth_ties
				CAN_war_fueled_economy
			}
		}
		"HUN"={
			minor = yes
			history = "HUN_GATHERING_STORM_DESC"
			ideology = neutrality
			ideas = {
				HUN_treaty_of_triannon
			}
			focuses = {
				HUN_secret_rearmament
				"Riga Operations"
				HUN_proclaim_greater_hungary
				
			}
		}

	"RAJ"={
			minor = yes
			history = "RAJ_GATHERING_STORM_DESC"
			ideology = neutrality
			ideas = {
				RAJ_agrarian_society
				RAJ_princely_states
			}
			focuses = {
				RAJ_indianisation_of_army
				RAJ_provincial_elections
				RAJ_ishapore_arsenal
			}
		}

		"ROM"={
			minor = yes
			history = "ROM_GATHERING_STORM_DESC"
			ideology = neutrality
			ideas = {
				ROM_king_carol_ii_hedonist
				neutrality_idea
			}
			focuses = {
				ROM_institute_royal_dictatorship
				ROM_focus_preserve_greater_romania
				ROM_force_abdication
			}
		}

		"SPR"={
			minor = yes
			history = "SPAIN_GATHERING_STORM_DESC"
			ideology = fascism
			ideas = {
			}
			focuses = {
				"Consolidate the North"
				"Fuse The Parties"
				"National Recovery"
			}
		}
		
		"UAC"={
			minor = yes
			history = "UAC_GATHERING_STORM_DESC"
			ideology = democratic
			ideas = {
				UAC_depression
				UAC_arab_revolt
			}
			focuses = {
				UAC_focus_recovery
				UAC_focus_one_africa
				"Mass Mobilization"
			}	
		}

		"BUL"={
		minor = yes
		history = "BUL_GATHERING_STORM_DESC"
		ideology = neutrality
		ideas = {
		}
		focuses = {
			BUL_acquire_modern_tools		
			BUL_power_to_the_tsar
	        BUL_negotiate_bulgarian_rearmament
		}
		
		}
		"AST"={
			minor = yes
			history = "AST_GATHERING_STORM_DESC"
			ideology = democratic
			ideas = {
				AST_great_depression_1
			}
			focuses = {
				AST_rats_of_tobruk
				AST_invest_in_victory
				AST_squash_the_squanderbugs
			}
		}

		effect = {
			randomize_weather = 22345 # <- Obligatory in every bookmark !
			#123 = { rain_light = yes }
		}
	}	
}
