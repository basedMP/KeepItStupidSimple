atlantikwall = {
is_coastal = yes
OR = {
	state = 806
	state = 19
	state = 23
	state = 30
	state = 14
	state = 15
	state = 785
	state = 29
	state = 6
	state = 35
	state = 7
	state = 36
	state = 56
	state = 59
	state = 58
	state = 99
	state = 142
	state = 143
	state = 144
	}
}

tfu_italy_fortress = { 
	OR = { state = 1 state = 114 state = 115 state = 184 state = 186 state = 731 state = 47 state = 187 state = 164 state = 182 }
}

tfu_italy_fortress_libya = {
	OR = {state = 115 state = 450 state = 449 state = 662 state = 661 state = 448 state = 665 state = 458 state = 451 state = 663}
}

tfu_ignore_aa_states = { NOT = { OR = { is_core_of = SPR state = 551 state = 456 state = 549 state = 550 state = 271}}}


axis = {
	OR = {
		tag = GER
		tag = ITA
		tag = HUN
		tag = ROM
		tag = SPR
		tag = BUL
	}
}

allies = {
	OR = {
		tag = ENG
		tag = FRA
		tag = CAN
		tag = AST
		tag = UAC
	}
}

geacps = {
	OR = {  
		tag = JAP 
		tag = MAN 
	} 
}

is_controlled_by_ROOT_or_ally = {
	custom_trigger_tooltip = {
		tooltip = is_controlled_by_ROOT_or_ally
		CONTROLLER = {
			OR = {
				tag = ROOT
				is_subject_of = ROOT
				is_in_faction_with = ROOT
			}
		}
	}
}

has_received_expeditionary_forces = {
	OR = { 
		received_expeditionary_forces = { sender = GER value > 0 } 
		received_expeditionary_forces = { sender = ITA value > 0 } 
		received_expeditionary_forces = { sender = ROM value > 0 } 
		received_expeditionary_forces = { sender = HUN value > 0 }
		received_expeditionary_forces = { sender = SPR value > 0 }  
		received_expeditionary_forces = { sender = BUL value > 0 } 
		received_expeditionary_forces = { sender = ENG value > 0 } 
		received_expeditionary_forces = { sender = FRA value > 0 } 
		received_expeditionary_forces = { sender = CAN value > 0 }  
		received_expeditionary_forces = { sender = AST value > 0 }  
		received_expeditionary_forces = { sender = UAC value > 0 }  
	}  
}