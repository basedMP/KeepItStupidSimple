focus_tree = {
	id = spainish_focus
	
	country = {
		factor = 0
		
		modifier = {
			add = 10
			tag = SPR
		}
	}

	initial_show_position = {
		x = 0
	}
	continuous_focus_position = { x = 50 y = 1200 }
	focus = {
		id = "The Nationalist Front"
		icon = GFX_focus_spa_unify_the_nationalist_front
		x = 3
		y = 0
		cost = 5
		ai_will_do = {
			factor = 5
		}
	
		available_if_capitulated = yes
	
		bypass = {
			
		}
	
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_political_power = 100
		}
	}


	focus = {
	id = "Consolidate the North"
	icon = GFX_focus_spa_eliminate_the_carlists
	x = 0
	y = 1
	prerequisite = { focus = "The Nationalist Front" }
	relative_position_id = "The Nationalist Front"
	cost = 5
	ai_will_do = {
		factor = 5
	}
	
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		
		790 = {
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
		792 = {
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
		}
	}

	focus = {
		id = "Fuse The Parties"
		icon = GFX_focus_spa_fuse_the_parties
		x = 0
		y = 2
		prerequisite = { focus = "Consolidate the North" }
		relative_position_id = "The Nationalist Front"
		cost = 5
		ai_will_do = {
			factor = 5
		}
		
		available_if_capitulated = yes
	
		bypass = {
			
		}
	
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_popularity = { ideology = fascism popularity = 0.05 }
			add_stability = 0.05
			}
		}

		focus = {
			id = "National Recovery"
			icon = GFX_focus_spa_strengthen_the_supreme_reality_of_spain
			x = -2
			y = 3
			prerequisite = { focus = "Fuse The Parties" }
			relative_position_id = "The Nationalist Front"
			cost = 10
			ai_will_do = {
				factor = 5
			}
			
			available_if_capitulated = yes
			available = {
				date > 1938.1.1
			}
		
			bypass = {
				
			}
		
			search_filters = { FOCUS_FILTER_INDUSTRY }
			completion_reward = {
				41 = {
					add_extra_state_shared_building_slots = 10
					add_building_construction = {
						type = industrial_complex
						level = 8
						instant_build = yes
					}
				}
				41 = {
					add_extra_state_shared_building_slots = 10
					add_building_construction = {
						type = arms_factory
						level = 7
						instant_build = yes
					}
				}
				}
			}

			focus = {
				id = "La reforma agraria española"
				Text = "La reforma agraria española"
				icon = SPR_agrar
				x = 0
				y = 1
				prerequisite = { focus = "National Recovery" }
				relative_position_id = "National Recovery"
				cost = 10
				ai_will_do = {
					factor = 5
				}
				
				available_if_capitulated = yes
				available = {
				}
			
				bypass = {
					
				}
			
				search_filters = { FOCUS_FILTER_INDUSTRY }
				completion_reward = {
					add_ideas = SPR_construction_effort
					41 = {
						add_extra_state_shared_building_slots = 2
						add_building_construction = {
							type = industrial_complex
							level = 2
							instant_build = yes
						}
					}
				}
			}	
			
			focus = {
				id = "Expand Civilian Industry II"
				icon = SPR_infrastructure
				x = -1
				y = 2
				prerequisite = { focus = "La reforma agraria española"}
				prerequisite = { focus = "The Duty To Work" }
				relative_position_id = "National Recovery"
				cost = 10
				ai_will_do = {
					factor = 5
				}
				
				available_if_capitulated = yes
				available = {
				}
			
				bypass = {
					
				}
			
				search_filters = { FOCUS_FILTER_INDUSTRY }
				completion_reward = {
					41 = {
						add_extra_state_shared_building_slots = 3
						add_building_construction = {
							type = industrial_complex
							level = 3
							instant_build = yes
						}
					}
				}
			}	

			focus = {
		id = SPR_church
		Text = "Integrate the Iglesia Católica"
		icon = SPR_church
		x = 3
		y = 1
		prerequisite = { focus = "Consolidate the North"}
		relative_position_id = "Consolidate the North"
		cost = 5
		ai_will_do = {
			factor = 5
		}
		
		available_if_capitulated = yes
	
		bypass = {
			
		}
	
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			
			add_stability = 0.1
			}
		}
			
			focus = {
				id = "Adopt the 26 Points"
				icon = SPR_points
				x = 3
				y = 3
				prerequisite = { focus = SPR_church }
				relative_position_id = "The Nationalist Front"
				cost = 5
				ai_will_do = {
					factor = 5
				}
				
				available_if_capitulated = yes
			
				bypass = {
					SPR = { has_idea = war_economy }
				}
			
				search_filters = { FOCUS_FILTER_INDUSTRY }
				completion_reward = {
					add_ideas = war_economy
					}
				}

	focus = {
			id = SPR_denounce_nazism
			Text = "Denounce Nazism"
			icon =  SPR_denounce_nazism

			x = 2
			y = 4
			prerequisite = { focus = "Adopt the 26 Points" }
		    mutually_exclusive = {focus = SPR_focus_take_foreign_fascists}
			relative_position_id = "The Nationalist Front"
			cost = 10
			ai_will_do = {
				factor = 5
			}
			
			available_if_capitulated = yes
		
			bypass = {
				
			}
		
			search_filters = { FOCUS_FILTER_INDUSTRY }
			completion_reward = {
				add_stability = 0.05
				add_named_threat = { threat = -1 name = "Denounce Nazism" }
				}
			}

focus = {
			id = SPR_focus_take_foreign_fascists
			Text = "Asylum for foreign fascists"
			icon =  SPR_take

			x = 4
			y = 4
			prerequisite = { focus = "Adopt the 26 Points" }
			mutually_exclusive = {focus = SPR_denounce_nazism}
			relative_position_id = "The Nationalist Front"
			cost = 10
			ai_will_do = {
				factor = 5
			}
			
			available_if_capitulated = yes
		
			bypass = {
				
			}
		
			search_filters = { FOCUS_FILTER_INDUSTRY }
			completion_reward = {
				add_popularity = { ideology = fascism popularity = 0.05 }
				add_stability = 0.05
				}
			}
				
				focus = {
					id = "Legacy of Industrial Revolution"
					icon = SPR_civs
					x = -4
					y = 3
					prerequisite = { focus = "Fuse The Parties" }
					relative_position_id = "The Nationalist Front"
					cost = 10
					ai_will_do = {
						factor = 5
					}
					
					available_if_capitulated = yes
				
					bypass = {
						
					}
				
					search_filters = { FOCUS_FILTER_INDUSTRY }
					completion_reward = {
						41 = {
							add_extra_state_shared_building_slots = 2
							add_building_construction = {
								type = industrial_complex
								level = 2
								instant_build = yes
							}
						}
						41 = {
							add_extra_state_shared_building_slots = 2
							add_building_construction = {
								type = arms_factory
								level = 2
								instant_build = yes
							}
						}
						}
					}
								

	focus = {
	id = "Universidad  del Cadillo"
	icon = GFX_focus_research
	x = 0
	y = 3
	prerequisite = { focus = "Fuse The Parties" }
	relative_position_id = "The Nationalist Front"
	cost = 10
	ai_will_do = {
	factor = 5
	}
					
	available_if_capitulated = yes
				
	bypass = {
						
	}
				
	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_research_slot = 1
		}
	}

focus = {
	id = "The Duty To Work"
	icon = GFX_goal_generic_production
	x = 0
	y = 4
	prerequisite = { focus = "National Recovery" }
	relative_position_id = "The Nationalist Front"
	cost = 10
	ai_will_do = {
		factor = 5
	}
		
	available_if_capitulated = yes
	
	bypass = {
		
	}
	available = {
		date > 1939.1.1
	}
	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		41 = {
			add_extra_state_shared_building_slots = 6
			add_building_construction = {
				type = infrastructure
				level = 4
				instant_build = yes
			}
			add_building_construction = {
				type = industrial_complex
				level = 2
				instant_build = yes
			}
		}
		add_ideas = SPR_the_duty_to_work
		add_tech_bonus = {
				name = "The duty to work"
				bonus = 1.0
				uses = 2
				technology = assembly_line_production
				technology = streamlined_line
				technology = flexible_line
			
			}
		}
		
	}

	focus = {
		id = "Expand The Southern Military Complex"
		icon = GFX_goal_generic_construct_mil_factory
		x = -1
		y = 1
		prerequisite = { focus = "The Duty To Work" }
		relative_position_id = "The Duty To Work"
		cost = 10
		ai_will_do = {
			factor = 5
		}
			
		available_if_capitulated = yes
		
		bypass = {
			
		}
	
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			41 = {
				add_extra_state_shared_building_slots = 5
				add_building_construction = {
					type = arms_factory
					level = 5
					instant_build = yes
				}
			}
			}
		}

		focus = {
			id = "War Economy"
			icon = SPR_war_eco
			x = -4
			y = 0
			prerequisite = { focus = "Legacy of Industrial Revolution" }
			relative_position_id = "The Duty To Work"
			cost = 10
			ai_will_do = {
				factor = 5
			}
				
			available_if_capitulated = yes
			
			available = {
				has_war = yes
			}
			bypass = {
				
			}
		
			search_filters = { FOCUS_FILTER_INDUSTRY }
			completion_reward = {
				swap_ideas = { 
					remove_idea = SPR_the_duty_to_work
					add_idea = SPR_the_duty_to_work2 
				}
				41 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
			}
				}
			}

	  focus = {
					id = SPR_focus_German_Ties
					Text = "German Ties"
					icon = SPR_german_ties
					x = 3
					y = 1
					prerequisite = { focus = SPR_focus_take_foreign_fascists }
					relative_position_id = SPR_denounce_nazism
					cost = 10
					ai_will_do = {
						factor = 5
					}
						
					available_if_capitulated = yes
					
					bypass = {
						
					}
					search_filters = { FOCUS_FILTER_INDUSTRY }
					completion_reward = {
						add_relation_modifier = {
				target = GER
				modifier = ROM_license_german_equipment
			}
						
					}
					}


		focus = {
			id = "German Aid"
			icon = GFX_focus_usa_reestablish_the_gold_standard
			x = 1
			y = 1
			prerequisite = { focus = SPR_focus_German_Ties}
			
			relative_position_id = SPR_focus_German_Ties
			cost = 10
			ai_will_do = {
				factor = 5
			}
				
			available_if_capitulated = yes
			
			bypass = {
				
			}
		
			search_filters = { FOCUS_FILTER_INDUSTRY }
			completion_reward = {
				add_ideas = SPR_german_aid
				}
			}

focus = {
			id = SPR_german_mil_focus
			Text = "German Advisors"
			icon = SPR_ger_help
			x = -1
			y = 1
			prerequisite = { focus = SPR_focus_German_Ties}
			available = {has_completed_focus = "Pensamiento Táctico"}
			relative_position_id = SPR_focus_German_Ties
			cost = 10
			ai_will_do = {
				factor = 5
			}
				
			available_if_capitulated = yes
			
			bypass = {
				
			}
		
			search_filters = { FOCUS_FILTER_INDUSTRY }
			completion_reward = {
				swap_ideas = {
				remove_idea = SPR_pensamiento_tactico
				add_idea = SPR_pensamiento_tactico2
			}
				}
			}

	  
	focus = {
		id = SPR_focus_Italian_Ties
		Text = "Italian Ties"
		icon = SPR_ITA_SPA
		x = -1
		y = 1
		prerequisite = { focus = SPR_denounce_nazism }
		relative_position_id = SPR_denounce_nazism
		cost = 10
		ai_will_do = {
			factor = 5
		}
			
		available_if_capitulated = yes
		
		bypass = {
			
		}

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_relation_modifier = {
				target = ITA
				modifier = ROM_license_german_equipment
			}
			add_resource = {
				type = steel
				amount = 50
				state = 165
			}
		}
	}
		
	focus = {
		id = SPR_glory
		Text = "Reclaim our former Glory"
		icon = SPR_legacy
		x = 0
		y = 2
		prerequisite = { focus = SPR_denounce_nazism  focus = SPR_focus_take_foreign_fascists}
		relative_position_id = "Adopt the 26 Points"
		cost = 10
		ai_will_do = {
			factor = 5
		}
			
		available_if_capitulated = yes
		
		bypass = {
			
		}
	
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = { 
			custom_effect_tooltip = Spain_glory_localisation_tt
			
			hidden_effect = {
				set_cosmetic_tag = SPR_STATE_SPANISH }
				swap_ideas = {
				remove_idea = SPR_schivilwar
				add_idea = SPR_schivilwarfixed
				}
			}
		}
			
	focus = {
		id = "Italian Aid"
		icon = SPR_produce
		x = -2
		y = 2
		prerequisite = { focus = SPR_focus_Italian_Ties}
		
		relative_position_id = SPR_denounce_nazism
		cost = 10
		ai_will_do = {
			factor = 5
		}
			
		available_if_capitulated = yes
		
		bypass = {
			
		}

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_timed_idea = {
				idea = SPR_italian_aid days = 365
			}
		}
	}
	
	focus = {
		id = SPR_repay
		Text = "Repay our debt to Mussolini"
		icon = SPR_pay
		x = 0
		y = 2
		prerequisite = { focus = SPR_focus_Italian_Ties}
		
		relative_position_id = SPR_denounce_nazism
		cost = 10
		ai_will_do = {
			factor = 5
		}
			
		available_if_capitulated = yes
		
		bypass = {
			
		}

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			SPR = {
				give_resource_rights = {
					receiver = ITA 
					state = 165
				}
			}
		}
	}

############ ARMY

focus = {
	id = "The Condor Legion"
	icon = GFX_focus_chi_mission_to_germany
	x = 16
	y = 1
	cost = 5
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "The Nationalist Front" }
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine
			bonus = 1
			uses = 4
			category = land_doctrine
		}
	}
}

focus = {
	id = SPR_focus_hp
	Text = "Improve our military production"
	icon = SPR_sustain
	x = 1
	y = 1
	cost = 5
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "Expand Civilian Industry II" focus= "Expand The Southern Military Complex"}
	relative_position_id = "Expand Civilian Industry II"
	available_if_capitulated = yes
	available {date > 1941.6.21}
	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_timed_idea = {
						idea = SPR_sustain_hp days = 365
					}
	}
}

focus = {
	id = SPR_focus_franco
	Text = "The Caudillo Academy"
	icon = SPR_franco
	x = 10
	y = 1
	cost = 10
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "The Nationalist Front" }
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine
			bonus = 2
			uses = 2
			category = land_doctrine
		}
	}
}

focus = {
	id = SPR_gib_focus
	Text = "Downfall of the Imperialists"
	icon = SPR_gib
	x = 10
	y = 2
	cost = 2
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = SPR_focus_franco }
	available_if_capitulated = yes

	available = {
		date > 1939.9.1
	}

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
	
	add_timed_idea = {
				idea = SPR_revenge
				days = 75
			}
	
	}
}




focus = {
	id = SPR_katsu
	Text = "Promote Katsu"
	icon = SPR_katsus
	x = 10
	y = 3
	cost = 5
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = SPR_gib_focus}
	available_if_capitulated = yes

	available = {
		
	}

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		every_unit_leader = {
				limit={has_id=7779}
				add_attack=2
				add_defense=1
				add_planning=2
				add_logistics=2
			} 
	}
}

focus = {
	id = SPR_GIB_INDUSTRY
	Text = "Strenghten National Companies"
	icon = SPR_workers
	x = -1
	y = 1
	cost = 10
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = SPR_katsu }
	mutually_exclusive = {focus = SPR_GIB_INDUSTRY_2}
	relative_position_id = SPR_katsu
	available_if_capitulated = yes

	available { OR = {
				
				any_country = {
					is_in_faction_with = ITA
					controls_state = 446
					
				}
				
			}
	}
	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
	41 = {
			add_extra_state_shared_building_slots = 14
			add_building_construction = {
				type = arms_factory
				level = 12
				instant_build = yes
			}
		}
	167 = {
			add_building_construction = {
				type = infrastructure
				level = 4
				instant_build = yes
			}
		}
	swap_ideas = {
				remove_idea = SPR_the_duty_to_work
				add_idea = SPR_the_duty_to_work_6
			}
	}
}

	focus = {
	id = SPR_GIB_INDUSTRY_2
	Text = "Expand foreign Trade"
	icon = SPR_trade
	x = 1
	y = 1
	cost = 10
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = SPR_katsu }
	mutually_exclusive = {focus = SPR_GIB_INDUSTRY}
	relative_position_id = SPR_katsu
	available_if_capitulated = yes

	available { OR = {
				
				any_country = {
					is_in_faction_with = ITA
					controls_state = 446
					
				}
				
			}
	}
	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
	41 = {
					add_extra_state_shared_building_slots = 8
					add_building_construction = {
						type = arms_factory
						level = 8
						instant_build = yes
					}
				}
	swap_ideas = {
				remove_idea = SPR_the_duty_to_work
				add_idea = SPR_the_duty_to_work_7
			}
	}
}

focus = {
	id = SPR_egypt_2
	Text = "Veteranos del Cairo"
	icon = SPR_cairo_vet
	x = 0
	y = 2
	cost = 10
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = SPR_GIB_INDUSTRY focus = SPR_GIB_INDUSTRY_2}
	mutually_exclusive = {focus = SPR_egypt_1}
	relative_position_id = SPR_katsu
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
	add_ideas = SPR_fighters
	790 = {
					add_extra_state_shared_building_slots = 4
					add_building_construction = {
						type = arms_factory
						level = 4
						instant_build = yes
					}
				}
	}
}

focus = {
	id = SPR_egypt_1
	Text = "Exploit occupied industries"
	icon = SPR_egypt_1
	x = 2
	y = 2
	cost = 10
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = SPR_GIB_INDUSTRY focus = SPR_GIB_INDUSTRY_2}
	mutually_exclusive = {focus = SPR_egypt_2}
	relative_position_id = SPR_katsu
	available_if_capitulated = yes

	available { OR = {
				
				any_country = {
					is_in_faction_with = ITA
					controls_state = 446
					
				}
				
			}
	}
	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_timed_idea = { idea = SOV_prisoners_of_war days = 500 }
	792 = {
					add_extra_state_shared_building_slots = 4
					add_building_construction = {
						type = arms_factory
						level = 4
						instant_build = yes
					}
				}
	}
}



focus = {
	id = "Experimentos de tanques"
	icon = GFX_goal_generic_army_tanks
	x = -1
	y = 1
	cost = 5
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "The Condor Legion" }
	relative_position_id = "The Condor Legion"
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		hidden_effect = {country_event = { id = bhutan.7 }}
		add_tech_bonus = {
			name = armor_bonus
			bonus = 3.0
			uses = 1
			technology = basic_medium_tank
			technology = basic_heavy_tank
		}
			add_tech_bonus = {
			name = armor_bonus
			bonus = 1.0
			uses = 1
			technology = improved_medium_tank
			technology = improved_heavy_tank
		}
		add_tech_bonus = {
			name = armor_bonus
			bonus = 1.0
			uses = 1
			technology = advanced_medium_tank
			technology = advanced_heavy_tank
		}
		add_tech_bonus = {
			name = armor_bonus
			bonus = 1.0
			uses = 1
			technology = advanced_light_tank
		}
	}
}

focus = {
	id = "Modernización mecanizada"
	icon = GFX_goal_generic_army_motorized
	x = -1
	y = 2
	cost = 5
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "Experimentos de tanques" }
	relative_position_id = "The Condor Legion"
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_tech_bonus = {
			name = HUN_the_botond
			bonus = 3.0
			uses = 1
			category = motorized_equipment
		}
		add_tech_bonus = {
			name = HUN_the_botond
			bonus = 2.0
			uses = 1
			category = motorized_equipment
		}
	}
}

focus = {
	id = "Mechanized Infantry Battalions"
	icon = GFX_goal_generic_army_motorized
	x = -3
	y = 2
	cost = 5
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "Experimentos de tanques" }
	relative_position_id = "The Condor Legion"
	available_if_capitulated = yes

	available = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		custom_effect_tooltip = spr_mech_tt 
		add_ideas = SPR_Mechanized 
	}
}

focus = {
	id = "Recycle Old Tank Models"
	icon = GFX_goal_continuous_repairments
	x = -2
	y = 3
	cost = 10
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "Experimentos de tanques" }
	relative_position_id = "The Condor Legion"
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_ideas = SPR_recycle_old_tank_models
	}
}

focus = {
	id = "Ratas Del Desierto"
	icon = GFX_focus_generic_military_mission
	x = 1
	y = 1
	cost = 10
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "The Condor Legion" }
	relative_position_id = "The Condor Legion"
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_ideas = SPR_civil_war_veterans
	}
}

focus = {
	id = "Pensamiento Táctico"
	icon = GFX_goal_generic_army_doctrines
	x = 3
	y = 1
	cost = 10
	ai_will_do = {
		factor = 5
	}
	
	prerequisite = { focus = "The Condor Legion" }
	relative_position_id = "The Condor Legion"
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_ideas = SPR_pensamiento_tactico
	}
}

focus = {
	id = "Gun Modernisation"
	icon = GFX_goal_generic_small_arms
	x = 1
	y = 2
	cost = 5
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "Ratas Del Desierto" }
	relative_position_id = "The Condor Legion"
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_tech_bonus = {
			name = "Gun Research"
			bonus = 1
			uses = 2
			technology = improved_infantry_weapons_2
			technology = advanced_infantry_weapons
		}
	}
}

focus = {
	id = "Artillería de montaña"
	icon = GFX_goal_generic_army_artillery
	x = 3
	y = 2
	cost = 5
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "Ratas Del Desierto" }
	relative_position_id = "The Condor Legion"
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_tech_bonus = {
			name = arty_bonus
			bonus = 1.0
			uses = 2
			technology = artillery3
			technology = artillery4
		}
		add_tech_bonus = {
			name = Rarty_bonus
			bonus = 0.75
			uses = 6
			category = rocketry
		}
	}
}

focus = {
	id = "Equipo antitanque"
	icon = GFX_goal_generic_army_artillery2
	x = 2
	y = 3
	cost = 5
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "Ratas Del Desierto" }
	relative_position_id = "The Condor Legion"
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_tech_bonus = {
			name = "Anti Tank Modernisation"
			bonus = 0.5
			uses = 6
			category = cat_anti_tank
		}
	}
}

focus = {
	id = "Fuerzas especiales"
	icon = GFX_goal_generic_allies_build_infantry
	x = -3
	y = 1
	cost = 5
	ai_will_do = {
		factor = 5
	}
	prerequisite = { focus = "The Condor Legion" }
	relative_position_id = "The Condor Legion"
	available_if_capitulated = yes

	bypass = {
		
	}

	search_filters = { FOCUS_FILTER_INDUSTRY }
	completion_reward = {
		add_tech_bonus = {
			name = special_bonus
			bonus = 1.0
			uses = 2
			category = mountaineers_tech
		}
	add_tech_bonus = {
			name = special_bonus
			bonus = 1.0
			uses = 2
			category = marine_tech
		}
		add_tech_bonus = {
			name = "Landing Crafts"
			bonus = 1
			uses = 2
			category = tp_tech
		}
	}
}


}
