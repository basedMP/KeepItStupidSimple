﻿focus_tree = {
	id = soviet_focus

	country = {
		factor = 0
		
		modifier = {
			add = 10
			tag = SOV
		}
	}
	
	default = no

	focus = {
		id = SOV_finish_five_year_plan
		icon = GFX_SOV_5_year_plan
		x = 0
		y = 0
		cost = 5
		ai_will_do = {
			factor = 5
		}

		available_if_capitulated = yes

		bypass = {
			
		}

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			219 = {
				add_extra_state_shared_building_slots = 20
				add_building_construction = {
					type = industrial_complex
					level = 8
					instant_build = yes
				}
				add_building_construction = {
					type = arms_factory
					level = 4
					instant_build = yes
				}
			}
			195 = {
				add_extra_state_shared_building_slots = 20
				add_building_construction = {
					type = industrial_complex
					level = 8
					instant_build = yes
				}
				add_building_construction = {
					type = arms_factory
					level = 4
					instant_build = yes
				}
			}
		}
	}
	
	focus = {
		id = "Short-Term Investments"
		icon = GFX_SOV_short_term_industry
		mutually_exclusive = {
			focus = "Long-Term Investments"
			focus = "Medium-Term Investments"
		}
		prerequisite = { focus = SOV_finish_five_year_plan }
		x = -2
		y = 1
		relative_position_id = SOV_finish_five_year_plan
		cost = 10
		ai_will_do = {
			factor = 5
		}

		available_if_capitulated = yes

		bypass = {
		
		}
		
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_ideas = SOV_short_term_investments
			582 = {
				add_extra_state_shared_building_slots = 28
				add_building_construction = {
					type = arms_factory
					level = 28
					instant_build = yes
				}
			}
			add_tech_bonus = {
				name = "Short-Term Investments"
				bonus = 0.5
				uses = 1
				technology = streamlined_line
				technology = flexible_line
			
			}
			add_ideas = SOV_julaska_kowalski
		}
	}

	focus = {
		id = "Medium-Term Investments" 
		icon = GFX_SOV_tractor_factories
		mutually_exclusive = {
			focus = "Short-Term Investments"
			focus = "Long-Term Investments"
		}
		prerequisite = { focus = SOV_finish_five_year_plan }
		x = 0
		y = 1
		relative_position_id = SOV_finish_five_year_plan
		cost = 10
		ai_will_do = {
			factor = 5
		}

		available_if_capitulated = yes

		bypass = {
			
		}
		
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_ideas = SOV_medium_term_investments
			582 = {
				add_extra_state_shared_building_slots = 20
				add_building_construction = {
					type = industrial_complex
					level = 8
					instant_build = yes
				}
				add_building_construction = {
					type = arms_factory
					level = 12
					instant_build = yes
				}
			}
			add_ideas = SOV_sergei_korolev
		}
	}

	focus = {
		id = "Long-Term Investments" 
		icon = GFX_SOV_long_term_industry
		mutually_exclusive = {
			focus = "Short-Term Investments"
			focus = "Medium-Term Investments"
		}
		prerequisite = { focus = SOV_finish_five_year_plan }
		x = 2
		y = 1
		relative_position_id = SOV_finish_five_year_plan
		cost = 10
		ai_will_do = {
			factor = 5
		}

		available_if_capitulated = yes

		bypass = {
			
		}
		
		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_ideas = SOV_long_term_investments
			582 = {
				add_extra_state_shared_building_slots = 10
				add_building_construction = {
					type = industrial_complex
					level = 10
					instant_build = yes
				}
			}
			add_ideas = SOV_nikolai_voznesensky
		}
	}

	focus = {
		id = SOV_extra_tech_slot_early
		icon = GFX_SOV_research_slot
		text = extra_tech_slot
		prerequisite = { focus = "Long-Term Investments" focus = "Short-Term Investments" focus = "Medium-Term Investments" }
		x = 1
		y = 1
		relative_position_id = "Short-Term Investments"
		cost = 10
		ai_will_do = {
			factor = 5
		}

		available_if_capitulated = yes
		
		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			add_research_slot = 1
		}
	}
	
	focus = {
		id = SOV_improve_railway
		icon = GFX_SOV_railway
		x = 0
		y = 3
		cost = 5
		ai_will_do = {
			factor = 1
		}
		prerequisite = { focus = SOV_extra_tech_slot_early focus = "Construction Effort" }
		relative_position_id = SOV_finish_five_year_plan
		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			251 = {
				add_extra_state_shared_building_slots = 7
				add_building_construction = {
					type = infrastructure
					level = 2
					instant_build = yes
				}
			}
			652 = {
				add_extra_state_shared_building_slots = 7
				add_building_construction = {
					type = infrastructure
					level = 3
					instant_build = yes
				}
			}
			651 = {
				add_extra_state_shared_building_slots = 7
				add_building_construction = {
					type = infrastructure
					level = 2
					instant_build = yes
				}
			}
			249 = {
				add_extra_state_shared_building_slots = 7
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			398= {
				add_extra_state_shared_building_slots = 7
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			400 = {
				add_extra_state_shared_building_slots = 7
				add_building_construction = {
					type = infrastructure
					level = 3
					instant_build = yes
				}
			}
			399 = {
				add_extra_state_shared_building_slots = 7
				add_building_construction = {
					type = infrastructure
					level = 4
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = SOV_peoples_commissariat
		icon = GFX_goal_support_communism
		prerequisite = { focus = SOV_improve_railway }
		x = 0
		y = 1
		relative_position_id = SOV_improve_railway
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_ideas = peoples_commissariat_focus
		}
	}

	focus = {
		id = SOV_focus_civs
		text = "Expand Civilian Economy"
		icon = GFX_goal_generic_construct_civ_factory
		prerequisite = { focus = SOV_improve_railway }
		x = -2
		y = 1
		relative_position_id = SOV_improve_railway
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			219 = {
				add_extra_state_shared_building_slots = 4
				add_building_construction = {
					type = industrial_complex
					level = 4
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = SOV_focus_mills
		text = "Expand The Military Sector"
		icon = GFX_goal_generic_construct_mil_factory
		prerequisite = { focus = SOV_improve_railway }
		x = 2
		y = 1
		relative_position_id = SOV_improve_railway
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			219 = {
				add_extra_state_shared_building_slots = 8
				add_building_construction = {
					type = arms_factory
					level = 8
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = SOV_tranformation_of_nature
		icon = GFX_SOV_transformation_of_nature
		prerequisite = { focus =  SOV_peoples_commissariat }
		x = -1
		y = 1
		relative_position_id = SOV_peoples_commissariat
		cost = 5
		ai_will_do = {
			factor = 5
		}

		available = {
		
		}

		search_filters = { FOCUS_FILTER_INDUSTRY FOCUS_FILTER_POLITICAL }
		completion_reward = {
			add_resource = {
				type = rubber
				amount = 30
				state = 573
			}
			add_resource = {
				type = steel
				amount = 180
				state = 573 
			}
			add_resource = {
				type = tungsten
				amount = 180
				state = 573
			}
			add_resource = {
				type = chromium
				amount = 180
				state = 573
			}
			add_resource = {
				type = aluminium
				amount = 180
				state = 573
			}
			add_tech_bonus = {
				name = SOV_tranformation_of_nature
				bonus = 1.0
				uses = 3
				technology = excavation1
				technology = excavation2
				technology = excavation3
				technology = excavation4
				technology = excavation5
			}
		}
	}

	focus = {
		id = SOV_stalin_constitution
		icon = GFX_SOV_construction
		x = 8
		y = 0
		cost = 10
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			add_ideas = war_economy
		}
	}

	focus = {
		id = "Soviet Tank Plants"
		icon = GFX_SOV_tankograd
		prerequisite = { focus = SOV_progress_cult }
		x = -2
		y = 2
		relative_position_id = SOV_stalin_constitution
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
				add_tech_bonus = {
				name = "Soviet Tank Research"
				ahead_reduction = 3
				uses = 1
				technology = basic_medium_tank
			}
				add_tech_bonus = {
				name = "Soviet Tank Research"
				bonus = 1.0
				uses = 1
				technology = improved_medium_tank
			}
			add_tech_bonus = {
				name = "Soviet Tank Research"
				bonus = 1.0
				uses = 1
				technology = improved_heavy_tank
			}
			add_tech_bonus = {
				name = armor_bonus
				bonus = 1.0
				uses = 1
				technology = advanced_light_tank
			}
			hidden_effect = {country_event = { id = bhutan.7 }}
			set_technology = { armored_car_at_upgrade = 1 }
			217 = {
				add_extra_state_shared_building_slots = 3
				add_building_construction = {
					type = arms_factory
					level = 3
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = "Mechanized Warfare"
		icon = GFX_goal_generic_army_motorized
		prerequisite = { focus = SOV_progress_cult }
		x = 1
		y = 2
		relative_position_id = SOV_stalin_constitution
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_tech_bonus = {
				name = "Mechanized Warfare"
				ahead_reduction = 1
				bonus = 2.0
				uses = 1
				technology = mechanised_infantry
			}
			add_tech_bonus = {
				name = "Mechanized Warfare"
				bonus = 2.0
				ahead_reduction = 1
				uses = 1
				technology = mechanised_infantry2
			}
			add_tech_bonus = {
				name = "Gun Bonus"
				bonus = 1.2
				uses = 4
				category = infantry_weapons
			}
			195 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = SOV_collectivist_propaganda
		icon = GFX_SOV_collectivist_propaganda
		prerequisite = { focus = SOV_stalin_constitution }
		x = -2
		y = 1
		relative_position_id = SOV_stalin_constitution
		cost = 5
		ai_will_do = {
			factor = 0.25
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_POLITICAL FOCUS_FILTER_STABILITY }
		completion_reward = {
			add_political_power = -25
			add_stability = 0.05
		}
	}

	focus = {
		id = SOV_militarized_schools
		icon = GFX_SOV_militarized_schools
		prerequisite = { focus = "Soviet Tank Plants" }
		x = -3
		y = 1
		relative_position_id = "Soviet Tank Plants"
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_MANPOWER }
		completion_reward = {
			add_political_power = -50
			add_ideas = militarized_schools
			add_ideas = extensive_conscription
		}
	}

	focus = {
		id = "ZiS-3 76mm Field Gun"
		icon = GFX_SOV_tank_hunt
		prerequisite = { focus = "Soviet Tank Plants" }
		mutually_exclusive = {
			focus = "Anti-Tank mass production"
		}
		x = 3
		y = 1
		relative_position_id = "Soviet Tank Plants"
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_MANPOWER }
		completion_reward = {
			add_tech_bonus = {
				name = "Anti Tank Modernisation"
				bonus = 1
				uses = 4
				technology = antitank2
				technology = antitank3
				technology = antitank4
				technology = antitank5
			}
			
			add_ideas = SOV_HA_production
		}
	}

	focus = {
		id = "Anti-Tank mass production"
		icon = GFX_SOV_tank_hunt
		prerequisite = { focus = "Soviet Tank Plants" }
		mutually_exclusive = {
			focus = "ZiS-3 76mm Field Gun"
		}
		x = 5
		y = 1
		relative_position_id = "Soviet Tank Plants"
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_MANPOWER }
		completion_reward = {			
			add_ideas = SOV_at_production
			218 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = "Pushka 152mm"
		icon = GFX_goal_generic_army_artillery2
		prerequisite = { focus = "ZiS-3 76mm Field Gun" focus = "Anti-Tank mass production" }
		x = 4
		y = 2
		relative_position_id = "Soviet Tank Plants"
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_MANPOWER }
		completion_reward = {
			add_tech_bonus = {
				name = "Arty Modernisation"
				bonus = 1.0
				uses = 2
				category = artillery
			}
			add_ideas = SOV_arty_production
			202 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = arms_factory
					level = 2
					instant_build = yes
				}
			}
		}
	}

	focus = {
		id = SOV_focus_support_equipment
		text = "Support Equipment Production"
		icon = GFX_focus_generic_combined_arms
		prerequisite = { focus = "Soviet Tank Plants" }
		
		x = 6
		y = 2
		relative_position_id = "Soviet Tank Plants"
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_MANPOWER }
		completion_reward = {			
			add_ideas = SOV_support_equipment 
		}
	}

	focus = {
		id = SOV_focus_siege_tactics
		text = "Siege Tactics"
		icon = GFX_goal_generic_fortify_city
		prerequisite = { focus = SOV_focus_every_2nd_soldier_gets_a_gun }
		x = 5
		y = 4
		relative_position_id = "Soviet Tank Plants"
		cost = 5
		ai_will_do = {
			factor = 1
		}
		available = {
			AND = {
				has_tech = antitank2
				has_tech = artillery1
				has_tech = improved_infantry_weapons
			} 
		}
		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_MANPOWER }
		
		completion_reward = {	
			custom_effect_tooltip = "Will add 7500 Artillery 2, 50000 Infantry Equipment 2, 12500 Anti Tank 2, 6000 Anti Air Equipment to our Stockpile"
			hidden_effect = {
				add_equipment_to_stockpile = {
					type = artillery_equipment_2
					amount = 7500
				} 
				add_equipment_to_stockpile = {
					type = infantry_equipment_2
					amount = 50000
				} 
				add_equipment_to_stockpile = {
					type = anti_tank_equipment_2
					amount = 12500
				}
				add_equipment_to_stockpile = {
					type = anti_air_equipment_1
					amount = 6000
				}
			}
		}
	}

	focus = {
		id = SOV_focus_every_2nd_soldier_gets_a_gun
		text = "Every 2nd Soldier Gets A Gun"
		icon = GFX_focus_generic_manpower
		prerequisite = { focus = SOV_focus_support_equipment }
		prerequisite = { focus = "Pushka 152mm" }
		x = 5
		y = 3
		relative_position_id = "Soviet Tank Plants"
		cost = 5
		ai_will_do = {
			factor = 1
		}
		available = {
			has_tech = improved_infantry_weapons
		}
		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_MANPOWER }
		completion_reward = {			
			add_ideas = SOV_gun_production
		}
	}

	focus = {
		id = "T-44-85" 
		icon = GFX_focus_generic_tank_production
		prerequisite = { focus = "Soviet Tank Plants" }
		mutually_exclusive = {
			focus = "T-34-85"
		}
		x = -1
		y = 1
		relative_position_id = "Soviet Tank Plants"
		cost = 5
		ai_will_do = {
			factor = 1
		}
	
		available = {
			date > 1939.1.1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_tech_bonus = {
				name = armor_bonus
				bonus = 1
				uses = 1
				technology = advanced_medium_tank
			}
			add_tech_bonus = {
				name = armor_bonus
				bonus = 1
				uses = 1
				technology = advanced_heavy_tank
			}
			hidden_effect = {country_event = { id = bhutan.7 }}
		}
	}

	focus = {
		id = "T-34-85"
		icon = GFX_focus_generic_tank_production
		prerequisite = { focus = "Soviet Tank Plants" }
		mutually_exclusive = {
			focus = "T-44-85"
		}
		x = 1
		y = 1
		relative_position_id = "Soviet Tank Plants"
		cost = 5
		ai_will_do = {
			factor = 1
		}
	
		available = {
			date > 1939.1.1
			has_tech = improved_medium_tank
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_ideas = SOV_t34 
			set_technology = {
				t3485 = 1
			}
		}
	}

	focus = {
		id = "KV-2"
		icon = GFX_SOV_heavy_assault_guns
		prerequisite = { focus = "T-44-85" focus = "T-34-85" }
		x = 1
		y = 2
		relative_position_id = "T-44-85"
		cost = 5
		ai_will_do = {
			factor = 1 
		}

		available = { 
			OR = {
				date > 1942.6.1 
			}
		}
		available_if_capitulated = yes
		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			custom_effect_tooltip = "Will raise 2 KV-2 Divisions"
			set_technology = { advanced_heavy_td = 1 }
			hidden_effect = {
			load_oob = SOV_KV-2
			}
		}
	}

	focus = {
		id = "Construction Effort"
		icon = GFX_SOV_expansion
		prerequisite = { focus = "Long-Term Investments" focus = "Short-Term Investments" focus = "Medium-Term Investments"}
		x = 1
		y = 2
		relative_position_id = SOV_finish_five_year_plan
		cost = 10 
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_ideas = workers_culture
		}
	}

	focus = {
		id = SOV_progress_cult
		icon = GFX_SOV_research_slot
		prerequisite = { focus = SOV_stalin_constitution }
		x = 0
		y = 1
		relative_position_id = SOV_stalin_constitution
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			add_political_power = -25
			add_ideas = progress_cult_focus
			add_research_slot = 1
		}
	}
	
	
	focus = {
		id = SOV_positive_heroism
		icon = GFX_SOV_positive_heroism
		prerequisite = { focus = "Soviet Tank Plants" }
		mutually_exclusive = {
			focus = "Industrial Supremacy"
		}
		x = 2
		y = 2
		relative_position_id = "Soviet Tank Plants"
		cost = 10
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		completion_reward = {
			custom_effect_tooltip = available_military_high_command
			show_ideas_tooltip = konstantin_rokossovsky
			show_ideas_tooltip = kliment_voroshilov
			every_unit_leader = {
				limit={has_id=410}
				add_attack = 2
				add_defense = 3
			} 
			# REVISIT Might want to add more
		}
	}

	focus = {
		id = "Industrial Supremacy"
		icon = GFX_SOV_industrial_supremacy
		prerequisite = { focus = "Soviet Tank Plants" }
		mutually_exclusive = {
			focus = SOV_positive_heroism
		}
		x = -2
		y = 2
		relative_position_id = "Soviet Tank Plants"
		cost = 10
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		completion_reward = {
			add_ideas = SOV_industrial_supremacy
			show_ideas_tooltip = pavel_zhigarev
		}
	}

	focus = {
		id = SOV_socialist_realism
		icon = GFX_SOV_socialist_realism
		prerequisite = { focus = SOV_stalin_constitution }
		x = 2
		y = 1
		relative_position_id = SOV_stalin_constitution
		cost = 5
		ai_will_do = {
			factor = 1
		}
 
		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = {
			add_timed_idea = {
				idea = SOV_socialist_realism
				days = 180
			}
		}
	}

	
	focus = {
		id = SOV_expand_red_fleet
		icon = GFX_goal_generic_build_navy
		prerequisite = { focus = SOV_peoples_commissariat }
		available = {  }
		x = 1
		y = 1
		relative_position_id = SOV_peoples_commissariat
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		complete_tooltip = {
						
		}

		completion_reward = {
			add_offsite_building = {
				type = dockyard
				level = 20
			}
			add_tech_bonus = {
				name = ships_bonus
				bonus = 1.0
				uses = 2
				category = naval_equipment
			}
		}
	}

	focus = {
		id = SOV_focus_black_sea_fleet
		text = "Black Sea Fleet"
		icon = GFX_goal_generic_navy_battleship
		prerequisite = { focus = SOV_expand_red_fleet }
		available = {  }
		mutually_exclusive = { focus = SOV_focus_baltic_sea_fleet }
		x = 0
		y = 2
		relative_position_id = SOV_peoples_commissariat
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		complete_tooltip = {
						
		}
 
		completion_reward = {
			add_ideas = SOV_capital_ship
			add_offsite_building = {
				type = dockyard
				level = 12
			}
		}
	}

	focus = {
		id = SOV_focus_baltic_sea_fleet
		text = "Baltic Sea Fleet"
		icon = GFX_goal_generic_navy_submarine
		prerequisite = { focus = SOV_expand_red_fleet }
		available = {  }
		mutually_exclusive = { focus = SOV_focus_black_sea_fleet }
		x = 2
		y = 2
		relative_position_id = SOV_peoples_commissariat
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		complete_tooltip = {
						
		}

		completion_reward = {
			add_ideas = SOV_submarine
			add_offsite_building = {
				type = dockyard
				level = 12
			}
		}
	}

	focus = {
		id = SOV_great_purge
		icon = GFX_SOV_purge
		cancelable = no
		available = {
			date > 1938.6.1
		}
		x = 18
		y = 0
		cost = 20
		ai_will_do = {
			factor = 10
			modifier = {
				factor = 0

			}
		}
		
		search_filters = { FOCUS_FILTER_POLITICAL }
		completion_reward = { 
			add_political_power = -250
			add_ideas = officers_purged
			remove_ideas = trotskyite_plot
			country_event = { id = news.219 days = 1}
	
		}
	}

	

	
	




	focus = {
		id = SOV_lessons_of_war
		icon = GFX_SOV_lessons_of_war
		prerequisite = { focus = SOV_great_purge }

		available = {
		}
		x = 1
		y = 1
		relative_position_id = SOV_great_purge
		cost = 10
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 1000
			}
		}

		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			add_tech_bonus = {
				name = armor_bonus
				bonus = 3.0
				uses = 3
				category = land_doctrine
			}
			add_tech_bonus = {
				name = "Soviet Tank Research"
				bonus = 1.5
				uses = 1
				technology = mechanised_infantry3
			}
			swap_ideas = {
				remove_idea = officers_purged
				add_idea = officers_purged2 
			}
			
		}
	}


	focus = {
		id = SOV_research_city_experiment
		icon = GFX_SOV_research_city_experiment
		prerequisite = { focus = SOV_great_purge }
		relative_position_id = SOV_great_purge
		available = {

		}
		x = -1
		y = 1
		
		cost = 5 
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			swap_ideas = {
				remove_idea = progress_cult_focus
				add_idea = progress_cult_focus2 
			}
			add_tech_bonus = {
				name = "Radar Modernisation"
				bonus = 1.0
				uses = 4
				category = radar_tech
			}
		}
	}


	focus = {
		id = SOV_closed_city_network
		icon = GFX_SOV_research_slot
		prerequisite = { focus = SOV_research_city_experiment }
		available = {

		}
		x = -1
		y = 1
		relative_position_id = SOV_research_city_experiment
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			add_research_slot = 1
		}
	}

	focus = {
		id = "Prisoners of War"
		icon = GFX_focus_YUG_pan_slavic_congress
		prerequisite = { focus = SOV_nkvd_divisions }
		
		x = -1
		y = 3
		relative_position_id = SOV_research_city_experiment
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			add_timed_idea = { idea = SOV_prisoners_of_war days = 75 }
		}
	}

	focus = {
		id = "The Gulag"
		icon = GFX_focus_YUG_pan_slavic_congress
		prerequisite = { focus = SOV_nkvd_divisions }
		available = {
			
		}
		x = 1
		y = 3
		relative_position_id = SOV_research_city_experiment
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			add_timed_idea = { idea = SOV_the_gulag days = 200 }
		}
	}

	focus = {
		id = SOV_focus_nkvd_defense_forces
		text = "Defensive Warfare"
		icon = GFX_goal_generic_cavalry
		prerequisite = { focus = SOV_nkvd_divisions }
		
		x = 0
		y = 4
		relative_position_id = SOV_research_city_experiment
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			swap_ideas = {
				remove_idea = nkvd_2
				add_idea = nkvd_3
			}
		}
	}
############## LARP


	focus = {
		id = "Attack Germany"
		icon = GFX_SOV_attack_germany
		available = {
			date > 1941.9.1
		}
		x = 22
		y = 0
		cost = 1
		ai_will_do = {
			factor = 1
		}

		bypass = { has_war_with = GER}

		

		completion_reward = {
			custom_effect_tooltip = "We will declare war on Germany"
		hidden_effect = { 
			GER = { activate_decision = "Unternehmen Barbarossa" }
			}
			remove_ideas = SOV_disorganized_army
		}
	}


	focus = {
		id = SOV_Great_Patriotic_War
		icon = GFX_SOV_great_patriotic_war
		available = {
			has_war = yes
		}
		x = 4
		y = 1
		cost = 5
		ai_will_do = {
			factor = 1
		}
		prerequisite = { focus = SOV_lessons_of_war }
		relative_position_id = SOV_lessons_of_war
		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			swap_ideas = {
				remove_idea = home_of_revolution
				add_idea = home_of_revolution2	
			}
			add_ideas = tot_economic_mobilisation
			
		}
		
	}
	
	focus = {
		id = "Order 227"
		icon = GFX_SOV_order_227
		prerequisite = { focus = "Order 270" }
		x = -1
		y = 2
		relative_position_id = SOV_Great_Patriotic_War
		cost = 1
		ai_will_do = {
			factor = 1
		}
		available = {
			date > 1942.3.1
		}
		

		available_if_capitulated = yes

		completion_reward = {
			hidden_effect = {scoped_sound_effect = "sp_sov_stalin"
			BHU = { country_event = news.5002 }
			}
			swap_ideas = {
				remove_idea = SOV_disorganized_army2
				add_idea = SOV_disorganized_army3	
			}
			
		}
	}

	focus = {
		id = "The First Winter Counter Offensive"
		icon = GFX_SOV_lessons_of_the_winter_war
		prerequisite = { focus = "Order 270" }
		x = -3
		y = 2
		relative_position_id = SOV_Great_Patriotic_War
		cost = 1 
		ai_will_do = {
			factor = 1
		}
		available = {
			date > 1941.11.1  
			date < 1942.2.1 
		}
		
		available_if_capitulated = yes
		completion_reward = {
			hidden_effect = {
			BHU = { country_event = news.5004 }
			}
			add_timed_idea = {
				idea = SOV_winter_offensive1
				days = 90
			}
		}
	}

	focus = {
		id = "The Second Winter Counter Offensive"
		icon = GFX_SOV_lessons_of_the_winter_war
		prerequisite = { focus = "Order 270" }
		x = 1
		y = 2
		relative_position_id = SOV_Great_Patriotic_War
		cost = 1
		ai_will_do = {
			factor = 1
		}
		available = {
				date > 1942.11.1
				date < 1943.2.1
			}
			

		
		

		available_if_capitulated = yes

		completion_reward = {
			hidden_effect = {
			BHU = { country_event = news.5004 }
			}
			add_timed_idea = {
				idea = SOV_winter_offensive2
				days = 90
			}
		}
	}

	focus = {
		id = "The Third Winter Counter Offensive"
		icon = GFX_SOV_lessons_of_the_winter_war
		prerequisite = { focus = "Order 270" }
		x = -2
		y = 3
		relative_position_id = SOV_Great_Patriotic_War
		cost = 1
		ai_will_do = {
			factor = 1
		}
		available = {
				date > 1943.11.1
				date < 1944.2.1
			}
			

		
		

		available_if_capitulated = yes

		completion_reward = {
			hidden_effect = {
			BHU = { country_event = news.5004 }
			}
			add_timed_idea = {
				idea = SOV_winter_offensive3
				days = 90
			}
		}
	}

	focus = {
		id = "The Final Winter Counter Offensive"
		icon = GFX_SOV_lessons_of_the_winter_war
		prerequisite = { focus = "Order 270" }
		x = 0
		y = 3
		relative_position_id = SOV_Great_Patriotic_War
		cost = 1
		ai_will_do = {
			factor = 1
		}
		available = {
				date > 1944.11.1
				date < 1945.2.1
			}
			

		
		

		available_if_capitulated = yes

		completion_reward = {
			hidden_effect = {
			BHU = { country_event = news.5004 }
			}
			add_timed_idea = {
				idea = SOV_winter_offensive4
				days = 90
			}
		}
	}

	focus = {
		id = "Man of Steel"
		icon = GFX_SOV_order_227
		prerequisite = { focus = "Order 227" }
		x = -1
		y = 4
		relative_position_id = SOV_Great_Patriotic_War
		cost = 1
		ai_will_do = {
			factor = 1
		}
		available = {
			date > 1943.1.1
		}
		

		available_if_capitulated = yes

		completion_reward = {
			swap_ideas = {
				remove_idea = SOV_disorganized_army3
				add_idea = SOV_man_of_steel
			}
		}
	}

	focus = {
		id = "Order 270"
		icon = GFX_SOV_order_227
		prerequisite = { focus = SOV_Great_Patriotic_War }
		x = -1
		y = 1
		relative_position_id = SOV_Great_Patriotic_War
		cost = 1
		ai_will_do = {
			factor = 1
		}
		available = {
			date > 1941.9.1
		}
		

		available_if_capitulated = yes

		completion_reward = {
			hidden_effect = {
				BHU = { country_event = news.5003 }
			}
			
			swap_ideas = {
				remove_idea = SOV_disorganized_army
				add_idea = SOV_disorganized_army2	
			}
		}
	}

	focus = {
		id = "Allied Help"
		icon = GFX_SOV_allied_help
		prerequisite = { focus = SOV_Great_Patriotic_War }
		x = 4
		y = 1
		relative_position_id = SOV_Great_Patriotic_War
		cost = 1
		ai_will_do = {
			factor = 1
		}
		available = {
				date > 1942.1.1
		}

		available_if_capitulated = yes

		completion_reward = {
			add_offsite_building = { type = industrial_complex level = 10 }
			add_offsite_building = { type = arms_factory level = 15  }
			add_ideas = SOV_allied_help 
			ENG = {add_ideas = ENG_donation_to_the_soviet_union}
		}
	}

	focus = {
		id = "Allied Tank Shipments"
		icon = GFX_SOV_lend_lease
		prerequisite = { focus = "Allied Help" }
		x = 5
		y = 2
		relative_position_id = SOV_Great_Patriotic_War
		cost = 1
		ai_will_do = {
			factor = 1
		}
		available = {
			OR = {
				NOT = { SOV = { controls_province = 413 }}
				NOT = {	SOV = { controls_province = 6380 }}
				NOT = {	SOV = { controls_province = 3151 }}
			}				
		}

		available_if_capitulated = yes 

		
		completion_reward = {
			custom_effect_tooltip = SOV_ALLIED_TANK_LL
			hidden_effect = {
			add_equipment_to_stockpile = {
			type = medium_tank_equipment_2 

			amount = 4000

			}

			add_equipment_to_stockpile = {
			type = medium_tank_equipment_3 

			amount = 2000

			}
			add_equipment_to_stockpile = {
				type = mechanized_equipment_1 
	
				amount = 1700
	
				}
	
				add_equipment_to_stockpile = {
				type = mechanized_equipment_2 
	
				amount = 3300
	
				}
			}
		}
	}


	focus = {
		id = "Allied Gun Shipments"
		icon = GFX_SOV_lend_lease
		prerequisite = { focus = "Allied Help" }
		x = 3
		y = 2
		relative_position_id = SOV_Great_Patriotic_War
		cost = 1
		ai_will_do = {
			factor = 1
		}
		available = {
			
		}
				

		available_if_capitulated = yes

		completion_reward = {
			add_equipment_to_stockpile = {
			type = infantry_equipment_0

			amount = 100000

			}
		}
	}
	focus = {
		id = "Military Air Forces"
		icon = GFX_SOV_fighter_designer
		prerequisite = { focus = SOV_progress_cult }
		x = 6
		y = 2
		relative_position_id = SOV_stalin_constitution
		cost = 5
		ai_will_do = {
			factor = 1
		}
		available = {date > 1938.1.1}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			if = { limit = { NOT = { has_idea = mig_design_bureau }} add_ideas = mig_design_bureau add_political_power = -50  }
			set_technology = { fighter2 = 1 }
			408 = {
				add_extra_state_shared_building_slots = 10
				add_building_construction = {
					type = arms_factory
					level = 10
					instant_build = yes
				}
			}
			hidden_effect = {
				SOV = {
					add_equipment_production = {
						equipment = {
							type = fighter_equipment_2
						}		
						requested_factories = 150
						amount = 0
					}
				}
				
			}
		}
	}

	focus = {
		id = "Ilyushin Il-2 Shturmovik"
		icon = GFX_SOV_cas_designer
		prerequisite = { focus = "Military Air Forces" }
		x = -1
		y = 1
		relative_position_id = "Military Air Forces"
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_tech_bonus = {
				name = "CAS"
				bonus = 3.0
				uses = 2
				category = cas_bomber			
			}
		}
	}

	focus = {
		id = "Petlyakov Pe-3"
		icon = GFX_SOV_strat_bomber_doctrine
		prerequisite = { focus = "Military Air Forces" }
		x = 1
		y = 1
		relative_position_id = "Military Air Forces"
		cost = 5
		ai_will_do = {
			factor = 1
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			add_tech_bonus = {
				name = "TACS"
				bonus = 1.0
				uses = 2
				category = tactical_bomber
			}
		}
	}

	focus = {
		id = "Yakolev Yak-3"
		icon = GFX_SOV_yak_3
		prerequisite = { focus = "Military Air Forces" }
		x = 0
		y = 2
		relative_position_id = "Military Air Forces"
		cost = 5
		ai_will_do = {
			factor = 1
		}
		available = {date > 1941.1.1}
		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_INDUSTRY }
		completion_reward = {
			if = { limit = { NOT = { has_idea = mig_design_bureau }} add_ideas = mig_design_bureau add_political_power = -50  }
			set_technology = { fighter3 = 1 }
		}
	}

	focus = {
		id = SOV_military_reorganization
		icon = GFX_SOV_communist_army
		prerequisite = { focus = SOV_lessons_of_war }
		relative_position_id = SOV_lessons_of_war
		x = 1
		y = 1
		cost = 10
		ai_will_do = {
			factor = 0.25
			modifier = {
				factor = 1000
				any_claim = yes
				has_war = yes
			}
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			add_tech_bonus = {
				bonus = 3.0
				uses = 1
				category = land_doctrine
			}
		}
	}
	focus = {
		id = SOV_rehabilitated_military
		icon = GFX_SOV_communist_army
		prerequisite = { focus = SOV_lessons_of_war }
		relative_position_id = SOV_lessons_of_war
		x = -1
		y = 1
		
		cost = 10
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 100
				any_claim = yes
				has_war = yes
			}
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_MANPOWER }
		completion_reward = {
			add_ideas = rehabilitated_military_focus
		}
	}
	focus = {
		id = "Officer Schools"
		icon = GFX_SOV_communist_army
		prerequisite = { focus = SOV_rehabilitated_military }
		prerequisite = { focus = SOV_military_reorganization }
		mutually_exclusive = {
			focus = SOV_nkvd_divisions
		}
		relative_position_id = SOV_lessons_of_war
		x = 0
		y = 2
		cost = 20
		ai_will_do = {
			factor = 0.25
			modifier = {
				factor = 1000
				any_claim = yes
				has_war = yes
			}
		}

		available_if_capitulated = yes

		search_filters = { FOCUS_FILTER_RESEARCH }
		completion_reward = {
			swap_ideas = {
				remove_idea = officers_purged2
				add_idea = officers_purged3
			}
			add_tech_bonus = {
				bonus = 3.0
				uses = 1
				category = land_doctrine
			}
		}
	}
	focus = {
		id = SOV_nkvd_divisions
		icon = GFX_SOV_nkvd_divisions
		prerequisite = { focus = SOV_military_reorganization }
		prerequisite = { focus = SOV_rehabilitated_military }
		mutually_exclusive = {
			focus = "Officer Schools"
		}
		x = -3
		y = 1
		relative_position_id = SOV_military_reorganization
		cost = 20
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 1000
				any_claim = yes
				has_war = yes
			}
		}

		available_if_capitulated = yes

		completion_reward = {
			custom_effect_tooltip = SOV_nkvd_upgrade_tooltip

			swap_ideas = {
				remove_idea = nkvd
				add_idea = nkvd_2
			}
		}
	}
}
