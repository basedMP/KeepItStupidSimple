state={
	id=126
	name="STATE_126"
	manpower=9824296
	
	state_category = megalopolis
	resources = { 
		aluminium = 289
		oil = 300
	 }

	history={
		owner = ENG
		victory_points = {
			6103 50 
		}
		buildings = {
			infrastructure = 9
			industrial_complex = 2
			dockyard = 17
			anti_air_building = 5
			radar_station = 1
			air_base = 20
			11374 = {
				naval_base = 10
			}
		}
		add_extra_state_shared_building_slots = 24
		add_core_of = ENG

	}

	provinces={
		6103 9239 11333 11374 
	}
}
