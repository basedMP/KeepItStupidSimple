
state={
	id=6
	name="STATE_6" # Flanders
	manpower = 4747700
	
	
	state_category = metropolis

	history={
		owner = BEL
		victory_points = { 516 30 }
		victory_points = { 6598 30 }
		victory_points = { 6560 5 }
		victory_points = { 13068 1 }
		victory_points = { 3576 1 }
		buildings = {
			infrastructure = 9
			arms_factory = 2
			dockyard = 2
			air_base = 4
			6560 = {	naval_base = 2	}
			6598 = {	naval_base = 7	}
		}	
		add_extra_state_shared_building_slots = 3
		
		add_core_of = BEL

	}
	

	provinces={
		516 3576 6446 6560 6598 9574 11419 13068 
	}
}
