state={
	id=582
	name="STATE_582"
	resources={
		steel=227.000
		chromium=71
	}

	history={
		owner = SOV
		buildings = {
			infrastructure = 5
			anti_air_building = 7
			air_base = 4

		}
			 
		
		add_core_of = SOV
		victory_points = {
			10256 1
		}
	}

	provinces={
		1334 1406 4215 4261 4265 4311 4383 4411 7281 7300 7328 7373 7401 10185 10196 10224 10256 12200 
	}
	manpower=1044991
	buildings_max_level_factor=1.000
	state_category=rural
}
