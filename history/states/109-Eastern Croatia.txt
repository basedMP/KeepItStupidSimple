
state={
	id=109
	name="STATE_109" # Croatia
	manpower = 2224400
	
	state_category = large_town

	history={
		add_extra_state_shared_building_slots = 6
		owner = YUG
		buildings = {
			infrastructure = 7
			industrial_complex = 3
		}
		victory_points = {
			11581 3 
		}
		add_core_of = YUG
			
	}

	provinces={
		624 3592 3596 3627 6647 9595 9611 11577 11580 11581 11594
	}
}
