
state={
	id=434
	name="STATE_434"
	manpower = 200000
	
	state_category = wasteland
	resources={
		tungsten=49 # was: 54
	}

	history={
		owner = ENG
		buildings = {
			infrastructure = 1
		}
		1936.1.1.13 = {
			owner = RAJ
			add_core_of = RAJ
		}
	}

	provinces={
		1116 1996 10810 12322 12730 
	}
}
