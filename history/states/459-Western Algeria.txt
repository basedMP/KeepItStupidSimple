
state={
	id=459
	name="STATE_459" # Algiers
	manpower = 3089944
	state_category = town
	resources={
		oil=4 #oil=90 # was: 70
		rubber=4
		aluminium=10
		
	}
	
	history={
		owner = FRA
		set_demilitarized_zone = yes
		victory_points = {
			1145 1 
		}
		victory_points = {
			7132 1 
		}
		buildings = {
			infrastructure = 10
			air_base = 4
			1145 = {
				naval_base = 2
			}
			7132 = {
				naval_base = 6
			}
		}
		
	}

	provinces={
		1013 1145 4116 4131 4146 7132 9964 11923 11939 12760 
	}
}
