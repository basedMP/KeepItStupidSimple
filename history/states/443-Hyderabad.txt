
state={
	id=443
	name="STATE_443" # Sind
	manpower = 3887000
	
	state_category = rural

	history={
		owner = ENG
		victory_points = {
			3456 3 
		}
		buildings = {
			infrastructure = 10
			industrial_complex = 1
			3456 = {
				naval_base = 20
			}
		}
		1936.1.1.13 = {
			owner = RAJ
			add_core_of = RAJ
		}

	}

	provinces={
		1147 1971 1980 3456 4003 4110 4382 7049 7084 10108 10735 
	}
}
