state={
	id=191
	name="STATE_191"
	
	state_category = rural

	history={
		owner = BHU
		victory_points = {
			9221 1 
		}
		buildings = {
			infrastructure = 8
			arms_factory =  8
			industrial_complex = 9
			4640 = {
				naval_base = 1
			}
			dockyard = 1

		}
		add_core_of = BHU
		add_extra_state_shared_building_slots = 24

	}

	provinces={
		592 4640 6099 6178 6408 9221 9485 11057 11443 13127 
	}
	manpower=376400
	buildings_max_level_factor=1.000
}
