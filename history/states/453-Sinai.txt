
state={
	id=453
	name="STATE_453"
	manpower = 15359
	
	state_category = wasteland
	resources = { oil = 10 }
	
	history = {
		owner = UAC
		buildings = {
			infrastructure = 10
			air_base = 4
			10099 = {
				naval_base = 3
			}
		}

		victory_points = {
			4161 3
		}
		victory_points = {
			10099 1
		}


	}

	provinces={
		13375 13374 1112 1155 4161 9947 10002 10099 11922 11979 12073 13244
	}
}
