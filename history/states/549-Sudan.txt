
state={
	id=549
	name="STATE_549"
	manpower = 3694293
	
	state_category = rural
	
	
	history={
		victory_points = {
			2096 500
		}
		owner = UAC
		add_extra_state_shared_building_slots = 30
		buildings = {
			infrastructure = 10
			air_base = 54
			industrial_complex = 22
			arms_factory = 4
		}

	}

	provinces={
		13505  13512 13513 13514 13515 13516 13517 13511 13509 13510 13506 13507 13508 1914 1953 1960 2016 2096 4033 4918 5060 7996 8052 8084 10739 10764 10795 10827 10851 10857 10859 10877 10908 12744 12800 12887 
	}
}
