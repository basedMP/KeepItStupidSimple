state=
{
	id=188
	name="STATE_188"
	manpower = 150000
	
	
	state_category = rural
	
	history = {
		victory_points = {
			3288 1
		}
		owner = BHU
		buildings = {
			infrastructure = 7
			industrial_complex = 3
			arms_factory = 1
			dockyard = 1 
			air_base = 20
			3288 = {
				naval_base = 1
			}
		}
		add_core_of = BHU
		add_extra_state_shared_building_slots = 4
		
	}
	provinces={
		3288 	
	}
}
