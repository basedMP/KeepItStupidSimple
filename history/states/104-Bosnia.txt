
state={
	id=104
	name="STATE_104"
	manpower =1910200
	
	
	state_category = town
	
	resources={
		#aluminium=7 # was: 10
	}

	history={
		add_extra_state_shared_building_slots = 10
		owner = YUG
		victory_points = {
			11899 5 
		}
		victory_points = {
			6983 3
		}
		buildings = {
			infrastructure = 8
			industrial_complex = 5
			arms_factory = 3
		}
		add_core_of = YUG
				
	}

	provinces={
		606 982 3985 6614 6619 6799 6957 6983 9586 9588 9591 9922 11572 11574 11741 11872 11899 
	}
}
