﻿##### Division Templates #####
division_template = {
	name = "Battle of France Veterans"				# Infantry Brigade

	division_names_group = FRA_INF_01

	regiments = {
		mechanized = { x = 0 y = 0 }
		mechanized = { x = 0 y = 1 }
		mechanized = { x = 0 y = 2 }		
		mechanized = { x = 0 y = 3}
		mechanized = { x = 1 y = 3 }
		mechanized = { x = 1 y = 0 }
		mechanized = { x = 1 y = 1 }
		mechanized = { x = 1 y = 2 }
		medium_armor = { x = 3 y = 0 }
		medium_armor = { x = 3 y = 2 }
		medium_armor = { x = 3 y = 1 }
		medium_armor = { x = 3 y = 3 }
		medium_armor = { x = 3 y = 4 }
		medium_armor = { x = 4 y = 0 }
		medium_armor = { x = 4 y = 1 }
		medium_armor = { x = 4 y = 2 }
		medium_armor = { x = 4 y = 3 }
		medium_armor = { x = 2 y = 0 }
		medium_armor = { x = 2 y = 1 }
		medium_armor = { x = 2 y = 2 }			
	}
	support = {
	logistics_company = { x = 0 y = 0 }
	engineer = { x = 0 y = 1 }
	maintenance_company = { x = 0 y = 2 }
	armored_car_recon = { x = 0 y =3}
	field_hospital = { x = 0 y = 4}
	military_police = { x = 1 y = 1}
	anti_air = { x = 1 y = 2}
	artillery = { x = 1 y = 3}
	anti_tank= { x = 1 y = 4}
	}
}

units = {

	division= {	
		#name = "1. Gyalogdandár"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 1
		}
		location = 2096
		division_template = "Battle of France Veterans"
		start_experience_factor = 1
		start_equipment_factor = 0.1

	}
division= {	
		#name = "1. Gyalogdandár"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 1
		}
		location = 2096
		division_template = "Battle of France Veterans"
		start_experience_factor = 1
		start_equipment_factor = 0.1

	}
division= {	
		#name = "1. Gyalogdandár"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 1
		}
		location = 2096
		division_template = "Battle of France Veterans"
		start_experience_factor = 1
		start_equipment_factor = 0.1

	}
division= {	
		#name = "1. Gyalogdandár"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 1
		}
		location = 2096
		division_template = "Battle of France Veterans"
		start_experience_factor = 1
		start_equipment_factor = 0.1

	}
}