﻿division_template = {
	name = "Africa Defense"				# Infantry Brigade

	division_names_group = ENG_INF_02

	regiments = {
		converts = { x = 0 y = 0 }
		converts = { x = 0 y = 1 }
		converts = { x = 0 y = 2 }
		converts = { x = 0 y = 3 }
		converts = { x = 0 y = 4 }
		converts = { x = 1 y = 0 }
		converts = { x = 1 y = 1 }
		converts = { x = 1 y = 2 }
		converts = { x = 1 y = 3 }
		converts = { x = 1 y = 4 }
		converts = { x = 2 y = 0 }
		

	}
	support = {
	anti_air = { x = 0 y = 0 } recon = { x = 1 y = 0 }
	engineer = { x = 0 y = 1 } signal_company = { x = 1 y = 1 }
	anti_tank = { x = 0 y = 2 } field_hospital = { x = 1 y = 2 } 
	artillery = { x = 0 y = 3 } logistics_company = { x = 1 y = 3 }
	
	}
}

units = {
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7011
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7011
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7011
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7011
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13371
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13371
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13371
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13371
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13373
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13373
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13373
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13373
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10126
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10126
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10126
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10126
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1080
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1080
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1080
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1080
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 9957
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 9957
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 9957
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 9957
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13366
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13366
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13366
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13366
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 11974
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 11974
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 11974
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 11974
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13307
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13307
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13307
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13307
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7030
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7030
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7030
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7030
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7188
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7188
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7188
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7188
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4143
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4143
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4143
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4143
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13488
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13488
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13488
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13488
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12004
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12004
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12004
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12004
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4073
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4073
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4073
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4073
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12049
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12049
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12049
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12049
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10005
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10005
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10005
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10005
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13372
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13372
		division_template = "Africa Defense"
	}

	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13372
		division_template = "Africa Defense"
	}

	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13372
		division_template = "Africa Defense"
	}

	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10073
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10073
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10073
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10073
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13263
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13263
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13263
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13263
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 11964
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 11964
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 11964
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 11964
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7091
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7091
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7091
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7091
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13476
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13476
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13476
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13476
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13280
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13280
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13280
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13280
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13306
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13306
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13306
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13306
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12044
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12044
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12044
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 12044
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13281
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13281
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13281
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13281
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13477
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13477
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13477
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13477
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13282
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13282
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13282
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13282
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13478
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13478
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13478
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13478
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13479
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13479
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13479
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13479
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7144
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7144
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7144
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7144
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13283
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13283
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13283
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13283
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13480
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13480
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13480
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13480
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13315
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13315
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13315
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13315
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10031
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10031
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10031
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10031
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1068
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1068
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1068
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1068
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4055
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4055
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4055
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4055
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 3996
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 3996
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 3996
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 3996
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7164
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7164
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7164
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7164
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13305
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13305
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13305
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13305
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13304
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13304
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13304
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13304
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4076
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4076
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4076
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4076
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13303
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13303
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13303
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13303
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 992
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 992
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 992
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 992
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13259
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13259
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13259
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13259
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13258
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13258
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13258
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13258
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13257
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13257
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13257
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13257
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13242
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13242
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13242
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13242
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13481
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13481
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13481
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13481
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10127
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10127
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10127
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 10127
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13256
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13256
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13256
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13256
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7117
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7117
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7117
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 7117
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13246
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13246
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13246
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13246
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13245
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13245
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13245
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13245
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4145
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4145
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4145
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 4145
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1071
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1071
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1071
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 1071
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13251
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13251
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13251
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13251
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13249
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13249
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13249
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13249
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13248
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13248
		division_template = "Africa Defense"
	}

	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13248
		division_template = "Africa Defense"
	}

	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13248
		division_template = "Africa Defense"
	}

	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13310
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13310
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13310
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13310
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13247
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13247
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13247
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13247
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13243
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13243
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13243
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13243
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13254
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13254
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13254
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13254
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13255
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13255
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13255
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13255
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13236
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13236
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13236
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13236
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13253
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13253
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13253
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13253
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13237
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13237
		division_template = "Africa Defense"
	}

	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13237
		division_template = "Africa Defense"
	}

	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13237
		division_template = "Africa Defense"
	}

	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13250
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13250
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13250
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13250
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13252
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13252
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13252
		division_template = "Africa Defense"
	}
	division = {	
		start_equipment_factor = 0.01 start_experience_factor = 0.3
		location = 13252
		division_template = "Africa Defense"
	}




}
