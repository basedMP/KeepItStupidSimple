division_template = {
	is_locked = yes
	name = "Jagdpanzerdivison"
	regiments = {
		mechanized = { x = 0 y = 0 }
		mechanized = { x = 0 y = 1 }
		mechanized = { x = 0 y = 2 }		
		mechanized = { x = 0 y = 3}
		mechanized = { x = 1 y = 3 }
		mechanized = { x = 1 y = 0 }
		mechanized = { x = 1 y = 1 }
		mechanized = { x = 1 y = 2 }
		medium_armor = { x = 3 y = 0 }
		medium_armor = { x = 3 y = 2 }
		medium_armor = { x = 3 y = 1 }
		medium_armor = { x = 3 y = 3 }
		medium_armor = { x = 3 y = 4 }
		medium_armor = { x = 4 y = 0 }
		medium_armor = { x = 4 y = 1 }
		medium_armor = { x = 4 y = 3 }
		medium_sp_anti_air_brigade = { x = 4 y = 2 }
		heavy_tank_destroyer_brigade = { x = 2 y = 0 }
		heavy_tank_destroyer_brigade = { x = 2 y = 1 }
		heavy_tank_destroyer_brigade = { x = 2 y = 2 }

		

	}


	support = {
	logistics_company = { x = 0 y = 0 }
	engineer = { x = 0 y = 1 }
	maintenance_company = { x = 0 y = 2 }
	mot_recon = { x = 0 y =3}
	field_hospital = { x = 0 y = 4}
	}
}

units = {

	division = {	
		start_equipment_factor = 1 start_experience_factor = 3
		location = 6521
		division_template = "Jagdpanzerdivison"
		name = "1. Jagdpanzerdivison"
		force_equipment_variants = { infantry_equipment_3 = { owner = "GER" } }
	}
	division = {	
		start_equipment_factor = 1 start_experience_factor = 3
		location = 6521
		division_template = "Jagdpanzerdivison"
		name = "2. Jagdpanzerdivison"
		force_equipment_variants = { infantry_equipment_3 = { owner = "GER" } }
	}
	

	