﻿##### Division Templates #####
division_template = {
	name = "INF"				# Infantry Brigade

	division_names_group = FRA_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 0 y = 4 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 1 y = 3 }
		infantry = { x = 1 y = 4 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		infantry = { x = 2 y = 3 }
		infantry = { x = 2 y = 4 }
		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
		infantry = { x = 3 y = 2 }
		infantry = { x = 3 y = 3 }
		infantry = { x = 3 y = 4 }			
	}
	support = {
		recon = { x = 0 y = 0 }   # recon bn of tankettes, ACs, mot inf
		engineer = {x = 0 y = 1}
	}
}

units = {

	division= {	
		#name = "1. Gyalogdandár"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 1
		}
		location = 11506
		division_template = "INF"
		start_experience_factor = 0.3
		start_equipment_factor = 1

	}
}
#########################
## STARTING PRODUCTION ##
#########################

instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "FRA"
		}
		requested_factories = 2
		progress = 0.1
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "FRA" 
		}
		requested_factories = 1
		progress = 0.3
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = light_tank_equipment_2
			creator = "FRA" 
		}
		requested_factories = 1
		progress = 0.4
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = motorized_equipment_1
			creator = "FRA" 
		}
		requested_factories = 1
		progress = 0.4
		efficiency = 50
	}

	add_equipment_production = {
		equipment = {
			type = Anti_Air_equipment_1
			creator = "FRA" 
		}
		requested_factories = 1
		progress = 0.4
		efficiency = 50
	}
}
###################
