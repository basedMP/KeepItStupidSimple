division_template = {
	is_locked = yes
	name = "Divisions CuiRassées"
	regiments = {
		motorized= { x = 0 y = 0 }
		motorized= { x = 0 y = 1 }
		motorized= { x = 0 y = 2 }		
		motorized= { x = 0 y = 3}
		motorized= { x = 1 y = 3 }
		motorized= { x = 1 y = 0 }
		motorized= { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }
		light_armor = { x = 3 y = 0 }
		light_armor= { x = 3 y = 2 }
		light_armor= { x = 3 y = 1 }
		light_armor= { x = 3 y = 3 }
		light_armor= { x = 3 y = 4 }
		light_armor= { x = 4 y = 0 }
		light_armor= { x = 4 y = 1 }
		light_armor= { x = 4 y = 3 }
		light_armor = { x = 4 y = 2 }
		light_armor= { x = 2 y = 0 }
		light_armor= { x = 2 y = 1 }
		light_tank_destroyer_brigade = { x = 2 y = 2 }		
	}
	support = {
	engineer = { x = 0 y = 0 }
	anti_air = { x = 0 y = 1 }
	artillery = { x = 0 y = 2 }
	light_tank_recon  = { x = 0 y = 3}
	logistics_company = { x = 0 y = 4}
	field_hospital = { x = 0 y = 5}
	}
}

units = {

	division = {	
		start_equipment_factor = 1 start_experience_factor = 3
		location = 11506
		division_template = "Divisions CuiRassées"
		name = "1re Division Cuirassée"
		force_equipment_variants = { infantry_equipment_1 = { owner = "FRA" } }
	}
	division = {	
		start_equipment_factor = 1 start_experience_factor = 3
		location = 11506
		division_template = "Divisions CuiRassées"
		name = "2re Division Cuirassée"
		force_equipment_variants = { infantry_equipment_1 = { owner = "FRA" } }
	}
	
	

	