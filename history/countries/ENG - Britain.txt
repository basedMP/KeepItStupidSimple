﻿capital = 126

set_oob = "ENG_1936"
if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	set_naval_oob = "ENG_1936_Naval"
	else = {
		set_naval_oob = "ENG_1936_Naval_Legacy"
	}
}

set_variable = {
    var = min_fuel
    value = 0.400
}
anti_cross_faction_trade_axis = yes
anti_cross_faction_trade_geacps = yes
set_country_flag=fuel_system_enabled
set_country_flag=host
add_to_tech_sharing_group = commonwealth_research

load_oob = "ENG_Motorized"

set_research_slots = 7

set_stability = 0.6
set_war_support = 0.1

# Starting tech
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1 
	tech_field_hospital = 1
	tech_logistics_company = 1
	tech_signal_company = 1
	tech_engineers = 1
	tech_recon = 1
	motorised_infantry = 1
	gw_artillery = 1
	interwar_antiair = 1
	gwtank = 1
	basic_light_tank = 1
	improved_light_tank = 1
	#improved_light_tank = 1  # PLACEHOLDER^
	#basic_heavy_tank = 1  # PLACEHOLDER
	#basic_medium_tank = 1 # PLACEHOLDER
	early_fighter = 1
	fighter1 = 1
	naval_bomber1 = 1
	early_bomber = 1
	CAS1 = 1
	tactical_bomber1 = 1
	
	trench_warfare = 1

	tech_mountaineers = 1
	interwar_antitank = 1

	
	fleet_in_being = 1
	electronic_mechanical_engineering = 1
	radio = 1
	radio_detection = 1
	fuel_silos = 1
	fuel_refining = 1
}

if = {
	limit = {
		has_dlc = "Battle for the Bosporus"
	}
	set_technology = { camelry = 1 }
}
if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	set_technology = {
		early_submarine = 1
		early_destroyer = 1
		basic_destroyer = 1
		early_light_cruiser = 1
		early_heavy_cruiser = 1
		early_battleship = 1
		early_battlecruiser = 1
		early_carrier = 1
		transport = 1
	}
}
if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	set_technology = {
		early_ship_hull_light = 1
		basic_ship_hull_light = 1
		early_ship_hull_submarine = 1
		early_ship_hull_cruiser = 1
		basic_ship_hull_cruiser = 1
		early_ship_hull_heavy = 1
		basic_ship_hull_heavy = 1
		early_ship_hull_carrier = 1
		basic_ship_hull_carrier = 1
		basic_battery = 1
		basic_light_battery = 1
		basic_medium_battery = 1
		basic_heavy_battery = 1
		basic_torpedo = 1
		sonar = 1
		smoke_generator = 1
		basic_depth_charges = 1
		basic_secondary_battery = 1
		basic_cruiser_armor_scheme = 1
		basic_heavy_armor_scheme = 1
		mtg_transport = 1
		improved_secondary_battery = 1
	}
}

if = {
	limit = {
		has_dlc = "La Resistance"
	}
	set_technology = {
		armored_car1 = 1
	}
}

set_variable = { var = eng_gateway_to_europe_influence value = 0 }

add_ideas = {
	stiff_upper_lip
	ENG_the_war_to_end_all_wars
	george_v
	export_focus
	peace_time_training
	
}
	

set_convoys = 800

add_equipment_to_stockpile = {
	type = artillery_equipment_1

	amount = 2000

}
add_equipment_to_stockpile = {
	type = anti_air_equipment_1

	amount = 2000

}
add_equipment_to_stockpile = {
	type = motorized_equipment_1

	amount = 3000

}
add_equipment_to_stockpile = {
	type = support_equipment_1

	amount = 4000

}
add_equipment_to_stockpile = {
	type = infantry_equipment_0

	amount = 20000

}
add_equipment_to_stockpile = {
	type = CAS_equipment_1

	amount = 400

}

add_equipment_to_stockpile = {
	type = nav_bomber_equipment_1

	amount = 400

}
add_equipment_to_stockpile = {
	type = tac_bomber_equipment_0

	amount = 100

}
# DIPLOMACY
if = {
	limit = {
		OR = {
			has_dlc = "Together for Victory"
			has_dlc = "Man the Guns"
		}
	}
	set_autonomy = {
		target = CAN
		autonomous_state = autonomy_dominion
		freedom_level = 0.4
	}
	set_autonomy = {
		target = RAJ
		autonomous_state = autonomy_dominion
		freedom_level = 0.4
	}
	set_autonomy = {
		target = AST
		autonomous_state = autonomy_dominion
		freedom_level = 0.4
	}
	set_autonomy = {
			target = UAC
			autonomous_state = autonomy_dominion
			freedom_level = 0.4
	}
}


if = {
	limit = { has_dlc = "Together for Victory" }

	add_to_tech_sharing_group = commonwealth_research
}


set_politics = {
	ruling_party = democratic
	last_election = "1935.11.14"
	election_frequency = 48
	elections_allowed = yes ##suspended through duration of war, which is handled via event
}
set_popularities = {
	democratic = 97
	fascism = 2
	communism = 1
}


create_faction = Allies
add_to_faction = ENG
#add_to_faction = FRA
add_to_faction = CAN
add_to_faction = AST
add_to_faction = RAJ
add_to_faction = USA

#give_guarantee = CZE

# Order matters - here Chamberlain becomes starting leader

create_country_leader = {
	name = "Winston Churchill"
	desc = "POLITICS_WINSTON_CHURCHILL_DESC"
	picture = "Portrait_Britain_Winston_Churchill.dds"
	expire = "1965.1.1"
	ideology = conservatism
	traits = {
		#traits here need to also be added to events britain.9 and britain.10. Don't ask why.
	}
}


create_country_leader = {
	name = "Stanley Baldwin"
	desc = "POLITICS_STANLEY_BALDWIN_DESC"
	picture = "gfx//leaders//ENG//Portrait_Britain_Stanley_Baldwin.dds"
	expire = "1938.1.1"
	ideology = liberalism
	traits = {
		conservative_grandee
	}
}

#create_country_leader = {
#	name = "Neville Chamberlain"
#	desc = "POLITICS_NEVILLE_CHAMBERLAIN_DESC"
#	picture = "Portrait_Britain_Neville_Chamberlain.dds"
#	expire = "1965.1.1"
#	ideology = liberalism
#	traits = {
#		
#	}
#}

create_country_leader = {
	name = "Oswald Mosley"
	desc = "POLITICS_OSWALD_MOSLEY_DESC"
	picture = "Portrait_Britain_Oswald_Mosley.dds"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
		champion_of_peace_1
		economic_reformer
	}
}

create_country_leader = {
	name = "Harry Pollitt"
	desc = "POLITICS_HARRY_POLLITT_DESC"
	picture = "portrait_eng_harry_pollitt.dds"
	expire = "1965.1.1"
	ideology = leninism
	traits = {
		staunch_stalinist
	}
}

#create_country_leader = {
#	name = "Rajani Palme Dutt"
#	desc = "POLITICS_PALME_DUTT_DESC"
#	picture = "Portrait_Britain_Palme_Dutt.dds"
#	expire = "1965.1.1"
#	ideology = leninism
#	traits = {
#		
#	}
#}

create_field_marshal = {
	name = "Alan Brooke" # Aristocrat
	id = 997
	picture = "Portrait_Britain_Alan_Brooke.dds"
	traits = { organizer thorough_planner logistics_wizard defensive_doctrine infantry_leader infantry_expert trait_cautious adaptable }
	skill = 7
	attack_skill = 5
	defense_skill = 6
	planning_skill = 4
	logistics_skill = 7
}

create_field_marshal = {
	name = "Claude Auchinleck"
	picture = "Portrait_Britain_Claude_Auchinleck.dds"
traits = { inflexible_strategist unyielding_defender infantry_leader infantry_expert trait_engineer scavenger fortress_buster commando camouflage_expert paratrooper }
	skill = 7
	attack_skill = 4
	defense_skill = 6
	planning_skill = 5
	logistics_skill = 7
}

create_corps_commander = {
	name = "Neil Ritchie"
	picture = "Portrait_Britain_Neil_Ritchie.dds"
traits = { inflexible_strategist unyielding_defender infantry_leader ambusher skilled_staffer   }
	skill = 7
	attack_skill = 3
	defense_skill = 4
	planning_skill = 5
	logistics_skill = 8
}

create_corps_commander = {
	name = "Alan Cunningham"
	picture = "Portrait_Britain_Alan_Cunningham.dds"
traits = { organizer fast_planner organisational_leader offensive_doctrine infantry_leader trickster hill_fighter trait_mountaineer logistics_wizard infantry_expert expert_improviser }
	skill = 7
	attack_skill = 5
	defense_skill = 4 
	planning_skill = 5
	logistics_skill = 8
}

create_corps_commander = {
	name = "William Slim"
	picture = "Portrait_Britain_William_Slim.dds"
	traits = { jungle_rat trickster infantry_expert expert_improviser infantry_leader }
	skill = 7
	attack_skill = 5
	defense_skill = 4
	planning_skill = 5
	logistics_skill = 8
}

create_corps_commander = {
	name = "Richard O'Connor"
	id = 999
	picture = "Portrait_Britain_Richard_OConnor.dds"
traits = { desert_fox infantry_leader ambusher commando skilled_staffer }
	skill = 7
	attack_skill = 2
	defense_skill = 4
	planning_skill = 7
	logistics_skill = 7
}

create_corps_commander = {
	name = "Miles Dempsey"
	picture = "Portrait_Britain_Miles_Dempsey.dds"
	traits = { infantry_leader old_guard infantry_expert trait_mountaineer  naval_invader invader_ii trickster expert_improviser guerilla_fighter skilled_staffer }
	skill = 7
	attack_skill = 6
	defense_skill = 5
	planning_skill = 5
	logistics_skill = 6
}

create_corps_commander = {
	name = "Arthur Percival"
		picture = "Portrait_Britain_Arthur_Percival.dds"
	traits = { trickster  organizer organisational_leader inspirational_leader logistics_wizard  bearer_of_artillery panzer_leader panzer_expert    }
	skill = 7
    attack_skill = 7
    defense_skill = 5
    planning_skill = 4
    logistics_skill = 6
}

create_corps_commander = {
	name = "Merton Beckwith-Smith"
		picture = "Portrait_Britain_Merton_Beckwith_Smith.dds"
	traits = { infantry_leader urban_assault_specialist commando paratrooper ambusher  trickster  }
	skill = 7
	attack_skill = 5
	defense_skill = 4
	planning_skill = 6
	logistics_skill = 7
}

create_corps_commander = {
	name = "Henry Pownall"
	picture = "Portrait_Britain_Henry_Pownall.dds"
traits = { infantry_leader infantry_expert ambusher trickster expert_improviser skilled_staffer } 
    skill = 7 
	attack_skill = 7
    defense_skill = 5
    planning_skill = 5
    logistics_skill = 5
}

create_corps_commander = {
	name = "William Gott"
		picture = "Portrait_Britain_William_Gott.dds"
	traits = { panzer_leader panzer_expert desert_fox inflexible_strategist unyielding_defender organizer thorough_planner logistics_wizard }
	skill = 7
	attack_skill = 4
	defense_skill = 5
	planning_skill = 4
	logistics_skill = 5
}

create_corps_commander = {
	 name = "Jackie Smyth"
    picture = "Portrait_Britain_John_Jackie_Smyth.dds"
traits = { brilliant_strategist infantry_leader aggressive_assaulter infantry_expert trait_mountaineer hill_fighter }
	skill = 7
	attack_skill = 5
	defense_skill = 5
	planning_skill = 5
	logistics_skill = 7
}

create_corps_commander = {
	name = "Archibald Wavell" # Aristocrat
	id = 998
	picture = "Portrait_Britain_Archibald_Wavell.dds"
	traits = { infantry_leader panzer_leader panzer_expert infantry_expert fortress_buster	desert_fox commando trait_engineer trait_engineer fortress_buster skirmisher }
	skill = 7
	attack_skill = 2
	defense_skill = 6
	planning_skill = 5
	logistics_skill = 7
}


create_navy_leader = {
	name = "James Fownes Somerville" # Aristocrat
	id = 983
		picture = "Portrait_Britain_James_Fownes_Somerville.dds"
	traits = { fly_swatter cruiser_captain fleet_protector destroyer_leader hunter_killer search_pattern_expert  arctic_water_expert    }
	skill = 4
	attack_skill = 2
	defense_skill = 2
	maneuvering_skill = 2
	coordination_skill = 9
}

create_navy_leader = {
	name = "Henry Harwood" # Aristocrat
	id = 984
		picture = "Portrait_Britain_Henry_Harwood.dds"

	traits = { fly_swatter cruiser_captain fleet_protector destroyer_leader hunter_killer search_pattern_expert  arctic_water_expert    }
	skill = 4
	attack_skill = 2
	defense_skill = 2
	maneuvering_skill = 2
	coordination_skill = 9
}

create_navy_leader = {
	name = "Andrew Cunningham"
	id = 985
		picture = "Portrait_Britain_Andrew_Cunningham.dds"
	traits = {  superior_tactician concealment_expert air_controller flight_deck_manager torpedo_bomber dive_bomber ground_pounder  blue_water_expert ironside big_guns_expert }
	skill = 5
	attack_skill = 2
	defense_skill = 2
	maneuvering_skill = 1
	coordination_skill = 3
}

create_navy_leader = {
	name = "Bruce Fraser"
	id = 986
		picture = "Portrait_Britain_Bruce_Fraser.dds"
	traits = { spotter blockade_runner smoke_screen_expert concealment_expert }
	skill = 3
	attack_skill = 2
	defense_skill = 2
	maneuvering_skill = 3
	coordination_skill = 6
}


### Ship Variants ###

if = {
	limit = { not = { has_dlc = "Man the Guns" } }
	### Variants ###
	# Submarines #
	create_equipment_variant = {
		name = "O/P/R Class"
		type = submarine_1
		parent_version = 0
		upgrades = {
			ship_reliability_upgrade = 3
			sub_engine_upgrade = 2
			sub_stealth_upgrade = 2
			sub_torpedo_upgrade = 3
		}
	}
	create_equipment_variant = {
		name = "S Class"
		type = submarine_1
		parent_version = 1
		upgrades = {
			ship_reliability_upgrade = 2
			sub_engine_upgrade = 0
			sub_stealth_upgrade = 2
			sub_torpedo_upgrade = 3
		}
	}
	# Destroyers #
	create_equipment_variant = {
		name = "A/B/C/D Class"
		type = destroyer_1
		parent_version = 0
		upgrades = {
			ship_torpedo_upgrade = 1
			destroyer_engine_upgrade = 3
			ship_ASW_upgrade = 2
			ship_anti_air_upgrade = 1
		}
		obsolete = yes
	}
	# Light Cruisers #
	create_equipment_variant = {
		name = "Emerald Class"
		type = light_cruiser_1
		parent_version = 0
		upgrades = {
			ship_reliability_upgrade = 3
			ship_engine_upgrade = 3
			ship_gun_upgrade = 2
			ship_anti_air_upgrade = 3
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Amphion Class"
		type = light_cruiser_1
		parent_version = 1
		upgrades = {
			ship_reliability_upgrade = 3
			ship_engine_upgrade = 3
			ship_gun_upgrade = 3
			ship_anti_air_upgrade = 3
		}
	}
	# Heavy Cruisers #
	create_equipment_variant = {
		name = "County Class"
		type = heavy_cruiser_1
		parent_version = 0
		upgrades = {
			ship_reliability_upgrade = 2
			ship_engine_upgrade = 3
			ship_armor_upgrade = 3
			ship_gun_upgrade = 3
		}
	}


	# Battlecruisers #
	create_equipment_variant = {
		name = "Admiral Class"
		type = battle_cruiser_1
		parent_version = 0
		upgrades = {
			ship_reliability_upgrade = 3
			ship_engine_upgrade = 2
			ship_armor_upgrade = 3
			ship_gun_upgrade = 2
		}
	}
	# Battleships #
	create_equipment_variant = {
		name = "Nelson Class"
		type = battleship_1
		parent_version = 0
		upgrades = {
			ship_reliability_upgrade = 3
			ship_engine_upgrade = 2
			ship_armor_upgrade = 3
			ship_gun_upgrade = 3
		}
	}
	create_equipment_variant = {
		name = "Queen Elizabeth Class"
		type = battleship_1
		parent_version = 0
		upgrades = {
			ship_reliability_upgrade = 1
			ship_engine_upgrade = 2
			ship_armor_upgrade = 1
		}
	}
}
if = {
	limit = { has_dlc = "Man the Guns" }
	
	create_equipment_variant = {
			name = "British Pride Class"
			type = ship_hull_heavy_2
			name_group = ENG_BB_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_heavy_battery_2
				fixed_ship_anti_air_slot = ship_anti_air_2
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = heavy_ship_engine_2
				fixed_ship_secondaries_slot = ship_secondaries_2
				fixed_ship_armor_slot = ship_armor_bb_2
				front_1_custom_slot = ship_heavy_battery_2
				mid_1_custom_slot = ship_heavy_battery_2
				mid_2_custom_slot = ship_heavy_battery_2
				rear_1_custom_slot = ship_heavy_battery_2
			}
		}
}