﻿capital = 328

#oob = "MAN_1936"

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1		
	tech_recon = 1
	early_fighter = 1
	gw_artillery = 1
	gwtank = 1
	marines = 1
	tech_mountaineers = 1
	fuel_silos = 1
	motorised_infantry = 1
	basic_light_tank = 1
	improved_light_tank = 1
	tech_engineers = 1
	interwar_antiair = 1
	interwar_antitank = 1
	mtg_transport = 1
	early_ship_hull_cruiser = 1
	basic_ship_hull_cruiser = 1
	early_ship_hull_light = 1
	basic_ship_hull_light = 1
	early_ship_hull_heavy = 1
	early_ship_hull_submarine = 1
	basic_ship_hull_submarine = 1
	basic_torpedo = 1
	basic_battery = 1
	basic_medium_battery = 1
	basic_light_battery = 1
	basic_heavy_battery = 1
	basic_secondary_battery = 1
	improved_secondary_battery = 1
	improved_airplane_launcher = 1
}


add_ideas = MAN_banditry
add_ideas = MAN_kwantung_veto
add_ideas = MAN_low_legitimacy_5
add_ideas =	limited_exports


set_convoys = 150
set_research_slots = 4

JAP = {
	if = {
	limit = {
		OR = {
			has_dlc = "Together for Victory"
			has_dlc = "Man the Guns"
		}
	}
	set_autonomy = {
		target = MAN
		autonomous_state = autonomy_wtt_imperial_protectorate
		freedom_level = 0.4
	}
	
}
}
anti_cross_faction_trade_axis = yes
anti_cross_faction_trade_allies = yes

set_politics = {
	ruling_party = fascism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	fascism = 80
	neutrality = 20
}

create_country_leader = {
	name = "Aisin Gioro Puyi"
	
	picture = "gfx/leaders/MAN/Portrait_Manchuria_Henry_Pu_Yi.dds"
	expire = "1960.1.1"
	ideology = rexism
	traits = {

	}
}

create_country_leader = {
	name = "Aisin Gioro Puyi"
	
	picture = "gfx/leaders/MAN/Portrait_Manchuria_Henry_Pu_Yi.dds"
	expire = "1960.1.1"
	ideology = despotism
	traits = {

	}
}

create_field_marshal = {
	name = "Harukichi Hyakutake"
		picture = "Portrait_Japan_Harukichi_Hyakutake.dds"
	  traits = { defensive_doctrine old_guard ambusher inflexible_strategist organizer infantry_leader infantry_expert unyielding_defender logistics_wizard  }
    skill = 6
    attack_skill = 4
    defense_skill = 8
    planning_skill = 6
    logistics_skill = 6
}
#INF GEN

create_corps_commander = {
	name = "Tomoyuki Yamashita"
		picture = "Portrait_Japan_Tomoyuki_Yamashita.dds"
	traits = { skilled_staffer jungle_rat ambusher commando ranger infantry_expert infantry_leader trait_mountaineer expert_improviser trait_engineer urban_assault_specialist ranger hill_fighter trait_mountaineer trickster }
	skill = 6
	attack_skill = 8
	defense_skill = 6
	planning_skill = 9
	logistics_skill = 8
}
create_corps_commander = {
	name = "Toshizo Nishio" #Toshizo
		picture = "Portrait_Japan_Toshizo_Nishio.dds"
		traits = { skilled_staffer jungle_rat ambusher commando ranger infantry_expert infantry_leader expert_improviser trait_mountaineer trait_engineer urban_assault_specialist ranger hill_fighter trait_mountaineer trickster }
	skill = 6
	attack_skill = 8
	defense_skill = 6
	planning_skill = 9
	logistics_skill = 8
}
#INF FIELDMARSHAL
create_field_marshal = {
	name = "Hisaichi Terauchi"
	picture = "Portrait_Japan_Hisaichi_Terauchi.dds"
	traits = { trait_mountaineer skilled_staffer hill_fighter infantry_leader infantry_expert brilliant_strategist organizer logistics_wizard fast_planner aggressive_assaulter naval_invader invader_ii }
	skill = 6
	attack_skill = 3
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 1
}
#TANK GEN
create_corps_commander = {
	name = "Kenkichi Ueda"
	picture = "Portrait_Japan_Kenkichi_Ueda.dds"
	traits = {  trait_engineer jungle_rat panzer_leader panzer_expert desert_fox ranger trickster trait_engineer urban_assault_specialist ranger hill_fighter trait_mountaineer }
	skill = 8
	attack_skill = 7
	defense_skill = 2
	planning_skill = 6
	logistics_skill = 8
}
create_field_marshal = {
	name = "Shunroku Hata"
	picture = "Portrait_Japan_Shunroku_Hata.dds"
	traits = { logistics_wizard brilliant_strategist trait_engineer organizer panzer_leader fast_planner expert_improviser desert_fox ranger trickster aggressive_assaulter   }
	skill = 8
	attack_skill = 5
	defense_skill = 1
	planning_skill = 5
	logistics_skill = 5
}

create_corps_commander = {
	name = "Shizuichi Tanaka"
		picture = "Portrait_Japan_Shizuichi_Tanaka.dds"
	traits = { ambusher infantry_leader skilled_staffer trickster guerilla_fighter}
	skill = 4
    attack_skill = 3
    defense_skill = 4
    planning_skill = 3
    logistics_skill = 3
}

create_corps_commander = {
	name = "Hitoshi Imamura"
		picture = "Portrait_Japan_Hitoshi_Imamura.dds"
	traits = { infantry_leader hill_fighter infantry_expert trait_mountaineer  naval_invader invader_ii trickster expert_improviser guerilla_fighter skilled_staffer }
	skill = 6
	attack_skill = 6
	defense_skill = 5
	planning_skill = 5
	logistics_skill = 6
}

create_navy_leader = {
	name = "Isoroku Yamamoto"
		picture = "Portrait_Japan_Isoroku_Yamamoto.dds"
	traits = { fleet_protector bold air_controller flight_deck_manager superior_tactician spotter torpedo_expert lancer loading_drill_master concealment_expert flight_deck_manager fighter_director dive_bomber torpedo_bomber blue_water_expert inshore_fighter }
	skill = 7
	attack_skill = 4
	defense_skill = 3
	maneuvering_skill = 9
	coordination_skill = 5
}

create_navy_leader = {
	name = "Mineichi Koga"
		picture = "Portrait_Japan_Mineichi_Koga.dds"
	traits = { bold battleship_adherent superior_tactician spotter ironside fly_swatter big_guns_expert safety_first marksman crisis_magician blue_water_expert inshore_fighter }
	skill = 5
	attack_skill = 3
	defense_skill = 3
	maneuvering_skill = 5
	coordination_skill = 5
}