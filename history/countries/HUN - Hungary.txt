﻿capital = 43

oob = "HUN_1936"

set_research_slots = 4
set_stability = 0.7
set_war_support = 0.6
set_convoys = 150
set_variable = {
    var = min_fuel
    value = 0.800
}

add_equipment_to_stockpile = {
	type = infantry_equipment_0

	amount = 2000

}
add_to_tech_sharing_group = axis_research

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1		
	tech_recon = 1
	gw_artillery = 1
	gwtank = 1
	early_fighter = 1
	CAS1 = 1
	fuel_silos = 1
	motorised_infantry = 1
	basic_light_tank = 1
	improved_light_tank = 1
	tech_engineers = 1
	interwar_antiair = 1
	interwar_antitank = 1
}
GER = { add_to_faction = HUN }

	add_ideas = {
		disarmed_nation
		HUN_treaty_of_triannon
		export_focus
		peace_time_training
		
	}


anti_cross_faction_trade_allies = yes
anti_cross_faction_trade_geacps = yes
set_politics = {
	ruling_party = neutrality
	last_election = "1935.3.31"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	democratic = 25
	fascism = 31
	neutrality = 44
}

#Start Horty as neutral if DOD
if = {
	limit = {
		has_dlc = "Death or Dishonor"
	}
	create_country_leader = {
		name = "Miklós Horthy"
		desc = "POLITICS_MIKLOS_HORTHY_DESC"
		picture = "Portrait_Hungary_Miklos_Horthy.dds"
		expire = "1965.1.1"
		ideology = oligarchism
		traits = {
			fascist_sympathies
			anti_communist
		}
		id = 500 #Horthy Neutral
	}
	else = {
		create_country_leader = {
			name = "Miklós Horthy"
			desc = "POLITICS_MIKLOS_HORTHY_DESC"
			picture = "Portrait_Hungary_Miklos_Horthy.dds"
			expire = "1965.1.1"
			ideology = despotism
		}
		create_country_leader = {
			name = "Miklós Horthy"
			desc = "POLITICS_MIKLOS_HORTHY_DESC"
			picture = "Portrait_Hungary_Miklos_Horthy.dds"
			expire = "1965.1.1"
			ideology = fascism_ideology
		}
	}
}

create_country_leader = {
	name = "Mátyás Rákosi"
	desc = "POLITICS_MATYAS_RAKOSKI_DESC"
	picture = "Portrait_Hungary_Matyas_Rakoski.dds"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Árpád Szakasits"
	desc = "POLITICS_ARPAD_SZAKASITS_DESC"
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
	expire = "1965.1.1"
	ideology = socialism
	traits = {
		#
	}
}


create_corps_commander = {
	name = "Géza Lakatos"
	gfx = GFX_Portrait_hungary_geza_lakatos
	traits = { skilled_staffer infantry_expert infantry_leader trickster guerilla_fighter unyielding_defender ranger trait_mountaineer trait_engineer hill_fighter }
	skill = 6
	attack_skill = 4
	defense_skill = 4
	planning_skill = 6
	logistics_skill = 2
}

create_corps_commander = {
	id = 334
	name = "Iván Hindy"
	gfx = GFX_Portrait_hungary_ivan_hindy
	traits = {  
	trickster skilled_staffer panzer_leader panzer_expert ranger trait_engineer urban_assault_specialist fortress_buster expert_improviser swamp_fox }
	skill = 7
	attack_skill = 7
	defense_skill = 4
	planning_skill = 6
	logistics_skill = 5
}

create_field_marshal = {
	name = "Károly Beregfy"
	gfx = GFX_Portrait_hungary_karoly_beregfy
	traits = { ranger trait_engineer infantry_expert aggressive_assaulter  trickster guerilla_fighter defensive_doctrine organizer logistics_wizard urban_assault_specialist }
	skill = 6
	attack_skill = 4
	defense_skill = 3
	planning_skill = 4
	logistics_skill = 3
}

create_corps_commander = {
	name = "Lajos Veress"
	gfx = GFX_Portrait_hungary_lajos_veress
	traits = { skilled_staffer infantry_leader infantry_expert unyielding_defender ambusher }
	skill = 4
	attack_skill = 1
	defense_skill = 9
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Gusztáv Jány"
	
	traits = { skilled_staffer trickster panzer_leader desert_fox hill_fighter panzer_expert trait_engineer urban_assault_specialist fortress_buster expert_improviser }
	skill = 7
	attack_skill = 6
	defense_skill = 7
	planning_skill = 4
	logistics_skill = 6
}

create_field_marshal = {
	id = 333
	name = "Ferenc Feketehalmy-Czeydner"
	gfx = GFX_Portrait_hungary_ferenc_feketehalmy_czeydner
	traits = {  aggressive_assaulter thorough_planner trickster panzer_leader panzer_expert ranger trait_engineer urban_assault_specialist fortress_buster offensive_doctrine logistics_wizard organizer expert_improviser }
	skill = 7
	attack_skill = 6
	defense_skill = 3
	planning_skill = 4
	logistics_skill = 6
}