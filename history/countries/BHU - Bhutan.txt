﻿capital = 324

# Starting tech
set_technology = {
	infantry_weapons = 1
	tech_mountaineers = 1
}

country_event = { id =  bhutan.4 days = 3 } #REMOVES STARTING EFFICIENCY
country_event = { id =  lar_spain.3 days = 199 } #PARTIAL MOB FOR ENG
country_event = { id =  bhutan.3 days = 1308 } #SPAWN INS
country_event = { id =  bhutan.1 days = 1328 } #SOVIET BALTICS
country_event = { id =  bhutan.2 days = 1337 } #WAR
country_event = { id =  bhutan.5 days = 1766 } #HP SPIRIT GONE
country_event = { id = DOD_yugoslavia.21 days = 759  } #YUG AIR


set_war_support = 0.1
set_stability = 0.8


set_politics = { 
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	neutrality = 100
}

add_ideas = {
	ai_limiter
}
set_research_slots = 0

create_country_leader = {
	name = "Jigme Wangchuck"
	desc = "POLITICS_JIGME_WANGCHUCK_DESC"
	picture = "GFX_portrait_buthan_jigme_wangchuk"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}