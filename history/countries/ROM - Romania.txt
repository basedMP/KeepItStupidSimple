﻿capital = 46

oob = "ROM_1936"
if = {
	limit = { has_dlc = "Man the Guns" }
		set_naval_oob = "ROM_1936_naval_mtg"
	else = {
		set_naval_oob = "ROM_1936_naval_legacy"
	}
}
set_variable = {
    var = min_fuel
    value = 0.800
}
add_to_tech_sharing_group = axis_research
anti_cross_faction_trade_allies = yes
anti_cross_faction_trade_geacps = yes
set_research_slots = 3
set_stability = 0.6
set_war_support = 0.6

# Starting tech
set_technology = {
	tech_support = 1		
	tech_engineers = 1
	tech_mountaineers = 1
	motorised_infantry = 1
	gwtank = 1
	basic_light_tank = 1
	improved_light_tank = 1
	infantry_weapons = 1
	infantry_weapons1 = 1
	gw_artillery = 1
	interwar_antiair = 1
	early_fighter = 1
	fuel_silos = 1
	interwar_antitank = 1
	marines = 1
	mtg_transport = 1


}

add_equipment_to_stockpile = {
	type = infantry_equipment_0

	amount = 50000

}
add_equipment_to_stockpile = {
	type = support_equipment_1

	amount = 150

}

GER = { add_to_faction = ROM }
if = {
	limit = { not = { has_dlc = "Man the Guns" } }
	set_technology = {
		early_submarine = 1
		early_destroyer = 1
	}
}
if = {
	limit = { has_dlc = "Man the Guns" }
	set_technology = {
		early_ship_hull_light = 1
		early_ship_hull_submarine = 1
		basic_ship_hull_submarine = 1
		basic_battery = 1
		basic_torpedo = 1
		basic_depth_charges = 1
	}
}

#Ideas

	add_ideas = {
		ROM_king_carol_ii_hedonist
		export_focus
		peace_time_training
	}




#Kick off kings crazy life




set_convoys = 10


set_politics = {
	ruling_party = democratic
	last_election = "1933.12.20"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	democratic = 60
	fascism = 18
	communism = 2
	neutrality = 20
}


create_country_leader = {
	name = "Gheorghe Tatarescu"
	desc = "POLITICS_GHEORGHE_TATARESCU_DESC"
	picture = "gfx/leaders/ROM/Portrait_Romania_Gheorghe_Tatarescu.dds"
	expire = "1965.1.1"
	ideology = conservatism #switched from centrism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Constantin Ion Parhon"
	desc = "POLITICS_CONSTANTIN_PARHON_DESC"
	picture = "Portrait_Romania_Constantin_Parhon.dds"
	expire = "1965.1.1"
	ideology = leninism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Armand Calinescu"
	desc = "POLITICS_ARMAND_CALINESCU_DESC"
	picture = "gfx/leaders/ROM/Portrait_Romania_Armand_Calinescu.dds"
	expire = "1965.1.1"
	ideology = centrism #switched from conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Octavian Goga"
	desc = "POLITICS_OCTAVIAN_GOGA_DESC"
	picture = "Portrait_Romania_Octavian_Goga.dds"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
		#
	}
} 

create_field_marshal = {
	name = "Petre Dumitrescu"
	gfx = GFX_Portrait_romania_petre_dumitrescu
	traits = { skilled_staffer organizer defensive_doctrine infantry_leader trait_engineer urban_assault_specialist hill_fighter trickster ambusher guerilla_fighter logistics_wizard scavenger }
	skill = 6
	
	attack_skill = 6
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 4
}

create_field_marshal = {
	name = "Ion Antonescu"
	picture = "Portrait_Romania_Ion_Antonescu.dds"
	traits = { skilled_staffer offensive_doctrine organizer panzer_leader hill_fighter trait_engineer trait_mountaineer urban_assault_specialist infantry_leader infantry_expert panzer_expert logistics_wizard  trickster fast_planner }
	skill = 6
	id = 555
	attack_skill = 4
	defense_skill = 4
	planning_skill = 4
	logistics_skill = 5
}

create_corps_commander = {
	name = "Ioan Mihail Racovita"
	gfx = GFX_Portrait_romania_ioan_mihail_racovita
traits = { skilled_staffer panzer_leader trickster trait_engineer hill_fighter panzer_expert fortress_buster desert_fox trait_mountaineer ranger }
	skill = 6
	attack_skill = 7
	defense_skill = 4
	planning_skill = 6
	logistics_skill = 5
}

create_corps_commander = {
	name = "Gheorghe Avramescu"
	gfx = GFX_Portrait_romania_gheorghe_avramescu
traits = { skilled_staffer hill_fighter trait_mountaineer infantry_leader trait_engineer urban_assault_specialist trickster infantry_expert fortress_buster naval_invader invader_ii }
	skill = 6
	attack_skill = 6
	defense_skill = 4
	planning_skill = 4
	logistics_skill = 6
}

create_corps_commander = {
	name = "Constantin Sanatescu"
	gfx = GFX_Portrait_romania_constantin_sanatescu 
	traits = { skilled_staffer infantry_leader ranger infantry_expert  trickster fortress_buster }
	skill = 6
	attack_skill = 4
	defense_skill = 4
	planning_skill = 5
	logistics_skill = 6
}



### VARIANTS ###
# 1936 Start #
if = {
	limit = { not = { has_dlc = "Man the Guns" } }
	### Ship Variants ###
	create_equipment_variant = {
		name = "Regele Ferdinand Class"
		type = destroyer_1
		upgrades = {
			ship_torpedo_upgrade = 1
			destroyer_engine_upgrade = 1
			ship_ASW_upgrade = 1
			ship_anti_air_upgrade = 1
		}
	}
}
if = {
	limit = { has_dlc = "Man the Guns" }
	# Submarines #
	create_equipment_variant = {
		name = "Delfinul Class"				
		type = ship_hull_submarine_2
		name_group = ROM_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = empty
		}
	}
	# Destroyers #
	create_equipment_variant = {
		name = "Marasti Class"				
		type = ship_hull_light_1
		name_group = ROM_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = empty
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Regele Ferdinand Class"	
		type = ship_hull_light_1
		name_group = ROM_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = empty
			rear_1_custom_slot = ship_depth_charge_1
		}
	}
}