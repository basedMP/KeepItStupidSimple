﻿capital = 2

oob = "ITA_1936"

set_research_slots = 7
set_stability = 0.6
set_war_support = 0.7
set_variable = {
    var = min_fuel
    value = 0.800
}
add_to_tech_sharing_group = axis_research
add_ideas = {
	victor_emmanuel
	ITA_italian_expansionism
	limited_exports
	limited_conscription
	partial_economic_mobilisation
	peace_time_training
	
}

anti_cross_faction_trade_allies = yes
anti_cross_faction_trade_geacps = yes
# Starting tech
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1		
	tech_engineers = 1
	tech_mountaineers = 1
	tech_recon = 1
	tech_field_hospital = 1
	tech_logistics_company = 1
	tech_signal_company = 1
	motorised_infantry = 1
	gw_artillery = 1
	interwar_antitank = 1
	interwar_antiair = 1
	gwtank = 1
	basic_light_tank = 1
	basic_heavy_tank = 1
	improved_light_tank = 1
	early_fighter = 1
	early_bomber = 1
	naval_bomber1 = 1
	CAS1 = 1
	fleet_in_being = 1
	fuel_silos = 1
}

add_equipment_to_stockpile = {
	type = artillery_equipment_1

	amount = 1500

}
add_equipment_to_stockpile = {
	type = infantry_equipment_0

	amount = 60000

}
add_equipment_to_stockpile = {
	type = support_equipment_1

	amount = 2500

}

add_equipment_to_stockpile = {
	type = CAS_equipment_1

	amount = 300

}

GER = { add_to_faction = ITA }
if = {
	limit = {
		has_dlc = "Battle for the Bosporus"
	}
	set_technology = { camelry = 1 }
}
if = {
	limit = { not = { has_dlc = "Man the Guns" } }
		set_technology = {
		early_submarine = 1
		early_destroyer = 1
		early_light_cruiser = 1
		early_heavy_cruiser = 1
		early_battleship = 1
		basic_battleship = 1
		early_battlecruiser = 1
		transport = 1
	}
	set_naval_oob = "ITA_1936_naval_legacy"
}
if = {
	limit = { has_dlc = "Man the Guns" }
	set_technology = {
		basic_depth_charges = 1
		basic_torpedo = 1
		basic_battery = 1
		basic_secondary_battery = 1
		basic_cruiser_armor_scheme = 1
		basic_heavy_battery = 1
		basic_medium_battery = 1
		basic_light_battery = 1
		improved_secondary_battery = 1
		basic_heavy_armor_scheme = 1
		early_ship_hull_submarine = 1
		basic_ship_hull_submarine = 1
		early_ship_hull_light = 1
		basic_ship_hull_light = 1
		early_ship_hull_cruiser = 1
		basic_ship_hull_cruiser = 1
		early_ship_hull_heavy = 1
		basic_ship_hull_heavy = 1
		mtg_transport = 1
		coastal_defense_ships = 1
		improved_airplane_launcher = 1
		early_ship_hull_carrier = 1
		basic_ship_hull_carrier = 1
	}
	set_naval_oob = "ITA_1936_naval_mtg"
}
if = {
	limit = {
		has_dlc = "Man the Guns"
	}
}


set_convoys = 200

set_politics = {
	ruling_party = fascism
	last_election = "1934.3.26"
	election_frequency = 60
	elections_allowed = no
}
set_popularities = {
	democratic = 22
	fascism = 76
	communism = 2
}

create_country_leader = {
	name = "Benito Mussolini"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "Portrait_Italy_Benito_Mussolini.dds"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
		
	}
}

create_country_leader = {
	name = "Ferruccio Parri"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "GFX_portrait_italy_ferruccio_parri"
	expire = "1965.1.1"
	ideology = socialism
	traits = {
		
	}
}

create_country_leader = {
	name = "Victor Emmanuel III"
	desc = "POLITICS_VICTOR_EMMANUEL_III_DESC"
	picture = "GFX_portrait_italy_victor_emmanuel_iii"
	expire = "1965.1.1"
	ideology = despotism
	traits = {

	}
}

create_country_leader = {
	name = "Palmiro Togliatti"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "GFX_portrait_ita_palmiro_togliatti"
	expire = "1965.1.1"
	ideology = marxism
	traits = {
		
	}
}

create_field_marshal = {
	name = "Pietro Badoglio"
	picture = "Portrait_Italy_Pietro_Badoglio.dds"
 	traits = { organizer thorough_planner logistics_wizard defensive_doctrine infantry_leader infantry_expert desert_fox trait_mountaineer hill_fighter brilliant_strategist }
	skill = 7
	attack_skill = 7
	defense_skill = 4
	planning_skill = 4
	logistics_skill = 7
}

create_field_marshal = {
	name = "Emilio De Bono"
	picture = "portrait_italy_emilio_de_bono.dds"
   traits = { defensive_doctrine old_guard ambusher inflexible_strategist organizer infantry_leader infantry_expert unyielding_defender logistics_wizard  }
    skill = 8
    attack_skill = 4
    defense_skill = 8
    planning_skill = 6
    logistics_skill = 6
}

create_field_marshal = {
	name = "Francesco Rossi"
	picture = "Portrait_Italy_Generic_land_5.dds"
   traits = { ambusher defensive_doctrine old_guard inflexible_strategist organizer infantry_leader logistics_wizard  }
    skill = 7
    attack_skill = 3
    defense_skill = 1
    planning_skill = 4
    logistics_skill = 6
}

create_corps_commander = {
	name = "Ugo Cavallero"
	picture = "Portrait_Italy_Ugo_Cavallero.dds"
	traits = {  old_guard trait_cautious skilled_staffer infantry_leader commando ambusher  }
	skill = 6
    attack_skill = 4
    defense_skill = 3
    planning_skill = 7
    logistics_skill = 8
}

create_corps_commander = {
	name = "Giovanni Messe"
	picture = "Portrait_Italy_Giovanni_Messe.dds"
	traits = { ranger trait_mountaineer trickster panzer_leader panzer_expert fortress_buster expert_improviser offensive_doctrine desert_fox hill_fighter trait_engineer }
    skill = 8
    attack_skill = 7
    defense_skill = 4
    planning_skill = 6
    logistics_skill = 7
}

create_corps_commander = {
	name = "Kenobel Popel"
	picture = "Portrait_Italy_Sebastiano_Visconti_Prasca.dds"
    traits = {  old_guard expert_officer inflexible_strategist infantry_leader ambusher scavenger guerilla_fighter skirmisher combined_arms_expert trait_engineer }
    skill = 6
    attack_skill = 4
    defense_skill = 8
    planning_skill = 7
    logistics_skill = 4
}

create_corps_commander = {
	name = "Ubaldo Soddu"
	picture = "Portrait_Italy_Ubaldo_Soddu.dds"
	traits = { defensive_doctrine organizer logistics_wizard old_guard infantry_leader ambusher inspirational_leader organisational_leader guerilla_fighter }
	skill = 6
	attack_skill = 4
	defense_skill = 7
	planning_skill = 2
	logistics_skill = 8
}

create_corps_commander = {
	name = "Italo Balbo"
	picture = "portrait_italy_italo_balbo.dds"
    traits = { infantry_leader infantry_expert scavenger offensive_doctrine naval_liason trait_mountaineer trait_engineer hill_fighter desert_fox expert_officer }
    skill = 6
    attack_skill = 5
    defense_skill = 1
    planning_skill = 6
    logistics_skill = 6
}

create_field_marshal = {
	name = "Rodolfo Graziani"
	picture = "portrait_italy_radolfo_graziani.dds"
  traits = { ranger urban_assault_specialist organizer panzer_leader panzer_expert thorough_planner logistics_wizard trait_engineer desert_fox trait_mountaineer hill_fighter offensive_doctrine trait_cautious }
    skill = 8
    attack_skill = 5
    defense_skill = 4
    planning_skill = 7
    logistics_skill = 6
}

create_navy_leader = {
	name = "Inigo Campioni"
		picture = "Portrait_Italy_Inigo_Campioni.dds"
	traits = { seawolf blockade_runner spotter green_water_expert concealment_expert   }
	skill = 4
	attack_skill = 4
	defense_skill = 1
	maneuvering_skill = 4
	coordination_skill = 4
}

create_navy_leader = {
	name = "Anton Inter"
		picture = "Portrait_Italy_Alberto_Da_Zara.dds"
	traits={ torpedo_bomber dive_bomber battleship_adherent gentlemanly flight_deck_manager ground_pounder superior_tactician ironside air_controller big_guns_expert green_water_expert concealment_expert }
	skill = 4
	attack_skill = 2
	defense_skill = 2
	maneuvering_skill = 1
	coordination_skill = 3
}

if = {
	limit = {
		has_dlc = "La Resistance"
	}

	create_operative_leader = {
		name = "Maurizio Giglio"
		GFX = GFX_portrait_maurizio_giglio
		traits = { }
		bypass_recruitment = no
		available_to_spy_master = yes
		nationalities = { ITA }
	}
}

### VARIANTS ###
# 1936 Start #
#plane variants
create_equipment_variant = {
		name = "CR.42"
		type = fighter_equipment_0
		
}
if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	### Ship Variants ###
	create_equipment_variant = {
		name = "Bandiera Class"
		type = submarine_1
		upgrades = {
			ship_reliability_upgrade = 1
			sub_engine_upgrade = 1
			sub_stealth_upgrade = 1
			sub_torpedo_upgrade = 1
		}
		obsolete = yes
	}

	create_equipment_variant = {
		name = "Sirena Class"
		type = submarine_1
		upgrades = {
			ship_reliability_upgrade = 2
			sub_engine_upgrade = 2
			sub_stealth_upgrade = 2
			sub_torpedo_upgrade = 2
		}
	}

	create_equipment_variant = {
		name = "Navigatori Class"
		type = destroyer_1
		upgrades = {
			ship_torpedo_upgrade = 1
			destroyer_engine_upgrade = 1
			ship_ASW_upgrade = 1
			ship_anti_air_upgrade = 1
		}
		obsolete = yes
	}

	create_equipment_variant = {
		name = "Maestrale Class"
		type = destroyer_1
		upgrades = {
			ship_torpedo_upgrade = 3
			destroyer_engine_upgrade = 3
			ship_ASW_upgrade = 3
			ship_anti_air_upgrade = 3
		}
	}

	create_equipment_variant = {
		name = "Giussano Class"
		type = light_cruiser_1
		upgrades = {
			ship_reliability_upgrade = 1
			ship_engine_upgrade = 1
			ship_gun_upgrade = 1
			ship_anti_air_upgrade = 1
		}
		obsolete = yes
	}

	create_equipment_variant = {
		name = "Montecuccoli Class"
		type = light_cruiser_1
		upgrades = {
			ship_reliability_upgrade = 3
			ship_engine_upgrade = 3
			ship_gun_upgrade = 3
			ship_anti_air_upgrade = 3
		}
	}

	create_equipment_variant = {
		name = "Zara Class"
		type = heavy_cruiser_1
		upgrades = {
			ship_reliability_upgrade = 2
			ship_engine_upgrade = 2
			ship_armor_upgrade = 2
			ship_gun_upgrade = 2
		}
	}
}
if = {
	limit = { has_dlc = "Man the Guns"}
	### Battleship Variants	
	create_equipment_variant = {			# original configuration of class pre-refit (refit in 1937-40)
		name = "Andrea Doria Class"
		type = ship_hull_heavy_1
		name_group = ITA_BB_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_heavy_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = heavy_ship_engine_1
			fixed_ship_secondaries_slot = ship_secondaries_1
			fixed_ship_armor_slot = ship_armor_bb_1
			front_1_custom_slot = ship_anti_air_1
			mid_1_custom_slot = ship_secondaries_1
			mid_2_custom_slot = empty
			rear_1_custom_slot = ship_heavy_battery_1
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Conte Di Cavour Class"
		type = ship_hull_heavy_1
		name_group = ITA_BB_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_heavy_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = heavy_ship_engine_2
			fixed_ship_secondaries_slot = ship_secondaries_1
			fixed_ship_armor_slot = ship_armor_bb_1
			front_1_custom_slot = empty
			mid_1_custom_slot = ship_secondaries_1
			mid_2_custom_slot = ship_airplane_launcher_1
			rear_1_custom_slot = ship_heavy_battery_1
		}
	}
	### Heavy Cruiser Variants	
	create_equipment_variant = {
		name = "San Giorgio Class"
		type = ship_hull_cruiser_coastal_defense_ship
		name_group = ITA_CA_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_medium_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_2
			fixed_ship_secondaries_slot = ship_secondaries_1
			front_1_custom_slot = ship_anti_air_1
			mid_1_custom_slot = ship_secondaries_1
			mid_2_custom_slot = empty
			rear_1_custom_slot = ship_torpedo_1
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Trento Class"
		type = ship_hull_cruiser_1
		name_group = ITA_CA_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_medium_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_1
			fixed_ship_secondaries_slot = empty
			mid_1_custom_slot = ship_medium_battery_1
			mid_2_custom_slot = ship_airplane_launcher_1
			rear_1_custom_slot = ship_anti_air_1
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Zara Class"
		type = ship_hull_cruiser_1
		name_group = ITA_CA_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_medium_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_2
			fixed_ship_secondaries_slot = empty
			mid_1_custom_slot = ship_medium_battery_1
			mid_2_custom_slot = ship_airplane_launcher_1
			rear_1_custom_slot = ship_anti_air_1
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Bolzano Class"
		type = ship_hull_cruiser_1
		name_group = ITA_CA_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_medium_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_2
			fixed_ship_armor_slot = ship_armor_cruiser_1
			fixed_ship_secondaries_slot = empty
			mid_1_custom_slot = ship_medium_battery_1
			mid_2_custom_slot = ship_airplane_launcher_1
			rear_1_custom_slot = ship_anti_air_1
		}
	}
	### Light Cruiser Variants	
	create_equipment_variant = {
		name = "Taranto Class"
		type = ship_hull_cruiser_1
		name_group = ITA_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_medium_battery_1
			fixed_ship_anti_air_slot = empty
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_1
			mid_1_custom_slot = ship_torpedo_1
			mid_2_custom_slot = empty
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Giussano Class"
		type = ship_hull_cruiser_1
		name_group = ITA_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_medium_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_2
			fixed_ship_armor_slot = empty
			mid_1_custom_slot = ship_torpedo_1
			mid_2_custom_slot = ship_airplane_launcher_1
			rear_1_custom_slot = ship_light_medium_battery_2
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Montecuccoli Class"
		type = ship_hull_cruiser_2
		name_group = ITA_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_medium_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_2
			fixed_ship_armor_slot = empty
			front_1_custom_slot = ship_anti_air_1
			mid_1_custom_slot = ship_torpedo_1
			mid_2_custom_slot = ship_airplane_launcher_1
			rear_1_custom_slot = ship_light_medium_battery_2
		}
	}

	### Light Cruiser Variants	
	create_equipment_variant = {
		name = "Duca degli Abruzzi Class"
		type = ship_hull_cruiser_2
		name_group = ITA_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_medium_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_2
			fixed_ship_armor_slot = ship_armor_cruiser_1
			front_1_custom_slot = ship_anti_air_2
			mid_1_custom_slot = ship_torpedo_1
			mid_2_custom_slot = ship_airplane_launcher_2
			rear_1_custom_slot = ship_light_medium_battery_2
		}
	}
	### Destroyer Variants
	create_equipment_variant = {
		name = "Curatone Class" 		#Represents the following WWI/1920s DDs/DEs: Curatone, Sauro/Sella, Turbine, Pilo, Audace, Siritori, La Masa, Palestro, Cantore
		type = ship_hull_light_1
		name_group = ITA_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = empty
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Navigatori Class"			#Represents the following WWI/1920s Heavy DDs: Mirabello, (Aquila,) Leone, Navigatori
		type = ship_hull_light_1
		name_group = ITA_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = empty
			rear_1_custom_slot = ship_depth_charge_1
		}
		obsolete = yes
	}	
	create_equipment_variant = {
		name = "Maestrale Class" 			#Represents the following WWI/1920s DDs: Freccia, Foglore, Mastrale, Oriani, Spica (and subclasses)
		type = ship_hull_light_1
		name_group = ITA_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_2
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = empty
			rear_1_custom_slot = ship_depth_charge_1
		}
	}
	create_equipment_variant = {
			name = "Littorio Class"
			type = ship_hull_heavy_2
			name_group = ITA_BB_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_heavy_battery_2
				fixed_ship_anti_air_slot = ship_anti_air_2
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = heavy_ship_engine_2
				fixed_ship_secondaries_slot = ship_secondaries_2
				fixed_ship_armor_slot = ship_armor_bb_2
				front_1_custom_slot = ship_anti_air_2
				mid_1_custom_slot = ship_secondaries_2
				mid_2_custom_slot = ship_airplane_launcher_2
				rear_1_custom_slot = ship_heavy_battery_2
			}
		}
		create_equipment_variant = {
			name = "Italian Pride Class"
			type = ship_hull_heavy_2
			name_group = ITA_BB_HISTORICAL
			parent_version = 0
			modules = {
				fixed_ship_battery_slot = ship_heavy_battery_2
				fixed_ship_anti_air_slot = ship_anti_air_1
				fixed_ship_fire_control_system_slot = ship_fire_control_system_0
				fixed_ship_radar_slot = empty
				fixed_ship_engine_slot = heavy_ship_engine_2
				fixed_ship_secondaries_slot = ship_secondaries_2
				fixed_ship_armor_slot = ship_armor_bb_2
				front_1_custom_slot = ship_heavy_battery_2
				mid_1_custom_slot = ship_heavy_battery_2
				mid_2_custom_slot = ship_heavy_battery_2
				rear_1_custom_slot = ship_heavy_battery_2
			}
		}
	### Submarine Variants	
	create_equipment_variant = {
		name = "Mameli Class"
		type = ship_hull_submarine_1
		name_group = ITA_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}	
	create_equipment_variant = {
		name = "Bandiera Class"
		type = ship_hull_submarine_1
		name_group = ITA_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = ship_torpedo_sub_1
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Sirena Class"
		type = ship_hull_submarine_2
		name_group = ITA_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Calvi Class"
		type = ship_hull_submarine_2
		name_group = ITA_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = ship_torpedo_sub_1
		}
	}
	create_equipment_variant = {
		name = "Deutschland Class"
		type = ship_hull_cruiser_panzerschiff
		name_group = GER_CA_HISTORICAL
		parent_version = 0
		modules = {
        	fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_battery_slot = ship_heavy_battery_2
			fixed_ship_secondaries_slot = ship_secondaries_1
			fixed_ship_armor_slot = ship_armor_cruiser_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_engine_slot = cruiser_ship_engine_1
			rear_1_custom_slot = ship_torpedo_1
			mid_1_custom_slot = ship_airplane_launcher_1
    	}
	}
	create_equipment_variant = {
		name = "Type 24 Class"
		type = ship_hull_light_1
		name_group = GER_TB_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = empty
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Zerstörer 1934 Class"
		type = ship_hull_light_2
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_2
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_torpedo_1
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Zerstörer 1934A Class"
		type = ship_hull_light_2
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_2
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_torpedo_1
			rear_1_custom_slot = ship_depth_charge_1
		}
	}
	create_equipment_variant = {
		name = "Type II Class"
		type = ship_hull_submarine_1
		name_group = GER_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Type VII Class"
		type = ship_hull_submarine_2
		name_group = GER_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_2
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = empty
		}
	}
	create_equipment_variant = {
		name = "Königsberg Class"
		type = ship_hull_cruiser_1
		name_group = GER_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_medium_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_1
			mid_1_custom_slot = ship_torpedo_1
			mid_2_custom_slot = ship_light_medium_battery_1
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Emden Class"
		type = ship_hull_cruiser_1
		name_group = GER_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_medium_battery_1
			fixed_ship_anti_air_slot = empty
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_1
			mid_1_custom_slot = ship_light_medium_battery_1
			mid_2_custom_slot = ship_torpedo_1
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Bremse Class"
		type = ship_hull_cruiser_1
		name_group = GER_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			mid_1_custom_slot = ship_anti_air_1
			mid_2_custom_slot = empty
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
	create_equipment_variant = {
		name = "Leipzig Class"
		type = ship_hull_cruiser_2
		name_group = GER_CL_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_medium_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_1
			front_1_custom_slot = ship_anti_air_1
			mid_1_custom_slot = ship_torpedo_1
			mid_2_custom_slot = ship_light_medium_battery_1
			rear_1_custom_slot = ship_airplane_launcher_1
		}
	}
	create_equipment_variant = {
		name = "Admiral Hipper Class"
		type = ship_hull_cruiser_2
		name_group = GER_CA_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_medium_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_2
			front_1_custom_slot = ship_anti_air_1
			mid_1_custom_slot = ship_torpedo_1
			mid_2_custom_slot = ship_airplane_launcher_1
			rear_1_custom_slot = ship_medium_battery_2
		}
	}
	create_equipment_variant = {
		name = "Scharnhorst Class"
		type = ship_hull_heavy_2
		name_group = GER_BB_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_heavy_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = heavy_ship_engine_2
			fixed_ship_secondaries_slot = ship_secondaries_1
			fixed_ship_armor_slot = ship_armor_bb_1
			front_1_custom_slot = ship_anti_air_1
			mid_1_custom_slot = empty
			mid_2_custom_slot = ship_secondaries_1
			rear_1_custom_slot = ship_heavy_battery_2
		}
	}
	create_equipment_variant = {
		name = "Deutschland Class"
		type = ship_hull_pre_dreadnought
		name_group = GER_BB_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_heavy_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = heavy_ship_engine_1
			fixed_ship_secondaries_slot = ship_secondaries_1
			fixed_ship_armor_slot = ship_armor_bb_1
			front_1_custom_slot = ship_anti_air_1
			mid_1_custom_slot = empty
			rear_1_custom_slot = empty
		}
		obsolete = yes
	}
}